/**
 * 
 */
package com.facturacion.api.security;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author CESAR
 *
 */
public class CryptographyGenerator {
	
	public static String generateMD2(String input) throws NoSuchAlgorithmException {
        return generateString(input, "MD2", 32);
    }

    public static String generateMD5(String input) throws NoSuchAlgorithmException {
        return generateString(input, "MD5", 32);
    }

    public static String generateSHA1(String input) throws NoSuchAlgorithmException {
        return generateString(input, "SHA-1", 40);
    }

    public static String generateSHA256(String input) throws NoSuchAlgorithmException {
        return generateString(input, "SHA-256", 64);
    }

    public static String generateSHA512(String input) throws NoSuchAlgorithmException {
        return generateString(input, "SHA-512", 128);
    }

    public static String generateSHA384(String input) throws NoSuchAlgorithmException {
        return generateString(input, "SHA-384", 96);
    }

    private static String generateString(String input, String algorithm, int minLength){
        MessageDigest messageDigest;
        String result = null;
		try {
			messageDigest = MessageDigest.getInstance(algorithm);
	        byte[] bytes = messageDigest.digest(input.getBytes());
	        BigInteger integer = new BigInteger(1, bytes);
	        result = integer.toString(16);
	        while (result.length() < minLength) {
	            result = "0" + result;
	        }

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
       
		return result;

    }

}
