/**
 * 
 */
package com.facturacion.api.security;

import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * @author CESAR
 *
 */
public class StringHashGenerator {
	
    public static void main(String args[]) throws NoSuchAlgorithmException {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("String: ");
        String input = inputScanner.next();

        CryptographyGenerator generator = new CryptographyGenerator();

        System.out.println("\nMD2: " + generator.generateMD2(input)
                + "\nMD5: " + generator.generateMD5(input)
                + "\nSHA-1: " + generator.generateSHA1(input)
                + "\nSHA-256: " + generator.generateSHA256(input)
                + "\nSHA-384: " + generator.generateSHA384(input)
                + "\nSHA-512: " + generator.generateSHA512(input));
    }

}
