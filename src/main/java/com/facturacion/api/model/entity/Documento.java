package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "documento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d")
    , @NamedQuery(name = "Documento.findByIddocumento", query = "SELECT d FROM Documento d WHERE d.iddocumento = :iddocumento")
    , @NamedQuery(name = "Documento.findByTipodocumento", query = "SELECT d FROM Documento d WHERE d.tipodocumento = :tipodocumento")
    , @NamedQuery(name = "Documento.findBySerie", query = "SELECT d FROM Documento d WHERE d.serie = :serie")
    , @NamedQuery(name = "Documento.findBySecuencia", query = "SELECT d FROM Documento d WHERE d.secuencia = :secuencia")
    , @NamedQuery(name = "Documento.findByFechaemision", query = "SELECT d FROM Documento d WHERE d.fechaemision = :fechaemision")
    , @NamedQuery(name = "Documento.findByFechavencimiento", query = "SELECT d FROM Documento d WHERE d.fechavencimiento = :fechavencimiento")
    , @NamedQuery(name = "Documento.findByNroordenservicio", query = "SELECT d FROM Documento d WHERE d.nroordenservicio = :nroordenservicio")
    , @NamedQuery(name = "Documento.findByEstado", query = "SELECT d FROM Documento d WHERE d.estado = :estado")
    , @NamedQuery(name = "Documento.findByObservacion", query = "SELECT d FROM Documento d WHERE d.observacion = :observacion")
    , @NamedQuery(name = "Documento.findBySubtotalventas", query = "SELECT d FROM Documento d WHERE d.subtotalventas = :subtotalventas")
    , @NamedQuery(name = "Documento.findByImporteanticipos", query = "SELECT d FROM Documento d WHERE d.importeanticipos = :importeanticipos")
    , @NamedQuery(name = "Documento.findByImportedescuentos", query = "SELECT d FROM Documento d WHERE d.importedescuentos = :importedescuentos")
    , @NamedQuery(name = "Documento.findByValorventatotal", query = "SELECT d FROM Documento d WHERE d.valorventatotal = :valorventatotal")
    , @NamedQuery(name = "Documento.findByImporteisc", query = "SELECT d FROM Documento d WHERE d.importeisc = :importeisc")
    , @NamedQuery(name = "Documento.findByImporteigv", query = "SELECT d FROM Documento d WHERE d.importeigv = :importeigv")
    , @NamedQuery(name = "Documento.findByImporteotroscargos", query = "SELECT d FROM Documento d WHERE d.importeotroscargos = :importeotroscargos")
    , @NamedQuery(name = "Documento.findByImporteotrostributos", query = "SELECT d FROM Documento d WHERE d.importeotrostributos = :importeotrostributos")
    , @NamedQuery(name = "Documento.findByImportetotaldoc", query = "SELECT d FROM Documento d WHERE d.importetotaldoc = :importetotaldoc")
    , @NamedQuery(name = "Documento.findByTipooperacion", query = "SELECT d FROM Documento d WHERE d.tipooperacion = :tipooperacion")
    , @NamedQuery(name = "Documento.findByTipoafectaci\u00f3n", query = "SELECT d FROM Documento d WHERE d.tipoafectaci\u00f3n = :tipoafectaci\u00f3n")
    , @NamedQuery(name = "Documento.findByEnviosunat", query = "SELECT d FROM Documento d WHERE d.enviosunat = :enviosunat")
    , @NamedQuery(name = "Documento.findByFechaenviosunat", query = "SELECT d FROM Documento d WHERE d.fechaenviosunat = :fechaenviosunat")
    , @NamedQuery(name = "Documento.findByEstadoenviosunat", query = "SELECT d FROM Documento d WHERE d.estadoenviosunat = :estadoenviosunat")
    , @NamedQuery(name = "Documento.findByObservacionsunat", query = "SELECT d FROM Documento d WHERE d.observacionsunat = :observacionsunat")
    , @NamedQuery(name = "Documento.findByFile1sunat", query = "SELECT d FROM Documento d WHERE d.file1sunat = :file1sunat")
    , @NamedQuery(name = "Documento.findByFile2sunat", query = "SELECT d FROM Documento d WHERE d.file2sunat = :file2sunat")
    , @NamedQuery(name = "Documento.findByFile3sunat", query = "SELECT d FROM Documento d WHERE d.file3sunat = :file3sunat")
    , @NamedQuery(name = "Documento.findByNotasadicionales", query = "SELECT d FROM Documento d WHERE d.notasadicionales = :notasadicionales")
    , @NamedQuery(name = "Documento.findByUsuarioregistro", query = "SELECT d FROM Documento d WHERE d.usuarioregistro = :usuarioregistro")
    , @NamedQuery(name = "Documento.findByUsuarioactualiza", query = "SELECT d FROM Documento d WHERE d.usuarioactualiza = :usuarioactualiza")
    , @NamedQuery(name = "Documento.findByFecharegistro", query = "SELECT d FROM Documento d WHERE d.fecharegistro = :fecharegistro")
    , @NamedQuery(name = "Documento.findByFechaactualiza", query = "SELECT d FROM Documento d WHERE d.fechaactualiza = :fechaactualiza")})
public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddocumento")
    @JsonProperty("id")
    private Integer iddocumento;
    
    @Basic(optional = false)
    @Column(name = "tipodocumento")
    @JsonProperty("tipoDocumento")
    private int tipodocumento;
    
    @Basic(optional = false)
    @Column(name = "serie")
    @JsonProperty("serie")
    private String serie;
    
    @Basic(optional = false)
    @Column(name = "secuencia")
    @JsonProperty("secuencia")
    private String secuencia;
    
    @Basic(optional = false)
    @Column(name = "fechaemision")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaEmision")
    private Date fechaemision;
    
    @Column(name = "fechavencimiento")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaVencimiento")
    private Date fechavencimiento;
    
    @Column(name = "nroordenservicio")
    @JsonProperty("nroOrden")
    private String nroordenservicio;
    
    @Basic(optional = false)
    @Column(name = "estado")
    @JsonProperty("estado")
    private int estado;
    
    @Basic(optional = false)
    @Column(name = "observacion")
    @JsonProperty("observacion")
    private String observacion;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "subtotalventas")
    @JsonProperty("subTotalVentas")
    private BigDecimal subtotalventas;
    
    @Basic(optional = false)
    @Column(name = "importeanticipos")
    @JsonProperty("anticipos")
    private BigDecimal importeanticipos;
    
    @Basic(optional = false)
    @Column(name = "importedescuentos")
    @JsonProperty("descuentos")
    private BigDecimal importedescuentos;
    
    @Basic(optional = false)
    @Column(name = "valorventatotal")
    @JsonProperty("ventaTotal")
    private BigDecimal valorventatotal;
    
    @Basic(optional = false)
    @Column(name = "importeisc")
    @JsonProperty("isc")
    private BigDecimal importeisc;
    
    @Basic(optional = false)
    @Column(name = "importeigv")
    @JsonProperty("igv")
    private BigDecimal importeigv;
    
    @Basic(optional = false)
    @Column(name = "importeotroscargos")
    @JsonProperty("otrosCargos")
    private BigDecimal importeotroscargos;
    
    @Basic(optional = false)
    @Column(name = "importeotrostributos")
    @JsonProperty("otrosTributos")
    private BigDecimal importeotrostributos;
    
    @Basic(optional = false)
    @Column(name = "importetotaldoc")
    @JsonProperty("totalDocumento")
    private BigDecimal importetotaldoc;
    
    @Basic(optional = false)
    @Column(name = "tipooperacion")
    @JsonProperty("tipoOperacion")
    private int tipooperacion;
    
    @Basic(optional = false)
    @Column(name = "tipoafectaci\u00f3n")
    @JsonProperty("tipoAfectacion")
    private int tipoafectación;
    
    @Column(name = "enviosunat")
    @JsonProperty("envioSunat")
    private Integer enviosunat;
    
    @Column(name = "fechaenviosunat")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaEnvioSunat")
    private Date fechaenviosunat;
    
    @Column(name = "estadoenviosunat")
    @JsonProperty("estadoEnvioSunat")
    private Integer estadoenviosunat;
    
    @Column(name = "observacionsunat")
    @JsonProperty("observacionSunat")
    private String observacionsunat;
    
    @Column(name = "file1sunat")
    @JsonProperty("file1Sunat" )
    private String file1sunat;
    
    @Column(name = "file2sunat")
    @JsonProperty("file2Sunat" )
    private String file2sunat;
    
    @Column(name = "file3sunat")
    @JsonProperty("file3Sunat" )
    private String file3sunat;
    
    @Column(name = "notasadicionales")
    @JsonProperty("notas" )
    private String notasadicionales;
    
    @Basic(optional = false)
    @Column(name = "usuarioregistro")
    @JsonProperty("usuarioRegistro")
    private String usuarioregistro;
    
 
    @Basic(optional = false)
    @Column(name = "usuarioactualiza")
    @JsonProperty("usuarioActualiza")
    private String usuarioactualiza;
    
    @Basic(optional = false)
    @Column(name = "fecharegistro")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaRegistro")
    private Date fecharegistro;
    
    @Basic(optional = false)
    @Column(name = "fechaactualiza")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaActualiza")
    private Date fechaactualiza;
    
    
    @JoinColumn(name = "cliente_idcliente", referencedColumnName = "idcliente")
    @ManyToOne(optional = false)
    @JsonProperty("cliente")
    private Cliente clienteIdcliente;
    
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    @ManyToOne(optional = false)
    @JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    @JoinColumn(name = "formapago_idformapago", referencedColumnName = "idformapago")
    @ManyToOne(optional = false)
    @JsonProperty("formaPago")
    private FormaPago formapagoIdformapago;
    
    @JoinColumn(name = "moneda_idmoneda", referencedColumnName = "idmoneda")
    @ManyToOne(optional = false)
    @JsonProperty("moneda")
    private Moneda monedaIdmoneda;
    

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentoIddocumento",fetch = FetchType.LAZY)
    @JsonProperty("guiasRemision")
    @JsonIgnoreProperties("documento")
    //@JsonIgnore
    private Set<GuiaRemision> guiaRemisionSet;
    
    @OneToMany(mappedBy = "documentoIddocumento", fetch = FetchType.LAZY, orphanRemoval=true, cascade = CascadeType.ALL) 
    //@JsonProperty("items")
    @JsonIgnoreProperties("documentoIddocumento")
    private List<DocumentoItem> documentoitemSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentoIddocumento",fetch = FetchType.LAZY)
    @JsonProperty("liquidaciones")
    @JsonIgnoreProperties("documento")
    private Set<Liquidacion> liquidacionSet;

    public Documento() {
    }

    public Documento(Integer iddocumento) {
        this.iddocumento = iddocumento;
    }

    public Documento(Integer iddocumento, int tipodocumento, String serie, String secuencia, Date fechaemision, int estado, String observacion, BigDecimal subtotalventas, BigDecimal importeanticipos, BigDecimal importedescuentos, BigDecimal valorventatotal, BigDecimal importeisc, BigDecimal importeigv, BigDecimal importeotroscargos, BigDecimal importeotrostributos, BigDecimal importetotaldoc, int tipooperacion, int tipoafectación, String usuarioregistro, String usuarioactualiza, Date fecharegistro, Date fechaactualiza) {
        this.iddocumento = iddocumento;
        this.tipodocumento = tipodocumento;
        this.serie = serie;
        this.secuencia = secuencia;
        this.fechaemision = fechaemision;
        this.estado = estado;
        this.observacion = observacion;
        this.subtotalventas = subtotalventas;
        this.importeanticipos = importeanticipos;
        this.importedescuentos = importedescuentos;
        this.valorventatotal = valorventatotal;
        this.importeisc = importeisc;
        this.importeigv = importeigv;
        this.importeotroscargos = importeotroscargos;
        this.importeotrostributos = importeotrostributos;
        this.importetotaldoc = importetotaldoc;
        this.tipooperacion = tipooperacion;
        this.tipoafectación = tipoafectación;
        this.usuarioregistro = usuarioregistro;
        this.usuarioactualiza = usuarioactualiza;
        this.fecharegistro = fecharegistro;
        this.fechaactualiza = fechaactualiza;
    }

    public Integer getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(Integer iddocumento) {
        this.iddocumento = iddocumento;
    }

    public int getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(int tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public Date getFechaemision() {
        return fechaemision;
    }

    public void setFechaemision(Date fechaemision) {
        this.fechaemision = fechaemision;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public String getNroordenservicio() {
        return nroordenservicio;
    }

    public void setNroordenservicio(String nroordenservicio) {
        this.nroordenservicio = nroordenservicio;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getSubtotalventas() {
        return subtotalventas;
    }

    public void setSubtotalventas(BigDecimal subtotalventas) {
        this.subtotalventas = subtotalventas;
    }

    public BigDecimal getImporteanticipos() {
        return importeanticipos;
    }

    public void setImporteanticipos(BigDecimal importeanticipos) {
        this.importeanticipos = importeanticipos;
    }

    public BigDecimal getImportedescuentos() {
        return importedescuentos;
    }

    public void setImportedescuentos(BigDecimal importedescuentos) {
        this.importedescuentos = importedescuentos;
    }

    public BigDecimal getValorventatotal() {
        return valorventatotal;
    }

    public void setValorventatotal(BigDecimal valorventatotal) {
        this.valorventatotal = valorventatotal;
    }

    public BigDecimal getImporteisc() {
        return importeisc;
    }

    public void setImporteisc(BigDecimal importeisc) {
        this.importeisc = importeisc;
    }

    public BigDecimal getImporteigv() {
        return importeigv;
    }

    public void setImporteigv(BigDecimal importeigv) {
        this.importeigv = importeigv;
    }

    public BigDecimal getImporteotroscargos() {
        return importeotroscargos;
    }

    public void setImporteotroscargos(BigDecimal importeotroscargos) {
        this.importeotroscargos = importeotroscargos;
    }

    public BigDecimal getImporteotrostributos() {
        return importeotrostributos;
    }

    public void setImporteotrostributos(BigDecimal importeotrostributos) {
        this.importeotrostributos = importeotrostributos;
    }

    public BigDecimal getImportetotaldoc() {
        return importetotaldoc;
    }

    public void setImportetotaldoc(BigDecimal importetotaldoc) {
        this.importetotaldoc = importetotaldoc;
    }

    public int getTipooperacion() {
        return tipooperacion;
    }

    public void setTipooperacion(int tipooperacion) {
        this.tipooperacion = tipooperacion;
    }

    public int getTipoafectación() {
        return tipoafectación;
    }

    public void setTipoafectación(int tipoafectación) {
        this.tipoafectación = tipoafectación;
    }

    public Integer getEnviosunat() {
        return enviosunat;
    }

    public void setEnviosunat(Integer enviosunat) {
        this.enviosunat = enviosunat;
    }

    public Date getFechaenviosunat() {
        return fechaenviosunat;
    }

    public void setFechaenviosunat(Date fechaenviosunat) {
        this.fechaenviosunat = fechaenviosunat;
    }

    public Integer getEstadoenviosunat() {
        return estadoenviosunat;
    }

    public void setEstadoenviosunat(Integer estadoenviosunat) {
        this.estadoenviosunat = estadoenviosunat;
    }

    public String getObservacionsunat() {
        return observacionsunat;
    }

    public void setObservacionsunat(String observacionsunat) {
        this.observacionsunat = observacionsunat;
    }

    public String getFile1sunat() {
        return file1sunat;
    }

    public void setFile1sunat(String file1sunat) {
        this.file1sunat = file1sunat;
    }

    public String getFile2sunat() {
        return file2sunat;
    }

    public void setFile2sunat(String file2sunat) {
        this.file2sunat = file2sunat;
    }

    public String getFile3sunat() {
        return file3sunat;
    }

    public void setFile3sunat(String file3sunat) {
        this.file3sunat = file3sunat;
    }

    public String getNotasadicionales() {
        return notasadicionales;
    }

    public void setNotasadicionales(String notasadicionales) {
        this.notasadicionales = notasadicionales;
    }

    public String getUsuarioregistro() {
        return usuarioregistro;
    }

    public void setUsuarioregistro(String usuarioregistro) {
        this.usuarioregistro = usuarioregistro;
    }

    public String getUsuarioactualiza() {
        return usuarioactualiza;
    }

    public void setUsuarioactualiza(String usuarioactualiza) {
        this.usuarioactualiza = usuarioactualiza;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Date getFechaactualiza() {
        return fechaactualiza;
    }

    public void setFechaactualiza(Date fechaactualiza) {
        this.fechaactualiza = fechaactualiza;
    }

    public Cliente getClienteIdcliente() {
        return clienteIdcliente;
    }

    public void setClienteIdcliente(Cliente clienteIdcliente) {
        this.clienteIdcliente = clienteIdcliente;
    }

    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    public FormaPago getFormapagoIdformapago() {
        return formapagoIdformapago;
    }

    public void setFormapagoIdformapago(FormaPago formapagoIdformapago) {
        this.formapagoIdformapago = formapagoIdformapago;
    }

    public Moneda getMonedaIdmoneda() {
        return monedaIdmoneda;
    }

    public void setMonedaIdmoneda(Moneda monedaIdmoneda) {
        this.monedaIdmoneda = monedaIdmoneda;
    }
    
    @XmlTransient
    public Set<GuiaRemision> getGuiaRemisionSet() {
        return guiaRemisionSet;
    }

    public void setGuiaRemisionSet(Set<GuiaRemision> guiaRemisionSet) {
        this.guiaRemisionSet = guiaRemisionSet;
    }


    @XmlTransient
    public List<DocumentoItem> getDocumentoitemSet() {
        return documentoitemSet;
    }

    public void setDocumentoitemSet(List<DocumentoItem> documentoitemSet) {
        this.documentoitemSet = documentoitemSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddocumento != null ? iddocumento.hashCode() : 0);
        return hash;
    }
    
    
    @XmlTransient
    public Set<Liquidacion> getLiquidacionSet() {
        return liquidacionSet;
    }

    public void setLiquidacionSet(Set<Liquidacion> liquidacionSet) {
        this.liquidacionSet = liquidacionSet;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) object;
        if ((this.iddocumento == null && other.iddocumento != null) || (this.iddocumento != null && !this.iddocumento.equals(other.iddocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.dto.Documento[ iddocumento=" + iddocumento + " ]";
    }
    
}

