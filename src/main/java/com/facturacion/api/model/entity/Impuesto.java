/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "impuesto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Impuesto.findAll", query = "SELECT i FROM Impuesto i")
    , @NamedQuery(name = "Impuesto.findByIdimpuesto", query = "SELECT i FROM Impuesto i WHERE i.idimpuesto = :idimpuesto")
    , @NamedQuery(name = "Impuesto.findByDescripimpuesto", query = "SELECT i FROM Impuesto i WHERE i.descripimpuesto = :descripimpuesto")
    , @NamedQuery(name = "Impuesto.findByPorcentajeimp", query = "SELECT i FROM Impuesto i WHERE i.porcentajeimp = :porcentajeimp")})
public class Impuesto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idimpuesto")
    @JsonProperty("id")
    private Integer idimpuesto;
    @Basic(optional = false)
    @Column(name = "descripimpuesto")
    @JsonProperty("descripcion")
    private String descripimpuesto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "porcentajeimp")
    @JsonProperty("valor")
    private BigDecimal porcentajeimp;

    public Impuesto() {
    }

    public Impuesto(Integer idimpuesto) {
        this.idimpuesto = idimpuesto;
    }

    public Impuesto(Integer idimpuesto, String descripimpuesto, BigDecimal porcentajeimp) {
        this.idimpuesto = idimpuesto;
        this.descripimpuesto = descripimpuesto;
        this.porcentajeimp = porcentajeimp;
    }

    public Integer getIdimpuesto() {
        return idimpuesto;
    }

    public void setIdimpuesto(Integer idimpuesto) {
        this.idimpuesto = idimpuesto;
    }

    public String getDescripimpuesto() {
        return descripimpuesto;
    }

    public void setDescripimpuesto(String descripimpuesto) {
        this.descripimpuesto = descripimpuesto;
    }

    public BigDecimal getPorcentajeimp() {
        return porcentajeimp;
    }

    public void setPorcentajeimp(BigDecimal porcentajeimp) {
        this.porcentajeimp = porcentajeimp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idimpuesto != null ? idimpuesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Impuesto)) {
            return false;
        }
        Impuesto other = (Impuesto) object;
        if ((this.idimpuesto == null && other.idimpuesto != null) || (this.idimpuesto != null && !this.idimpuesto.equals(other.idimpuesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Impuesto[ idimpuesto=" + idimpuesto + " ]";
    }
    
}
