/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "tarifaruta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TarifaRuta.findAll", query = "SELECT t FROM TarifaRuta t")
    , @NamedQuery(name = "TarifaRuta.findByIdtarifaruta", query = "SELECT t FROM TarifaRuta t WHERE t.idtarifaruta = :idtarifaruta")
    , @NamedQuery(name = "TarifaRuta.findByNombreruta", query = "SELECT t FROM TarifaRuta t WHERE t.nombreruta = :nombreruta")
    , @NamedQuery(name = "TarifaRuta.findByImportetarifa", query = "SELECT t FROM TarifaRuta t WHERE t.importetarifa = :importetarifa")
    , @NamedQuery(name = "TarifaRuta.findByFecharegistrosistema", query = "SELECT t FROM TarifaRuta t WHERE t.fecharegistrosistema = :fecharegistrosistema")
    , @NamedQuery(name = "TarifaRuta.findByFechaactualizasistema", query = "SELECT t FROM TarifaRuta t WHERE t.fechaactualizasistema = :fechaactualizasistema")
    , @NamedQuery(name = "TarifaRuta.findByEstadotarifa", query = "SELECT t FROM TarifaRuta t WHERE t.estadotarifa = :estadotarifa")
    , @NamedQuery(name = "TarifaRuta.findByMonedatarifa", query = "SELECT t FROM TarifaRuta t WHERE t.monedatarifa = :monedatarifa")})
public class TarifaRuta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @JsonProperty("id")
    @Column(name = "idtarifaruta")
    private Integer idtarifaruta;
    
    @Basic(optional = false)
    @JsonProperty("notas")
    @Column(name = "nombreruta")
    private String nombreruta;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "importetarifa")
    @JsonProperty("importe")
    private BigDecimal importetarifa;
    
    @Column(name = "fecharegistrosistema")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaRegistro")
    private Date fecharegistrosistema;
    
    @Column(name = "fechaactualizasistema")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaActualiza")
    private Date fechaactualizasistema;
    
    @Column(name = "estadotarifa")
    @JsonProperty("estado")
    private Integer estadotarifa;
    
    @Column(name = "monedatarifa")
    @JsonProperty("moneda")
    private Integer monedatarifa;
    
    @JoinColumn(name = "clientefactoria_idorigen", referencedColumnName = "idclientefactoria")
    @ManyToOne(optional = false)
    @JsonProperty("origen")
    private ClienteFactoria clientefactoriaIdorigen;
    
    @JoinColumn(name = "clientefactoria_iddestino", referencedColumnName = "idclientefactoria")
    @ManyToOne(optional = false)
    @JsonProperty("destino")
    private ClienteFactoria clientefactoriaIddestino;
    
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    @ManyToOne
    @JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    @JoinColumn(name = "turno_idturno", referencedColumnName = "idturno")
    @ManyToOne(optional = false)
    @JsonProperty("turno")
    private Turno turnoIdturno;

    public TarifaRuta() {
    }

    public TarifaRuta(Integer idtarifaruta) {
        this.idtarifaruta = idtarifaruta;
    }

    public TarifaRuta(Integer idtarifaruta, String nombreruta, BigDecimal importetarifa) {
        this.idtarifaruta = idtarifaruta;
        this.nombreruta = nombreruta;
        this.importetarifa = importetarifa;
    }

    public Integer getIdtarifaruta() {
        return idtarifaruta;
    }

    public void setIdtarifaruta(Integer idtarifaruta) {
        this.idtarifaruta = idtarifaruta;
    }

    public String getNombreruta() {
        return nombreruta;
    }

    public void setNombreruta(String nombreruta) {
        this.nombreruta = nombreruta;
    }

    public BigDecimal getImportetarifa() {
        return importetarifa;
    }

    public void setImportetarifa(BigDecimal importetarifa) {
        this.importetarifa = importetarifa;
    }

    public Date getFecharegistrosistema() {
        return fecharegistrosistema;
    }

    public void setFecharegistrosistema(Date fecharegistrosistema) {
        this.fecharegistrosistema = fecharegistrosistema;
    }

    public Date getFechaactualizasistema() {
        return fechaactualizasistema;
    }

    public void setFechaactualizasistema(Date fechaactualizasistema) {
        this.fechaactualizasistema = fechaactualizasistema;
    }

    public Integer getEstadotarifa() {
        return estadotarifa;
    }

    public void setEstadotarifa(Integer estadotarifa) {
        this.estadotarifa = estadotarifa;
    }

    public Integer getMonedatarifa() {
        return monedatarifa;
    }

    public void setMonedatarifa(Integer monedatarifa) {
        this.monedatarifa = monedatarifa;
    }

    public ClienteFactoria getClientefactoriaIdorigen() {
        return clientefactoriaIdorigen;
    }

    public void setClientefactoriaIdorigen(ClienteFactoria clientefactoriaIdorigen) {
        this.clientefactoriaIdorigen = clientefactoriaIdorigen;
    }

    public ClienteFactoria getClientefactoriaIddestino() {
        return clientefactoriaIddestino;
    }

    public void setClientefactoriaIddestino(ClienteFactoria clientefactoriaIddestino) {
        this.clientefactoriaIddestino = clientefactoriaIddestino;
    }

    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    public Turno getTurnoIdturno() {
        return turnoIdturno;
    }

    public void setTurnoIdturno(Turno turnoIdturno) {
        this.turnoIdturno = turnoIdturno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtarifaruta != null ? idtarifaruta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TarifaRuta)) {
            return false;
        }
        TarifaRuta other = (TarifaRuta) object;
        if ((this.idtarifaruta == null && other.idtarifaruta != null) || (this.idtarifaruta != null && !this.idtarifaruta.equals(other.idtarifaruta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.dto.TarifaRuta[ idtarifaruta=" + idtarifaruta + " ]";
    }
    
}
