package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "formapago")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormaPago.findAll", query = "SELECT f FROM FormaPago f")
    , @NamedQuery(name = "FormaPago.findByIdformapago", query = "SELECT f FROM FormaPago f WHERE f.idformapago = :idformapago")
    , @NamedQuery(name = "FormaPago.findByCodigo", query = "SELECT f FROM FormaPago f WHERE f.codigo = :codigo")
    , @NamedQuery(name = "FormaPago.findByDescripcion", query = "SELECT f FROM FormaPago f WHERE f.descripcion = :descripcion")
    , @NamedQuery(name = "FormaPago.findByNemonico", query = "SELECT f FROM FormaPago f WHERE f.nemonico = :nemonico")})
public class FormaPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idformapago")
    @JsonProperty("id")
    private Integer idformapago;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "nemonico")
    private String nemonico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "formapagoIdformapago")
    @JsonIgnore
    private Set<Documento> documentoSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "formapagoIdformapago")
    @JsonIgnore
    private Set<OrdenServicio> ordenservicioSet;

    public FormaPago() {
    }

    public FormaPago(Integer idformapago) {
        this.idformapago = idformapago;
    }

    public FormaPago(Integer idformapago, String codigo) {
        this.idformapago = idformapago;
        this.codigo = codigo;
    }

    public Integer getIdformapago() {
        return idformapago;
    }

    public void setIdformapago(Integer idformapago) {
        this.idformapago = idformapago;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNemonico() {
        return nemonico;
    }

    public void setNemonico(String nemonico) {
        this.nemonico = nemonico;
    }

    @XmlTransient
    public Set<Documento> getDocumentoSet() {
        return documentoSet;
    }

    public void setDocumentoSet(Set<Documento> documentoSet) {
        this.documentoSet = documentoSet;
    }

    @XmlTransient
    public Set<OrdenServicio> getOrdenservicioSet() {
        return ordenservicioSet;
    }

    public void setOrdenservicioSet(Set<OrdenServicio> ordenservicioSet) {
        this.ordenservicioSet = ordenservicioSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idformapago != null ? idformapago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormaPago)) {
            return false;
        }
        FormaPago other = (FormaPago) object;
        if ((this.idformapago == null && other.idformapago != null) || (this.idformapago != null && !this.idformapago.equals(other.idformapago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.dto.Formapago[ idformapago=" + idformapago + " ]";
    }
    
}
