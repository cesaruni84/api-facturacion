/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "transportista")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transportista.findAll", query = "SELECT t FROM Transportista t")
    , @NamedQuery(name = "Transportista.findByIdtransportista", query = "SELECT t FROM Transportista t WHERE t.idtransportista = :idtransportista")
    , @NamedQuery(name = "Transportista.findByNombres", query = "SELECT t FROM Transportista t WHERE t.nombres = :nombres")
    , @NamedQuery(name = "Transportista.findByApellidos", query = "SELECT t FROM Transportista t WHERE t.apellidos = :apellidos")
    , @NamedQuery(name = "Transportista.findByEmail", query = "SELECT t FROM Transportista t WHERE t.email = :email")
    , @NamedQuery(name = "Transportista.findByTelefono", query = "SELECT t FROM Transportista t WHERE t.telefono = :telefono")
    , @NamedQuery(name = "Transportista.findByNrodni", query = "SELECT t FROM Transportista t WHERE t.nrodni = :nrodni")
    , @NamedQuery(name = "Transportista.findByFecharegistro", query = "SELECT t FROM Transportista t WHERE t.fecharegistro = :fecharegistro")
    , @NamedQuery(name = "Transportista.findByFechaupdate", query = "SELECT t FROM Transportista t WHERE t.fechaupdate = :fechaupdate")
    , @NamedQuery(name = "Transportista.findByNrocertiinscrip", query = "SELECT t FROM Transportista t WHERE t.nrocertiinscrip = :nrocertiinscrip")
    , @NamedQuery(name = "Transportista.findByNrolicenciachofer", query = "SELECT t FROM Transportista t WHERE t.nrolicenciachofer = :nrolicenciachofer")})
public class Transportista implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtransportista")
    @JsonProperty("id")
    private Integer idtransportista;
    
    @Basic(optional = false)
    @Column(name = "nombres")
    @JsonProperty("nombres")
    private String nombres;
    
    @Basic(optional = false)
    @Column(name = "apellidos")
    @JsonProperty("apellidos")
    private String apellidos;
    
    @Basic(optional = false)
    @Column(name = "email")
    @JsonProperty("email")
    private String email;
    
    @Basic(optional = false)
    @Column(name = "telefono")
    @JsonProperty("telefono")
    private String telefono;
    
    @Basic(optional = false)
    @Column(name = "nrodni")
    @JsonProperty("dni")
    private String nrodni;
    
    @Basic(optional = false)
    @Column(name = "fecharegistro")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaRegistro")
    private Date fecharegistro;
    
    @Basic(optional = false)
    @Column(name = "fechaupdate")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaActualiza")
    private Date fechaupdate;
    
    @Column(name = "nrocertiinscrip")
    @JsonProperty("certificado")
    private String nrocertiinscrip;
    
    @Column(name = "nrolicenciachofer")
    @JsonProperty("licencia")
    private String nrolicenciachofer;
    

    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    @JoinColumn(name = "vehiculo_idvehiculo", referencedColumnName = "idvehiculo")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("vehiculo")
    private Vehiculo vehiculoIdvehiculo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transportistaIdtransportista", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<GuiaRemision> guiaRemisionSet;

    public Transportista() {
    }

    public Transportista(Integer idtransportista) {
        this.idtransportista = idtransportista;
    }

    public Transportista(Integer idtransportista, String nombres, String apellidos, String email, String telefono, String nrodni, Date fecharegistro, Date fechaupdate) {
        this.idtransportista = idtransportista;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.telefono = telefono;
        this.nrodni = nrodni;
        this.fecharegistro = fecharegistro;
        this.fechaupdate = fechaupdate;
    }

    public Integer getIdtransportista() {
        return idtransportista;
    }

    public void setIdtransportista(Integer idtransportista) {
        this.idtransportista = idtransportista;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNrodni() {
        return nrodni;
    }

    public void setNrodni(String nrodni) {
        this.nrodni = nrodni;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Date getFechaupdate() {
        return fechaupdate;
    }

    public void setFechaupdate(Date fechaupdate) {
        this.fechaupdate = fechaupdate;
    }
    
    public String getNrocertiinscrip() {
        return nrocertiinscrip;
    }

    public void setNrocertiinscrip(String nrocertiinscrip) {
        this.nrocertiinscrip = nrocertiinscrip;
    }

    public String getNrolicenciachofer() {
        return nrolicenciachofer;
    }

    public void setNrolicenciachofer(String nrolicenciachofer) {
        this.nrolicenciachofer = nrolicenciachofer;
    }

    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    public Vehiculo getVehiculoIdvehiculo() {
        return vehiculoIdvehiculo;
    }

    public void setVehiculoIdvehiculo(Vehiculo vehiculoIdvehiculo) {
        this.vehiculoIdvehiculo = vehiculoIdvehiculo;
    }

    @XmlTransient
    public Set<GuiaRemision> getGuiaRemisionSet() {
        return guiaRemisionSet;
    }

    public void setGuiaRemisionSet(Set<GuiaRemision> guiaRemisionSet) {
        this.guiaRemisionSet = guiaRemisionSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransportista != null ? idtransportista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transportista)) {
            return false;
        }
        Transportista other = (Transportista) object;
        if ((this.idtransportista == null && other.idtransportista != null) || (this.idtransportista != null && !this.idtransportista.equals(other.idtransportista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Transportista[ idtransportista=" + idtransportista + " ]";
    }
    
}
