package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "moneda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Moneda.findAll", query = "SELECT m FROM Moneda m")
    , @NamedQuery(name = "Moneda.findByIdmoneda", query = "SELECT m FROM Moneda m WHERE m.idmoneda = :idmoneda")
    , @NamedQuery(name = "Moneda.findByCodigo", query = "SELECT m FROM Moneda m WHERE m.codigo = :codigo")
    , @NamedQuery(name = "Moneda.findByDescripcion", query = "SELECT m FROM Moneda m WHERE m.descripcion = :descripcion")
    , @NamedQuery(name = "Moneda.findByNemonico", query = "SELECT m FROM Moneda m WHERE m.nemonico = :nemonico")})
public class Moneda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idmoneda")
    @JsonProperty("id")
    private Integer idmoneda;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "nemonico")
    private String nemonico;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "monedaIdmoneda")
    @JsonIgnore
    private Set<Documento> documentoSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "monedaIdmoneda")
    @JsonIgnore
    private Set<OrdenServicio> ordenservicioSet;

    public Moneda() {
    }

    public Moneda(Integer idmoneda) {
        this.idmoneda = idmoneda;
    }

    public Moneda(Integer idmoneda, String codigo) {
        this.idmoneda = idmoneda;
        this.codigo = codigo;
    }

    public Integer getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(Integer idmoneda) {
        this.idmoneda = idmoneda;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNemonico() {
        return nemonico;
    }

    public void setNemonico(String nemonico) {
        this.nemonico = nemonico;
    }

    @XmlTransient
    public Set<Documento> getDocumentoSet() {
        return documentoSet;
    }

    public void setDocumentoSet(Set<Documento> documentoSet) {
        this.documentoSet = documentoSet;
    }

    @XmlTransient
    public Set<OrdenServicio> getOrdenservicioSet() {
        return ordenservicioSet;
    }

    public void setOrdenservicioSet(Set<OrdenServicio> ordenservicioSet) {
        this.ordenservicioSet = ordenservicioSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmoneda != null ? idmoneda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Moneda)) {
            return false;
        }
        Moneda other = (Moneda) object;
        if ((this.idmoneda == null && other.idmoneda != null) || (this.idmoneda != null && !this.idmoneda.equals(other.idmoneda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.dto.Moneda[ idmoneda=" + idmoneda + " ]";
    }
    
}

