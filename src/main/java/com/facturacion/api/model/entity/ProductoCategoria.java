/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "productocategoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoCategoria.findAll", query = "SELECT p FROM ProductoCategoria p")
    , @NamedQuery(name = "ProductoCategoria.findByIdcategoria", query = "SELECT p FROM ProductoCategoria p WHERE p.idcategoria = :idcategoria")
    , @NamedQuery(name = "ProductoCategoria.findByNombrecategoria", query = "SELECT p FROM ProductoCategoria p WHERE p.nombrecategoria = :nombrecategoria")})
public class ProductoCategoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcategoria")
    @JsonProperty("id")
    private Integer idcategoria;
    
    @Basic(optional = false)
    @Column(name = "nombrecategoria")
    @JsonProperty("nombre")
    private String nombrecategoria;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productocategoriaIdcategoria", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Producto> productoSet;

    public ProductoCategoria() {
    }

    public ProductoCategoria(Integer idcategoria) {
        this.idcategoria = idcategoria;
    }

    public ProductoCategoria(Integer idcategoria, String nombrecategoria) {
        this.idcategoria = idcategoria;
        this.nombrecategoria = nombrecategoria;
    }

    public Integer getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(Integer idcategoria) {
        this.idcategoria = idcategoria;
    }

    public String getNombrecategoria() {
        return nombrecategoria;
    }

    public void setNombrecategoria(String nombrecategoria) {
        this.nombrecategoria = nombrecategoria;
    }

    @XmlTransient
    public Set<Producto> getProductoSet() {
        return productoSet;
    }

    public void setProductoSet(Set<Producto> productoSet) {
        this.productoSet = productoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategoria != null ? idcategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCategoria)) {
            return false;
        }
        ProductoCategoria other = (ProductoCategoria) object;
        if ((this.idcategoria == null && other.idcategoria != null) || (this.idcategoria != null && !this.idcategoria.equals(other.idcategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.ProductoCategoria[ idcategoria=" + idcategoria + " ]";
    }
    
}
