package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "documentoitem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoItem.findAll", query = "SELECT d FROM DocumentoItem d")
    , @NamedQuery(name = "DocumentoItem.findByIddocitem", query = "SELECT d FROM DocumentoItem d WHERE d.iddocitem = :iddocitem")
    , @NamedQuery(name = "DocumentoItem.findByTipoitem", query = "SELECT d FROM DocumentoItem d WHERE d.tipoitem = :tipoitem")
    , @NamedQuery(name = "DocumentoItem.findByCodigo", query = "SELECT d FROM DocumentoItem d WHERE d.codigo = :codigo")
    , @NamedQuery(name = "DocumentoItem.findByDescripcion", query = "SELECT d FROM DocumentoItem d WHERE d.descripcion = :descripcion")
    , @NamedQuery(name = "DocumentoItem.findByCantidad", query = "SELECT d FROM DocumentoItem d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "DocumentoItem.findByPreciounitario", query = "SELECT d FROM DocumentoItem d WHERE d.preciounitario = :preciounitario")
    , @NamedQuery(name = "DocumentoItem.findByIndtipodescuento", query = "SELECT d FROM DocumentoItem d WHERE d.indtipodescuento = :indtipodescuento")
    , @NamedQuery(name = "DocumentoItem.findByFactordescuento", query = "SELECT d FROM DocumentoItem d WHERE d.factordescuento = :factordescuento")
    , @NamedQuery(name = "DocumentoItem.findByDescuentos", query = "SELECT d FROM DocumentoItem d WHERE d.descuentos = :descuentos")
    , @NamedQuery(name = "DocumentoItem.findBySubtotal", query = "SELECT d FROM DocumentoItem d WHERE d.subtotal = :subtotal")
    , @NamedQuery(name = "DocumentoItem.findByIndtipoigv", query = "SELECT d FROM DocumentoItem d WHERE d.indtipoigv = :indtipoigv")
    , @NamedQuery(name = "DocumentoItem.findByValorigv", query = "SELECT d FROM DocumentoItem d WHERE d.valorigv = :valorigv")
    , @NamedQuery(name = "DocumentoItem.findByValorisc", query = "SELECT d FROM DocumentoItem d WHERE d.valorisc = :valorisc")
    , @NamedQuery(name = "DocumentoItem.findByTotal", query = "SELECT d FROM DocumentoItem d WHERE d.total = :total")})
public class DocumentoItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddocitem")
    @JsonProperty("id")
    private Integer iddocitem;
    
    @Basic(optional = false)
    @Column(name = "tipoitem")
    @JsonProperty("tipo")
    private int tipoitem;
    
    @Column(name = "codigo")
    @JsonProperty("codigo")
    private String codigo;
    
    @Column(name = "descripcion")
    @JsonProperty("descripcion")
    private String descripcion;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "cantidad")
    @JsonProperty("cantidad")
    private BigDecimal cantidad;
    
    @Basic(optional = false)
    @Column(name = "preciounitario")
    @JsonProperty("tarifa")
    private BigDecimal preciounitario;
    
    @Column(name = "indtipodescuento")
    @JsonProperty("tipoDescuento")
    private Integer indtipodescuento;
    
    @Column(name = "factordescuento")
    @JsonProperty("factorDescuento")
    private BigDecimal factordescuento;
    
    @Basic(optional = false)
    @Column(name = "descuentos")
    @JsonProperty("descuentos")
    private BigDecimal descuentos;
    
    @Basic(optional = false)
    @Column(name = "subtotal")
    @JsonProperty("subTotal")
    private BigDecimal subtotal;
    
    @Basic(optional = false)
    @Column(name = "indtipoigv")
    @JsonProperty("tipoIGV")
    private int indtipoigv;
    
    @Basic(optional = false)
    @Column(name = "valorigv")
    @JsonProperty("valorIGV")
    private BigDecimal valorigv;
    
    @Basic(optional = false)
    @Column(name = "valorisc")
    @JsonProperty("valorISC")
    private BigDecimal valorisc;
    
    @Basic(optional = false)
    @Column(name = "total")
    @JsonProperty("total")
    private BigDecimal total;
    
    @JoinColumn(name = "documento_iddocumento", referencedColumnName = "iddocumento", nullable=true)
    @ManyToOne(optional = false )
    // @JsonProperty("documento")
    @JsonIgnoreProperties({"documentoitemSet","ordenServicioSet", "guiaRemisionSet"})
    private Documento documentoIddocumento;
    
    @JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto")
    @ManyToOne
    @JsonProperty("productos")
    private Producto productoIdproducto;
    
    @JoinColumn(name = "unidadmedida_idunidadmedida", referencedColumnName = "idunidadmedida")
    @ManyToOne(optional = false)
    @JsonProperty("unidadMedida")
    private UnidadMedida unidadmedidaIdunidadmedida;
    
    public DocumentoItem() {
    }

    public DocumentoItem(Integer iddocitem) {
        this.iddocitem = iddocitem;
    }

    public DocumentoItem(Integer iddocitem, int tipoitem, BigDecimal cantidad, BigDecimal preciounitario, BigDecimal descuentos, BigDecimal subtotal, int indtipoigv, BigDecimal valorigv, BigDecimal valorisc, BigDecimal total) {
        this.iddocitem = iddocitem;
        this.tipoitem = tipoitem;
        this.cantidad = cantidad;
        this.preciounitario = preciounitario;
        this.descuentos = descuentos;
        this.subtotal = subtotal;
        this.indtipoigv = indtipoigv;
        this.valorigv = valorigv;
        this.valorisc = valorisc;
        this.total = total;
    }

    public Integer getIddocitem() {
        return iddocitem;
    }

    public void setIddocitem(Integer iddocitem) {
        this.iddocitem = iddocitem;
    }

    public int getTipoitem() {
        return tipoitem;
    }

    public void setTipoitem(int tipoitem) {
        this.tipoitem = tipoitem;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPreciounitario() {
        return preciounitario;
    }

    public void setPreciounitario(BigDecimal preciounitario) {
        this.preciounitario = preciounitario;
    }

    public Integer getIndtipodescuento() {
        return indtipodescuento;
    }

    public void setIndtipodescuento(Integer indtipodescuento) {
        this.indtipodescuento = indtipodescuento;
    }

    public BigDecimal getFactordescuento() {
        return factordescuento;
    }

    public void setFactordescuento(BigDecimal factordescuento) {
        this.factordescuento = factordescuento;
    }

    public BigDecimal getDescuentos() {
        return descuentos;
    }

    public void setDescuentos(BigDecimal descuentos) {
        this.descuentos = descuentos;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public int getIndtipoigv() {
        return indtipoigv;
    }

    public void setIndtipoigv(int indtipoigv) {
        this.indtipoigv = indtipoigv;
    }

    public BigDecimal getValorigv() {
        return valorigv;
    }

    public void setValorigv(BigDecimal valorigv) {
        this.valorigv = valorigv;
    }

    public BigDecimal getValorisc() {
        return valorisc;
    }

    public void setValorisc(BigDecimal valorisc) {
        this.valorisc = valorisc;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Documento getDocumentoIddocumento() {
        return documentoIddocumento;
    }

    public void setDocumentoIddocumento(Documento documentoIddocumento) {
        this.documentoIddocumento = documentoIddocumento;
    }

    public Producto getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(Producto productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public UnidadMedida getUnidadmedidaIdunidadmedida() {
        return unidadmedidaIdunidadmedida;
    }

    public void setUnidadmedidaIdunidadmedida(UnidadMedida unidadmedidaIdunidadmedida) {
        this.unidadmedidaIdunidadmedida = unidadmedidaIdunidadmedida;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddocitem != null ? iddocitem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoItem)) {
            return false;
        }
        DocumentoItem other = (DocumentoItem) object;
        if ((this.iddocitem == null && other.iddocitem != null) || (this.iddocitem != null && !this.iddocitem.equals(other.iddocitem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.dto.Documentoitem[ iddocitem=" + iddocitem + " ]";
    }
    
}
