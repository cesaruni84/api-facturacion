/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "guiaremisionproducto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GuiaRemisionProducto.findAll", query = "SELECT g FROM GuiaRemisionProducto g")
    , @NamedQuery(name = "GuiaRemisionProducto.findByIdguiaremisionprod", query = "SELECT g FROM GuiaRemisionProducto g WHERE g.idguiaremisionprod = :idguiaremisionprod")
    , @NamedQuery(name = "GuiaRemisionProducto.findByCantidadproducto", query = "SELECT g FROM GuiaRemisionProducto g WHERE g.cantidadproducto = :cantidadproducto")
    , @NamedQuery(name = "GuiaRemisionProducto.findByPesoproducto", query = "SELECT g FROM GuiaRemisionProducto g WHERE g.pesoproducto = :pesoproducto")})
public class GuiaRemisionProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idguiaremisionprod")
    @JsonProperty("id")
    private Integer idguiaremisionprod;
    
    @Basic(optional = false)
    @Column(name = "cantidadproducto")
    @JsonProperty("cantidad")
    private int cantidadproducto;
    
    @Basic(optional = false)
    @Column(name = "pesoproducto")
    @JsonProperty("peso")
    private BigDecimal pesoproducto;
    
    @JoinColumn(name = "guiaremision_idguiaremision", referencedColumnName = "idguiaremision")
    @ManyToOne(optional = false , fetch = FetchType.LAZY)
    @JsonIgnoreProperties("guiaRemisionProductoSet")
    @JsonIgnore
    private GuiaRemision guiaremisionIdguiaremision;
    
    @JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto")
    @ManyToOne
    @JsonProperty("producto")
    private Producto productoIdproducto;
    
    @JoinColumn(name = "unidadmedida_idunidadmedida", referencedColumnName = "idunidadmedida")
    @ManyToOne
    @JsonProperty("unidadMedida")
    private UnidadMedida unidadmedidaIdunidadmedida;

    public GuiaRemisionProducto() {
    }

    public GuiaRemisionProducto(Integer idguiaremisionprod) {
        this.idguiaremisionprod = idguiaremisionprod;
    }

    public GuiaRemisionProducto(Integer idguiaremisionprod, int cantidadproducto, BigDecimal pesoproducto) {
        this.idguiaremisionprod = idguiaremisionprod;
        this.cantidadproducto = cantidadproducto;
        this.pesoproducto = pesoproducto;
    }

    public Integer getIdguiaremisionprod() {
        return idguiaremisionprod;
    }

    public void setIdguiaremisionprod(Integer idguiaremisionprod) {
        this.idguiaremisionprod = idguiaremisionprod;
    }

    public int getCantidadproducto() {
        return cantidadproducto;
    }

    public void setCantidadproducto(int cantidadproducto) {
        this.cantidadproducto = cantidadproducto;
    }

    public BigDecimal getPesoproducto() {
        return pesoproducto;
    }

    public void setPesoproducto(BigDecimal pesoproducto) {
        this.pesoproducto = pesoproducto;
    }

    public GuiaRemision getGuiaremisionIdguiaremision() {
        return guiaremisionIdguiaremision;
    }

    public void setGuiaremisionIdguiaremision(GuiaRemision guiaremisionIdguiaremision) {
        this.guiaremisionIdguiaremision = guiaremisionIdguiaremision;
    }

    public Producto getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(Producto productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public UnidadMedida getUnidadmedidaIdunidadmedida() {
        return unidadmedidaIdunidadmedida;
    }

    public void setUnidadmedidaIdunidadmedida(UnidadMedida unidadmedidaIdunidadmedida) {
        this.unidadmedidaIdunidadmedida = unidadmedidaIdunidadmedida;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idguiaremisionprod != null ? idguiaremisionprod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuiaRemisionProducto)) {
            return false;
        }
        GuiaRemisionProducto other = (GuiaRemisionProducto) object;
        if ((this.idguiaremisionprod == null && other.idguiaremisionprod != null) || (this.idguiaremisionprod != null && !this.idguiaremisionprod.equals(other.idguiaremisionprod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.GuiaRemisionProducto[ idguiaremisionprod=" + idguiaremisionprod + " ]";
    }
    
}
