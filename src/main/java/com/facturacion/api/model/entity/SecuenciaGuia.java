/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "secuenciaguia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SecuenciaGuia.findAll", query = "SELECT s FROM SecuenciaGuia s")
    , @NamedQuery(name = "SecuenciaGuia.findByIdsecuencia", query = "SELECT s FROM SecuenciaGuia s WHERE s.idsecuencia = :idsecuencia")
    , @NamedQuery(name = "SecuenciaGuia.findByNroserieactual", query = "SELECT s FROM SecuenciaGuia s WHERE s.nroserieactual = :nroserieactual")
    , @NamedQuery(name = "SecuenciaGuia.findByNroguiaactual", query = "SELECT s FROM SecuenciaGuia s WHERE s.nroguiaactual = :nroguiaactual")
    , @NamedQuery(name = "SecuenciaGuia.findByFechaactualizacion", query = "SELECT s FROM SecuenciaGuia s WHERE s.fechaactualizacion = :fechaactualizacion")})
public class SecuenciaGuia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsecuencia")
    private Integer idsecuencia;
    @Basic(optional = false)
    @Column(name = "nroserieactual")
    private String nroserieactual;
    @Basic(optional = false)
    @Column(name = "nroguiaactual")
    private String nroguiaactual;
    @Basic(optional = false)
    @Column(name = "fechaactualizacion")
    @Temporal(TemporalType.DATE)
    private Date fechaactualizacion;

    public SecuenciaGuia() {
    }

    public SecuenciaGuia(Integer idsecuencia) {
        this.idsecuencia = idsecuencia;
    }

    public SecuenciaGuia(Integer idsecuencia, String nroserieactual, String nroguiaactual, Date fechaactualizacion) {
        this.idsecuencia = idsecuencia;
        this.nroserieactual = nroserieactual;
        this.nroguiaactual = nroguiaactual;
        this.fechaactualizacion = fechaactualizacion;
    }

    public Integer getIdsecuencia() {
        return idsecuencia;
    }

    public void setIdsecuencia(Integer idsecuencia) {
        this.idsecuencia = idsecuencia;
    }

    public String getNroserieactual() {
        return nroserieactual;
    }

    public void setNroserieactual(String nroserieactual) {
        this.nroserieactual = nroserieactual;
    }

    public String getNroguiaactual() {
        return nroguiaactual;
    }

    public void setNroguiaactual(String nroguiaactual) {
        this.nroguiaactual = nroguiaactual;
    }

    public Date getFechaactualizacion() {
        return fechaactualizacion;
    }

    public void setFechaactualizacion(Date fechaactualizacion) {
        this.fechaactualizacion = fechaactualizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsecuencia != null ? idsecuencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecuenciaGuia)) {
            return false;
        }
        SecuenciaGuia other = (SecuenciaGuia) object;
        if ((this.idsecuencia == null && other.idsecuencia != null) || (this.idsecuencia != null && !this.idsecuencia.equals(other.idsecuencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.SecuenciaGuia[ idsecuencia=" + idsecuencia + " ]";
    }
    
}
