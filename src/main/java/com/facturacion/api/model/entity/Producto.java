/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
    , @NamedQuery(name = "Producto.findByIdproducto", query = "SELECT p FROM Producto p WHERE p.idproducto = :idproducto")
    , @NamedQuery(name = "Producto.findByNombre", query = "SELECT p FROM Producto p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Producto.findByDescripcion", query = "SELECT p FROM Producto p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Producto.findByPrecio", query = "SELECT p FROM Producto p WHERE p.precio = :precio")
    , @NamedQuery(name = "Producto.findByStock", query = "SELECT p FROM Producto p WHERE p.stock = :stock")
    , @NamedQuery(name = "Producto.findByFechaingreso", query = "SELECT p FROM Producto p WHERE p.fechaingreso = :fechaingreso")
    , @NamedQuery(name = "Producto.findByFechaactualizacion", query = "SELECT p FROM Producto p WHERE p.fechaactualizacion = :fechaactualizacion")})
@JsonPropertyOrder( "{id}" )
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idproducto")
    @JsonProperty("id")
    private Integer idproducto;
    
    @Transient
    @JsonProperty("nemonico")
    private String nemonicoLargo;
    
    @Basic(optional = false)
    @Column(name = "nombre")
    @JsonProperty("nombre")
    private String nombre;
    
    @Basic(optional = false)
    @Column(name = "descripcion")
    @JsonProperty("descripcion")
    private String descripcion;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "precio")
    @JsonProperty("precio")
    private BigDecimal precio;
    
    @Basic(optional = false)
    @Column(name = "stock")
    @JsonProperty("stock")
    private BigDecimal stock;
    
    @Basic(optional = false)
    @Column(name = "fechaingreso")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaIngreso")
    private Date fechaingreso;
    
    @Basic(optional = false)
    @Column(name = "fechaactualizacion")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaActualizacion")
    private Date fechaactualizacion;
    

    @Column(name = "codigo")
    @JsonProperty("codigo")
    private String codigo;
    
    @JoinColumn(name = "productocategoria_idcategoria", referencedColumnName = "idcategoria")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("categoria")
    private ProductoCategoria productocategoriaIdcategoria;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoIdproducto", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<GuiaRemisionProducto> guiaRemisionProductoSet;

    public Producto() {
    }

    public Producto(Integer idproducto) {
        this.idproducto = idproducto;
    }

    public Producto(Integer idproducto, String nombre, String descripcion, BigDecimal precio, BigDecimal stock, Date fechaingreso, Date fechaactualizacion) {
        this.idproducto = idproducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.stock = stock;
        this.fechaingreso = fechaingreso;
        this.fechaactualizacion = fechaactualizacion;
    }

    public Integer getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(Integer idproducto) {
        this.idproducto = idproducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public Date getFechaactualizacion() {
        return fechaactualizacion;
    }

    public void setFechaactualizacion(Date fechaactualizacion) {
        this.fechaactualizacion = fechaactualizacion;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    /**
	 * @return the nemonicoLargo
	 */
	public String getNemonicoLargo() {
		return this.nombre + " - " + this.productocategoriaIdcategoria.getNombrecategoria();
	}

	/**
	 * @param nemonicoLargo the nemonicoLargo to set
	 */
	public void setNemonicoLargo(String nemonicoLargo) {
		this.nemonicoLargo = nemonicoLargo;
	}

	public ProductoCategoria getProductocategoriaIdcategoria() {
        return productocategoriaIdcategoria;
    }

    public void setProductocategoriaIdcategoria(ProductoCategoria productocategoriaIdcategoria) {
        this.productocategoriaIdcategoria = productocategoriaIdcategoria;
    }

    @XmlTransient
    public Set<GuiaRemisionProducto> getGuiaRemisionProductoSet() {
        return guiaRemisionProductoSet;
    }

    public void setGuiaRemisionProductoSet(Set<GuiaRemisionProducto> guiaRemisionProductoSet) {
        this.guiaRemisionProductoSet = guiaRemisionProductoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproducto != null ? idproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idproducto == null && other.idproducto != null) || (this.idproducto != null && !this.idproducto.equals(other.idproducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Producto[ idproducto=" + idproducto + " ]";
    }
    
}
