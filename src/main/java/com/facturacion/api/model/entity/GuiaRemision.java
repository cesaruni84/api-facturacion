/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "guiaremision")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GuiaRemision.findAll", query = "SELECT g FROM GuiaRemision g")
    , @NamedQuery(name = "GuiaRemision.findByIdguiaremision", query = "SELECT g FROM GuiaRemision g WHERE g.idguiaremision = :idguiaremision")
    , @NamedQuery(name = "GuiaRemision.findByNroserieguia", query = "SELECT g FROM GuiaRemision g WHERE g.nroserieguia = :nroserieguia")
    , @NamedQuery(name = "GuiaRemision.findByNrosecuenciaguia", query = "SELECT g FROM GuiaRemision g WHERE g.nrosecuenciaguia = :nrosecuenciaguia")
    , @NamedQuery(name = "GuiaRemision.findByTotalcantidad", query = "SELECT g FROM GuiaRemision g WHERE g.totalcantidad = :totalcantidad")
    , @NamedQuery(name = "GuiaRemision.findByTotalpeso", query = "SELECT g FROM GuiaRemision g WHERE g.totalpeso = :totalpeso")
    , @NamedQuery(name = "GuiaRemision.findByEstadoguia", query = "SELECT g FROM GuiaRemision g WHERE g.estadoguia = :estadoguia")
    , @NamedQuery(name = "GuiaRemision.findByMotivotraslado", query = "SELECT g FROM GuiaRemision g WHERE g.motivotraslado = :motivotraslado")
    , @NamedQuery(name = "GuiaRemision.findByImportetarifaasociado", query = "SELECT g FROM GuiaRemision g WHERE g.importetarifaasociado = :importetarifaasociado")
    , @NamedQuery(name = "GuiaRemision.findByFecharemision", query = "SELECT g FROM GuiaRemision g WHERE g.fecharemision = :fecharemision")
    , @NamedQuery(name = "GuiaRemision.findByFechatraslado", query = "SELECT g FROM GuiaRemision g WHERE g.fechatraslado = :fechatraslado")
    , @NamedQuery(name = "GuiaRemision.findByFecharecepcion", query = "SELECT g FROM GuiaRemision g WHERE g.fecharecepcion = :fecharecepcion")
    , @NamedQuery(name = "GuiaRemision.findByNroticketbalanza", query = "SELECT g FROM GuiaRemision g WHERE g.nroticketbalanza = :nroticketbalanza")
    , @NamedQuery(name = "GuiaRemision.findByNroserieguiacliente", query = "SELECT g FROM GuiaRemision g WHERE g.nroserieguiacliente = :nroserieguiacliente")
    , @NamedQuery(name = "GuiaRemision.findByNrosecuenguiacliente", query = "SELECT g FROM GuiaRemision g WHERE g.nrosecuenguiacliente = :nrosecuenguiacliente")
    , @NamedQuery(name = "GuiaRemision.findByFecharegistrosistema", query = "SELECT g FROM GuiaRemision g WHERE g.fecharegistrosistema = :fecharegistrosistema")
    , @NamedQuery(name = "GuiaRemision.findByFechaactualizasistema", query = "SELECT g FROM GuiaRemision g WHERE g.fechaactualizasistema = :fechaactualizasistema")
    , @NamedQuery(name = "GuiaRemision.findByUsuarioregistro", query = "SELECT g FROM GuiaRemision g WHERE g.usuarioregistro = :usuarioregistro")
    , @NamedQuery(name = "GuiaRemision.findByUsuarioactualiza", query = "SELECT g FROM GuiaRemision g WHERE g.usuarioactualiza = :usuarioactualiza")})
public class GuiaRemision implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idguiaremision")
    @JsonProperty("id")
    private Integer idguiaremision;
    
    @Basic(optional = false)
    @Column(name = "nroserieguia")
    @JsonProperty("serie")
    private String nroserieguia;
    
    @Basic(optional = false)
    @Column(name = "nrosecuenciaguia")
    @JsonProperty("secuencia")
    private String nrosecuenciaguia;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "totalcantidad")
    @JsonProperty("totalCantidad")
    private BigDecimal totalcantidad;
    
    @Basic(optional = false)
    @Column(name = "totalpeso")
    @JsonProperty("totalPeso")
    private BigDecimal totalpeso;
    
    @Basic(optional = false)
    @Column(name = "estadoguia")
    @JsonProperty("estado")
    private int estadoguia;
    
    @Basic(optional = false)
    @Column(name = "motivotraslado")
    @JsonProperty("motivoTraslado")
    private int motivotraslado;
    
    @Basic(optional = false)
    @Column(name = "importetarifaasociado")
    @JsonProperty("tarifa")
    private BigDecimal importetarifaasociado;
    
    @Transient
    @JsonProperty("subTotal")
    private BigDecimal subTotal;
    
    @Basic(optional = false)
    @Column(name = "fecharemision")
    @JsonProperty("fechaRemision")
    @Temporal(TemporalType.DATE)
    private Date fecharemision;
    
    @Basic(optional = false)
    @Column(name = "fechatraslado")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaTraslado")
    private Date fechatraslado;
    
    @Basic(optional = false)
    @Column(name = "fecharecepcion")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaRecepcion")
    private Date fecharecepcion;
    
    @Basic(optional = false)
    @Column(name = "nroticketbalanza")
    @JsonProperty("ticketBalanza")
    private String nroticketbalanza;
    
    @Basic(optional = false)
    @Column(name = "nroserieguiacliente")
    @JsonProperty("serieCliente")
    private String nroserieguiacliente;
    
    @Basic(optional = false)
    @Column(name = "nrosecuenguiacliente")
    @JsonProperty("secuenciaCliente")
    private String nrosecuenguiacliente;
    
    @Basic(optional = false)
    @Column(name = "fecharegistrosistema")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaRegistro")
    private Date fecharegistrosistema;
    
    @Basic(optional = false)
    @Column(name = "fechaactualizasistema")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaActualiza")
    private Date fechaactualizasistema;
    
    @Basic(optional = false)
    @Column(name = "usuarioregistro")
    @JsonProperty("usuarioRegistro")
    private String usuarioregistro;
    
    @Basic(optional = false)
    @Column(name = "usuarioactualiza")
    @JsonProperty("usuarioActualiza")
    private String usuarioactualiza;
    
    @Basic(optional = false)
    @Column(name = "placatracto")
    @JsonProperty("placaTracto")
    private String placatracto;
    
    @Basic(optional = false)
    @Column(name = "placabombona")
    @JsonProperty("placaBombona")
    private String placabombona;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "guiaremisionIdguiaremision", fetch = FetchType.LAZY)
    @JsonProperty("guiaDetalle")
    @JsonIgnoreProperties("guiaremisionIdguiaremision")
    private Set<GuiaRemisionProducto> guiaRemisionProductoSet;
    
    @JoinColumn(name = "balanza_idbalanza", referencedColumnName = "idbalanza")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("balanza")
    private Balanza balanzaIdbalanza;
    
    @JoinColumn(name = "clientefactoria_idremitente", referencedColumnName = "idclientefactoria")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("remitente")
    private ClienteFactoria clientefactoriaIdremitente;
    
    @JoinColumn(name = "clientefactoria_iddestinatario", referencedColumnName = "idclientefactoria")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("destinatario")
    private ClienteFactoria clientefactoriaIddestinatario;
    
    
    @JoinColumn(name = "documento_iddocumento", referencedColumnName = "iddocumento")
    @ManyToOne
    @JsonProperty("documento")
    @JsonIgnoreProperties({"documentoitemSet","liquidacionSet","guiaRemisionSet"})
    private Documento documentoIddocumento;
    
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    @ManyToOne
    @JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    @JoinColumn(name = "liquidacion_idliquidacion", referencedColumnName = "idliquidacion")
    //@ManyToOne(optional = false , fetch = FetchType.LAZY)
    @ManyToOne
    @JsonIgnoreProperties("guias")
    @JsonProperty("liquidacion")
    private Liquidacion liquidacionIdliquidacion;
    
    @JoinColumn(name = "transportista_idtransportista", referencedColumnName = "idtransportista")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("chofer")
    private Transportista transportistaIdtransportista;
    
    @Transient
    @JsonProperty("idOrigen")
    private String idOrigen;
    

    @Transient
    @JsonProperty("idDestino")
    private String idDestino;
    
    @Transient
    @JsonProperty("idProducto")
    private String idProducto;


    public GuiaRemision() {
    }

    public GuiaRemision(Integer idguiaremision) {
        this.idguiaremision = idguiaremision;
    }

    public GuiaRemision(Integer idguiaremision, String nroserieguia, String nrosecuenciaguia, BigDecimal totalcantidad, BigDecimal totalpeso, int estadoguia, int motivotraslado, BigDecimal importetarifaasociado, Date fecharemision, Date fechatraslado, Date fecharecepcion, String nroticketbalanza, String nroserieguiacliente, String nrosecuenguiacliente, Date fecharegistrosistema, Date fechaactualizasistema, String usuarioregistro, String usuarioactualiza) {
        this.idguiaremision = idguiaremision;
        this.nroserieguia = nroserieguia;
        this.nrosecuenciaguia = nrosecuenciaguia;
        this.totalcantidad = totalcantidad;
        this.totalpeso = totalpeso;
        this.estadoguia = estadoguia;
        this.motivotraslado = motivotraslado;
        this.importetarifaasociado = importetarifaasociado;
        this.fecharemision = fecharemision;
        this.fechatraslado = fechatraslado;
        this.fecharecepcion = fecharecepcion;
        this.nroticketbalanza = nroticketbalanza;
        this.nroserieguiacliente = nroserieguiacliente;
        this.nrosecuenguiacliente = nrosecuenguiacliente;
        this.fecharegistrosistema = fecharegistrosistema;
        this.fechaactualizasistema = fechaactualizasistema;
        this.usuarioregistro = usuarioregistro;
        this.usuarioactualiza = usuarioactualiza;
    }

    public Integer getIdguiaremision() {
        return idguiaremision;
    }

    public void setIdguiaremision(Integer idguiaremision) {
        this.idguiaremision = idguiaremision;
    }

    public String getNroserieguia() {
        return nroserieguia;
    }

    public void setNroserieguia(String nroserieguia) {
        this.nroserieguia = nroserieguia;
    }

    public String getNrosecuenciaguia() {
        return nrosecuenciaguia;
    }

    public void setNrosecuenciaguia(String nrosecuenciaguia) {
        this.nrosecuenciaguia = nrosecuenciaguia;
    }

    public BigDecimal getTotalcantidad() {
        return totalcantidad;
    }

    public void setTotalcantidad(BigDecimal totalcantidad) {
        this.totalcantidad = totalcantidad;
    }

    public BigDecimal getTotalpeso() {
        return totalpeso;
    }

    public void setTotalpeso(BigDecimal totalpeso) {
        this.totalpeso = totalpeso;
    }

    public int getEstadoguia() {
        return estadoguia;
    }

    public void setEstadoguia(int estadoguia) {
        this.estadoguia = estadoguia;
    }

    public int getMotivotraslado() {
        return motivotraslado;
    }

    public void setMotivotraslado(int motivotraslado) {
        this.motivotraslado = motivotraslado;
    }

    public BigDecimal getImportetarifaasociado() {
        return importetarifaasociado;
    }

    public void setImportetarifaasociado(BigDecimal importetarifaasociado) {
        this.importetarifaasociado = importetarifaasociado;
    }
    
    
    
    

    /**
	 * @return the subTotal
	 */
	public BigDecimal getSubTotal() {
        BigDecimal subTotalAjustado = importetarifaasociado.multiply(totalcantidad);
        subTotalAjustado = subTotalAjustado.setScale(2, BigDecimal.ROUND_HALF_UP);
		return subTotalAjustado;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public Date getFecharemision() {
        return fecharemision;
    }

    public void setFecharemision(Date fecharemision) {
        this.fecharemision = fecharemision;
    }

    public Date getFechatraslado() {
        return fechatraslado;
    }

    public void setFechatraslado(Date fechatraslado) {
        this.fechatraslado = fechatraslado;
    }

    public Date getFecharecepcion() {
        return fecharecepcion;
    }

    public void setFecharecepcion(Date fecharecepcion) {
        this.fecharecepcion = fecharecepcion;
    }

    public String getNroticketbalanza() {
        return nroticketbalanza;
    }

    public void setNroticketbalanza(String nroticketbalanza) {
        this.nroticketbalanza = nroticketbalanza;
    }

    public String getNroserieguiacliente() {
        return nroserieguiacliente;
    }

    public void setNroserieguiacliente(String nroserieguiacliente) {
        this.nroserieguiacliente = nroserieguiacliente;
    }

    public String getNrosecuenguiacliente() {
        return nrosecuenguiacliente;
    }

    public void setNrosecuenguiacliente(String nrosecuenguiacliente) {
        this.nrosecuenguiacliente = nrosecuenguiacliente;
    }

    public Date getFecharegistrosistema() {
        return fecharegistrosistema;
    }

    public void setFecharegistrosistema(Date fecharegistrosistema) {
        this.fecharegistrosistema = fecharegistrosistema;
    }

    public Date getFechaactualizasistema() {
        return fechaactualizasistema;
    }

    public void setFechaactualizasistema(Date fechaactualizasistema) {
        this.fechaactualizasistema = fechaactualizasistema;
    }

    public String getUsuarioregistro() {
        return usuarioregistro;
    }
    
    /**
	 * @return the placatracto
	 */
	public String getPlacatracto() {
		return placatracto;
	}

	/**
	 * @param placatracto the placatracto to set
	 */
	public void setPlacatracto(String placatracto) {
		this.placatracto = placatracto;
	}

	/**
	 * @return the placabombona
	 */
	public String getPlacabombona() {
		return placabombona;
	}

	/**
	 * @param placabombona the placabombona to set
	 */
	public void setPlacabombona(String placabombona) {
		this.placabombona = placabombona;
	}

	public void setUsuarioregistro(String usuarioregistro) {
        this.usuarioregistro = usuarioregistro;
    }

    public String getUsuarioactualiza() {
        return usuarioactualiza;
    }

    public void setUsuarioactualiza(String usuarioactualiza) {
        this.usuarioactualiza = usuarioactualiza;
    }

    @XmlTransient
    public Set<GuiaRemisionProducto> getGuiaRemisionProductoSet() {
        return guiaRemisionProductoSet;
    }

    public void setGuiaRemisionProductoSet(Set<GuiaRemisionProducto> guiaRemisionProductoSet) {
        this.guiaRemisionProductoSet = guiaRemisionProductoSet;
    }

    public Balanza getBalanzaIdbalanza() {
        return balanzaIdbalanza;
    }

    public void setBalanzaIdbalanza(Balanza balanzaIdbalanza) {
        this.balanzaIdbalanza = balanzaIdbalanza;
    }

    public ClienteFactoria getClientefactoriaIddestinatario() {
        return clientefactoriaIddestinatario;
    }

    public void setClientefactoriaIddestinatario(ClienteFactoria clientefactoriaIddestinatario) {
        this.clientefactoriaIddestinatario = clientefactoriaIddestinatario;
    }

    public ClienteFactoria getClientefactoriaIdremitente() {
        return clientefactoriaIdremitente;
    }

    public void setClientefactoriaIdremitente(ClienteFactoria clientefactoriaIdremitente) {
        this.clientefactoriaIdremitente = clientefactoriaIdremitente;
    }
    
    public Documento getDocumentoIddocumento() {
        return documentoIddocumento;
    }

    public void setDocumentoIddocumento(Documento documentoIddocumento) {
        this.documentoIddocumento = documentoIddocumento;
    }
    
    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    public Liquidacion getLiquidacionIdliquidacion() {
        return liquidacionIdliquidacion;
    }

    public void setLiquidacionIdliquidacion(Liquidacion liquidacionIdliquidacion) {
        this.liquidacionIdliquidacion = liquidacionIdliquidacion;
    }

    public Transportista getTransportistaIdtransportista() {
        return transportistaIdtransportista;
    }

    public void setTransportistaIdtransportista(Transportista transportistaIdtransportista) {
        this.transportistaIdtransportista = transportistaIdtransportista;
    }


    /**
	 * @return the idOrigen
	 */
	public String getIdOrigen() {
		if (this.getClientefactoriaIdremitente()!= null) {
			return this.getClientefactoriaIdremitente().getIdclientefactoria().toString();
		}
		return "0";
	}

	/**
	 * @param idOrigen the idOrigen to set
	 */
	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
		
	}

	/**
	 * @return the idDestino
	 */
	public String getIdDestino() {
		if (this.getClientefactoriaIddestinatario() != null ) {
			return this.getClientefactoriaIddestinatario().getIdclientefactoria().toString();
		}
		return "0";
	}

	/**
	 * @param idDestino the idDestino to set
	 */
	public void setIdDestino(String idDestino) {
		this.idDestino = idDestino;
	}
	

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		String valueProducto = "";
		Set<GuiaRemisionProducto> guiasDetalle = this.getGuiaRemisionProductoSet();
		for (GuiaRemisionProducto detalle : guiasDetalle) {
			if (detalle.getProductoIdproducto()!= null) {
				valueProducto =  detalle.getProductoIdproducto().getIdproducto().toString();
			}
		}
		return valueProducto.concat(this.getIdOrigen().concat(this.getIdDestino()));
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idguiaremision != null ? idguiaremision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuiaRemision)) {
            return false;
        }
        GuiaRemision other = (GuiaRemision) object;
        if ((this.idguiaremision == null && other.idguiaremision != null) || (this.idguiaremision != null && !this.idguiaremision.equals(other.idguiaremision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.GuiaRemision[ idguiaremision=" + idguiaremision + " ]";
    }
    
}
