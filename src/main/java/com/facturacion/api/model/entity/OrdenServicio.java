package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "ordenservicio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdenServicio.findAll", query = "SELECT o FROM OrdenServicio o")
    , @NamedQuery(name = "OrdenServicio.findByIdordenservicio", query = "SELECT o FROM OrdenServicio o WHERE o.idordenservicio = :idordenservicio")
    , @NamedQuery(name = "OrdenServicio.findByTipocod", query = "SELECT o FROM OrdenServicio o WHERE o.tipocod = :tipocod")
    , @NamedQuery(name = "OrdenServicio.findByNroorden", query = "SELECT o FROM OrdenServicio o WHERE o.nroorden = :nroorden")
    , @NamedQuery(name = "OrdenServicio.findBySituacion", query = "SELECT o FROM OrdenServicio o WHERE o.situacion = :situacion")
    , @NamedQuery(name = "OrdenServicio.findByEstado", query = "SELECT o FROM OrdenServicio o WHERE o.estado = :estado")
    , @NamedQuery(name = "OrdenServicio.findByFechaorden", query = "SELECT o FROM OrdenServicio o WHERE o.fechaorden = :fechaorden")
    , @NamedQuery(name = "OrdenServicio.findByFechavencimiento", query = "SELECT o FROM OrdenServicio o WHERE o.fechavencimiento = :fechavencimiento")
    , @NamedQuery(name = "OrdenServicio.findByFechaaprobacion", query = "SELECT o FROM OrdenServicio o WHERE o.fechaaprobacion = :fechaaprobacion")
    , @NamedQuery(name = "OrdenServicio.findByLugarentrega", query = "SELECT o FROM OrdenServicio o WHERE o.lugarentrega = :lugarentrega")
    , @NamedQuery(name = "OrdenServicio.findByInstrucciones", query = "SELECT o FROM OrdenServicio o WHERE o.instrucciones = :instrucciones")
    , @NamedQuery(name = "OrdenServicio.findByGlosa", query = "SELECT o FROM OrdenServicio o WHERE o.glosa = :glosa")
    , @NamedQuery(name = "OrdenServicio.findBySubtotal", query = "SELECT o FROM OrdenServicio o WHERE o.subtotal = :subtotal")
    , @NamedQuery(name = "OrdenServicio.findByTotalcantidad", query = "SELECT o FROM OrdenServicio o WHERE o.totalcantidad = :totalcantidad")
    , @NamedQuery(name = "OrdenServicio.findByDescuentos", query = "SELECT o FROM OrdenServicio o WHERE o.descuentos = :descuentos")
    , @NamedQuery(name = "OrdenServicio.findByValorcompra", query = "SELECT o FROM OrdenServicio o WHERE o.valorcompra = :valorcompra")
    , @NamedQuery(name = "OrdenServicio.findByIgvaplicado", query = "SELECT o FROM OrdenServicio o WHERE o.igvaplicado = :igvaplicado")
    , @NamedQuery(name = "OrdenServicio.findByImportetotal", query = "SELECT o FROM OrdenServicio o WHERE o.importetotal = :importetotal")
    , @NamedQuery(name = "OrdenServicio.findByUsuarioregistro", query = "SELECT o FROM OrdenServicio o WHERE o.usuarioregistro = :usuarioregistro")
    , @NamedQuery(name = "OrdenServicio.findByUsuarioactualiza", query = "SELECT o FROM OrdenServicio o WHERE o.usuarioactualiza = :usuarioactualiza")
    , @NamedQuery(name = "OrdenServicio.findByFecharegistro", query = "SELECT o FROM OrdenServicio o WHERE o.fecharegistro = :fecharegistro")
    , @NamedQuery(name = "OrdenServicio.findByFechaactualiza", query = "SELECT o FROM OrdenServicio o WHERE o.fechaactualiza = :fechaactualiza")})
public class OrdenServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idordenservicio")
    @JsonProperty("id")
    private Integer idordenservicio;
    
    @Basic(optional = false)
    @Column(name = "tipocod")
    private String tipocod;
    
    @Basic(optional = false)
    @Column(name = "nroorden")
    @JsonProperty("nroOrden")
    private String nroorden;
    
    @Basic(optional = false)
    @Column(name = "situacion")
    private int situacion;
    
    @Basic(optional = false)
    @Column(name = "estado")
    private int estado;
    
    @Basic(optional = false)
    @Column(name = "fechaorden")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaOrden")
    private Date fechaorden;
    
    @Column(name = "fechavencimiento")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaVencimiento")
    private Date fechavencimiento;
    
    @Column(name = "fechaaprobacion")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaAprobacion")
    private Date fechaaprobacion;
    
    @Column(name = "lugarentrega")
    @JsonProperty("lugarEntrega")
    private String lugarentrega;
    
    @Column(name = "instrucciones")
    @JsonProperty("instrucciones")
    private String instrucciones;
    
    @Basic(optional = false)
    @Column(name = "glosa")
    @JsonProperty("glosa")
    private String glosa;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "subtotal")
    @JsonProperty("subTotal")
    private BigDecimal subtotal;
    
    @Basic(optional = false)
    @Column(name = "totalcantidad")
    @JsonProperty("totalCantidad")
    private BigDecimal totalcantidad;
    
    @Basic(optional = false)
    @Column(name = "descuentos")
    private BigDecimal descuentos;
    
    @Basic(optional = false)
    @Column(name = "valorcompra")
    @JsonProperty("valorCompra")
    private BigDecimal valorcompra;
    
    @Basic(optional = false)
    @Column(name = "igvaplicado")
    @JsonProperty("igvAplicado")
    private BigDecimal igvaplicado;
    
    @Basic(optional = false)
    @Column(name = "importetotal")
    @JsonProperty("importeTotal")
    private BigDecimal importetotal;
    
    @Basic(optional = false)
    @Column(name = "usuarioregistro")
    @JsonProperty("usuarioRegistro")
    private String usuarioregistro;
    
    @Basic(optional = false)
    @Column(name = "usuarioactualiza")
    @JsonProperty("usuarioActualiza")
    private String usuarioactualiza;
    
    @Basic(optional = false)
    @Column(name = "fecharegistro")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaRegistro")
    private Date fecharegistro;
    
    @Basic(optional = false)
    @Column(name = "fechaactualiza")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaActualiza")
    private Date fechaactualiza;
    
    @JoinColumn(name = "cliente_idcliente", referencedColumnName = "idcliente")
    @ManyToOne(optional = false)
    @JsonProperty("cliente")
    private Cliente clienteIdcliente;
    
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    @ManyToOne(optional = false)
    @JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    @JoinColumn(name = "formapago_idformapago", referencedColumnName = "idformapago")
    @ManyToOne(optional = false)
    @JsonProperty("formaPago")
    private FormaPago formapagoIdformapago;
    
    @JoinColumn(name = "moneda_idmoneda", referencedColumnName = "idmoneda")
    @ManyToOne(optional = false)
    @JsonProperty("moneda")
    private Moneda monedaIdmoneda;
    
    @OneToMany(mappedBy = "ordenservicioIdordenservicio", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonProperty("liquidaciones")
    @JsonIgnoreProperties("ordenServicio")
    //@JsonIgnore
    private Set<Liquidacion> liquidacionSet;

    public OrdenServicio() {
    }

    public OrdenServicio(Integer idordenservicio) {
        this.idordenservicio = idordenservicio;
    }

    public OrdenServicio(Integer idordenservicio, String tipocod, String nroorden, int situacion, int estado, Date fechaorden, String glosa, BigDecimal subtotal, BigDecimal totalcantidad, BigDecimal descuentos, BigDecimal valorcompra, BigDecimal igvaplicado, BigDecimal importetotal, String usuarioregistro, String usuarioactualiza, Date fecharegistro, Date fechaactualiza) {
        this.idordenservicio = idordenservicio;
        this.tipocod = tipocod;
        this.nroorden = nroorden;
        this.situacion = situacion;
        this.estado = estado;
        this.fechaorden = fechaorden;
        this.glosa = glosa;
        this.subtotal = subtotal;
        this.totalcantidad = totalcantidad;
        this.descuentos = descuentos;
        this.valorcompra = valorcompra;
        this.igvaplicado = igvaplicado;
        this.importetotal = importetotal;
        this.usuarioregistro = usuarioregistro;
        this.usuarioactualiza = usuarioactualiza;
        this.fecharegistro = fecharegistro;
        this.fechaactualiza = fechaactualiza;
    }

    public Integer getIdordenservicio() {
        return idordenservicio;
    }

    public void setIdordenservicio(Integer idordenservicio) {
        this.idordenservicio = idordenservicio;
    }

    public String getTipocod() {
        return tipocod;
    }

    public void setTipocod(String tipocod) {
        this.tipocod = tipocod;
    }

    public String getNroorden() {
        return nroorden;
    }

    public void setNroorden(String nroorden) {
        this.nroorden = nroorden;
    }

    public int getSituacion() {
        return situacion;
    }

    public void setSituacion(int situacion) {
        this.situacion = situacion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Date getFechaorden() {
        return fechaorden;
    }

    public void setFechaorden(Date fechaorden) {
        this.fechaorden = fechaorden;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public Date getFechaaprobacion() {
        return fechaaprobacion;
    }

    public void setFechaaprobacion(Date fechaaprobacion) {
        this.fechaaprobacion = fechaaprobacion;
    }

    public String getLugarentrega() {
        return lugarentrega;
    }

    public void setLugarentrega(String lugarentrega) {
        this.lugarentrega = lugarentrega;
    }

    public String getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getTotalcantidad() {
        return totalcantidad;
    }

    public void setTotalcantidad(BigDecimal totalcantidad) {
        this.totalcantidad = totalcantidad;
    }

    public BigDecimal getDescuentos() {
        return descuentos;
    }

    public void setDescuentos(BigDecimal descuentos) {
        this.descuentos = descuentos;
    }

    public BigDecimal getValorcompra() {
        return valorcompra;
    }

    public void setValorcompra(BigDecimal valorcompra) {
        this.valorcompra = valorcompra;
    }

    public BigDecimal getIgvaplicado() {
        return igvaplicado;
    }

    public void setIgvaplicado(BigDecimal igvaplicado) {
        this.igvaplicado = igvaplicado;
    }

    public BigDecimal getImportetotal() {
        return importetotal;
    }

    public void setImportetotal(BigDecimal importetotal) {
        this.importetotal = importetotal;
    }

    public String getUsuarioregistro() {
        return usuarioregistro;
    }

    public void setUsuarioregistro(String usuarioregistro) {
        this.usuarioregistro = usuarioregistro;
    }

    public String getUsuarioactualiza() {
        return usuarioactualiza;
    }

    public void setUsuarioactualiza(String usuarioactualiza) {
        this.usuarioactualiza = usuarioactualiza;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Date getFechaactualiza() {
        return fechaactualiza;
    }

    public void setFechaactualiza(Date fechaactualiza) {
        this.fechaactualiza = fechaactualiza;
    }
    
    public Cliente getClienteIdcliente() {
        return clienteIdcliente;
    }

    public void setClienteIdcliente(Cliente clienteIdcliente) {
        this.clienteIdcliente = clienteIdcliente;
    }

    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    public FormaPago getFormapagoIdformapago() {
        return formapagoIdformapago;
    }

    public void setFormapagoIdformapago(FormaPago formapagoIdformapago) {
        this.formapagoIdformapago = formapagoIdformapago;
    }

    public Moneda getMonedaIdmoneda() {
        return monedaIdmoneda;
    }

    public void setMonedaIdmoneda(Moneda monedaIdmoneda) {
        this.monedaIdmoneda = monedaIdmoneda;
    }

    @XmlTransient
    public Set<Liquidacion> getLiquidacionSet() {
        return liquidacionSet;
    }

    public void setLiquidacionSet(Set<Liquidacion> liquidacionSet) {
        this.liquidacionSet = liquidacionSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idordenservicio != null ? idordenservicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenServicio)) {
            return false;
        }
        OrdenServicio other = (OrdenServicio) object;
        if ((this.idordenservicio == null && other.idordenservicio != null) || (this.idordenservicio != null && !this.idordenservicio.equals(other.idordenservicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.dto.Ordenservicio[ idordenservicio=" + idordenservicio + " ]";
    }
    
}
