/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "turno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Turno.findAll", query = "SELECT t FROM Turno t")
    , @NamedQuery(name = "Turno.findByIdturno", query = "SELECT t FROM Turno t WHERE t.idturno = :idturno")
    , @NamedQuery(name = "Turno.findByNombreturno", query = "SELECT t FROM Turno t WHERE t.nombreturno = :nombreturno")
    , @NamedQuery(name = "Turno.findByHorainicio", query = "SELECT t FROM Turno t WHERE t.horainicio = :horainicio")
    , @NamedQuery(name = "Turno.findByHorafin", query = "SELECT t FROM Turno t WHERE t.horafin = :horafin")})
public class Turno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idturno")
    @JsonProperty("id")
    private Integer idturno;
    @Basic(optional = false)
    @Column(name = "nombreturno")
    private String nombreturno;
    @Basic(optional = false)
    @Column(name = "horainicio")
    private String horainicio;
    @Basic(optional = false)
    @Column(name = "horafin")
    private String horafin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "turnoIdturno", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<TarifaRuta> tarifaRutaSet;

    public Turno() {
    }

    public Turno(Integer idturno) {
        this.idturno = idturno;
    }

    public Turno(Integer idturno, String nombreturno, String horainicio, String horafin) {
        this.idturno = idturno;
        this.nombreturno = nombreturno;
        this.horainicio = horainicio;
        this.horafin = horafin;
    }

    public Integer getIdturno() {
        return idturno;
    }

    public void setIdturno(Integer idturno) {
        this.idturno = idturno;
    }

    public String getNombreturno() {
        return nombreturno;
    }

    public void setNombreturno(String nombreturno) {
        this.nombreturno = nombreturno;
    }

    public String getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(String horainicio) {
        this.horainicio = horainicio;
    }

    public String getHorafin() {
        return horafin;
    }

    public void setHorafin(String horafin) {
        this.horafin = horafin;
    }

    @XmlTransient
    public Set<TarifaRuta> getTarifaRutaSet() {
        return tarifaRutaSet;
    }

    public void setTarifaRutaSet(Set<TarifaRuta> tarifaRutaSet) {
        this.tarifaRutaSet = tarifaRutaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idturno != null ? idturno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Turno)) {
            return false;
        }
        Turno other = (Turno) object;
        if ((this.idturno == null && other.idturno != null) || (this.idturno != null && !this.idturno.equals(other.idturno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Turno[ idturno=" + idturno + " ]";
    }
    
}
