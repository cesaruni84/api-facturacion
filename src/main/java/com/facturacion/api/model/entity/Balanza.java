/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "balanza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Balanza.findAll", query = "SELECT b FROM Balanza b")
    , @NamedQuery(name = "Balanza.findByIdbalanza", query = "SELECT b FROM Balanza b WHERE b.idbalanza = :idbalanza")
    , @NamedQuery(name = "Balanza.findByNombre", query = "SELECT b FROM Balanza b WHERE b.nombre = :nombre")
    , @NamedQuery(name = "Balanza.findByDescripcion", query = "SELECT b FROM Balanza b WHERE b.descripcion = :descripcion")})
public class Balanza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idbalanza")
    @JsonProperty("id")
    private Integer idbalanza;
    @Basic(optional = false)
    @Column(name = "nombre")
    @JsonProperty("nombre")
    private String nombre;
    
    @Basic(optional = false)
    @Column(name = "descripcion")
    @JsonProperty("descripcion")
    private String descripcion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "balanzaIdbalanza", fetch = FetchType.LAZY)
	@JsonIgnore
    private Set<GuiaRemision> guiaRemisionSet;

    public Balanza() {
    }

    public Balanza(Integer idbalanza) {
        this.idbalanza = idbalanza;
    }

    public Balanza(Integer idbalanza, String nombre, String descripcion) {
        this.idbalanza = idbalanza;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Integer getIdbalanza() {
        return idbalanza;
    }

    public void setIdbalanza(Integer idbalanza) {
        this.idbalanza = idbalanza;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

/*    @XmlTransient
    public Set<GuiaRemision> getGuiaRemisionSet() {
        return guiaRemisionSet;
    }

    public void setGuiaRemisionSet(Set<GuiaRemision> guiaRemisionSet) {
        this.guiaRemisionSet = guiaRemisionSet;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idbalanza != null ? idbalanza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Balanza)) {
            return false;
        }
        Balanza other = (Balanza) object;
        if ((this.idbalanza == null && other.idbalanza != null) || (this.idbalanza != null && !this.idbalanza.equals(other.idbalanza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Balanza[ idbalanza=" + idbalanza + " ]";
    }
    
}
