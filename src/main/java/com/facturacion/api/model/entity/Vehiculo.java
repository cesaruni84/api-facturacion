/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "vehiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculo.findAll", query = "SELECT v FROM Vehiculo v")
    , @NamedQuery(name = "Vehiculo.findByIdvehiculo", query = "SELECT v FROM Vehiculo v WHERE v.idvehiculo = :idvehiculo")
    , @NamedQuery(name = "Vehiculo.findByNroplacatracto", query = "SELECT v FROM Vehiculo v WHERE v.nroplacatracto = :nroplacatracto")
    , @NamedQuery(name = "Vehiculo.findByNroplacabombona", query = "SELECT v FROM Vehiculo v WHERE v.nroplacabombona = :nroplacabombona")
    , @NamedQuery(name = "Vehiculo.findByNrolicencia", query = "SELECT v FROM Vehiculo v WHERE v.nrolicencia = :nrolicencia")
    , @NamedQuery(name = "Vehiculo.findByEstadovehiculo", query = "SELECT v FROM Vehiculo v WHERE v.estadovehiculo = :estadovehiculo")
    , @NamedQuery(name = "Vehiculo.findByModelovehiculo", query = "SELECT v FROM Vehiculo v WHERE v.modelovehiculo = :modelovehiculo")
    , @NamedQuery(name = "Vehiculo.findByTienegarantia", query = "SELECT v FROM Vehiculo v WHERE v.tienegarantia = :tienegarantia")
    , @NamedQuery(name = "Vehiculo.findByFecharegistro", query = "SELECT v FROM Vehiculo v WHERE v.fecharegistro = :fecharegistro")
    , @NamedQuery(name = "Vehiculo.findByFechaactualiza", query = "SELECT v FROM Vehiculo v WHERE v.fechaactualiza = :fechaactualiza")})
public class Vehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idvehiculo")
    @JsonProperty("id")
    private Integer idvehiculo;
    
    @Basic(optional = false)
    @Column(name = "nroplacatracto")
    @JsonProperty("placaTracto")
    private String nroplacatracto;
    
    @Basic(optional = false)
    @Column(name = "nroplacabombona")
    @JsonProperty("placaBombona")
    private String nroplacabombona;
    
    @Basic(optional = false)
    @Column(name = "nrolicencia")
    @JsonProperty("licencia")
    private String nrolicencia;
    
    @Basic(optional = false)
    @Column(name = "estadovehiculo")
    @JsonProperty("estado")
    private int estadovehiculo;
    
    @Basic(optional = false)
    @Column(name = "modelovehiculo")
    @JsonProperty("modelo")
    private String modelovehiculo;
    
    @Basic(optional = false)
    @Column(name = "tienegarantia")
    @JsonProperty("garantia")
    private Character tienegarantia;
    
    @Basic(optional = false)
    @Column(name = "fecharegistro")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaRegistro")
    private Date fecharegistro;
    
    @Basic(optional = false)
    @Column(name = "fechaactualiza")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaActualiza")
    private Date fechaactualiza;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vehiculoIdvehiculo", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Transportista> transportistaSet;

    public Vehiculo() {
    }

    public Vehiculo(Integer idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    public Vehiculo(Integer idvehiculo, String nroplacatracto, String nroplacabombona, String nrolicencia, int estadovehiculo, String modelovehiculo, Character tienegarantia, Date fecharegistro, Date fechaactualiza) {
        this.idvehiculo = idvehiculo;
        this.nroplacatracto = nroplacatracto;
        this.nroplacabombona = nroplacabombona;
        this.nrolicencia = nrolicencia;
        this.estadovehiculo = estadovehiculo;
        this.modelovehiculo = modelovehiculo;
        this.tienegarantia = tienegarantia;
        this.fecharegistro = fecharegistro;
        this.fechaactualiza = fechaactualiza;
    }

    public Integer getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(Integer idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    public String getNroplacatracto() {
        return nroplacatracto;
    }

    public void setNroplacatracto(String nroplacatracto) {
        this.nroplacatracto = nroplacatracto;
    }

    public String getNroplacabombona() {
        return nroplacabombona;
    }

    public void setNroplacabombona(String nroplacabombona) {
        this.nroplacabombona = nroplacabombona;
    }

    public String getNrolicencia() {
        return nrolicencia;
    }

    public void setNrolicencia(String nrolicencia) {
        this.nrolicencia = nrolicencia;
    }

    public int getEstadovehiculo() {
        return estadovehiculo;
    }

    public void setEstadovehiculo(int estadovehiculo) {
        this.estadovehiculo = estadovehiculo;
    }

    public String getModelovehiculo() {
        return modelovehiculo;
    }

    public void setModelovehiculo(String modelovehiculo) {
        this.modelovehiculo = modelovehiculo;
    }

    public Character getTienegarantia() {
        return tienegarantia;
    }

    public void setTienegarantia(Character tienegarantia) {
        this.tienegarantia = tienegarantia;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Date getFechaactualiza() {
        return fechaactualiza;
    }

    public void setFechaactualiza(Date fechaactualiza) {
        this.fechaactualiza = fechaactualiza;
    }

    @XmlTransient
    public Set<Transportista> getTransportistaSet() {
        return transportistaSet;
    }

    public void setTransportistaSet(Set<Transportista> transportistaSet) {
        this.transportistaSet = transportistaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idvehiculo != null ? idvehiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculo)) {
            return false;
        }
        Vehiculo other = (Vehiculo) object;
        if ((this.idvehiculo == null && other.idvehiculo != null) || (this.idvehiculo != null && !this.idvehiculo.equals(other.idvehiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Vehiculo[ idvehiculo=" + idvehiculo + " ]";
    }
    
}
