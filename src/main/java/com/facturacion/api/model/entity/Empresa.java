/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
    , @NamedQuery(name = "Empresa.findByIdempresa", query = "SELECT e FROM Empresa e WHERE e.idempresa = :idempresa")
    , @NamedQuery(name = "Empresa.findByRazonsocial", query = "SELECT e FROM Empresa e WHERE e.razonsocial = :razonsocial")
    , @NamedQuery(name = "Empresa.findByNombrecomercial", query = "SELECT e FROM Empresa e WHERE e.nombrecomercial = :nombrecomercial")
    , @NamedQuery(name = "Empresa.findByNroruc", query = "SELECT e FROM Empresa e WHERE e.nroruc = :nroruc")
    , @NamedQuery(name = "Empresa.findByNrodni", query = "SELECT e FROM Empresa e WHERE e.nrodni = :nrodni")
    , @NamedQuery(name = "Empresa.findByDireccionfiscal", query = "SELECT e FROM Empresa e WHERE e.direccionfiscal = :direccionfiscal")
    , @NamedQuery(name = "Empresa.findByDireccioncomercial", query = "SELECT e FROM Empresa e WHERE e.direccioncomercial = :direccioncomercial")
    , @NamedQuery(name = "Empresa.findByEmail", query = "SELECT e FROM Empresa e WHERE e.email = :email")
    , @NamedQuery(name = "Empresa.findByNrotelefono1", query = "SELECT e FROM Empresa e WHERE e.nrotelefono1 = :nrotelefono1")
    , @NamedQuery(name = "Empresa.findByNrotelefono2", query = "SELECT e FROM Empresa e WHERE e.nrotelefono2 = :nrotelefono2")})
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idempresa")
    @JsonProperty("id")
    private Integer idempresa;
    
    @Basic(optional = false)
    @Column(name = "razonsocial")
    @JsonProperty("razonSocial")
    private String razonsocial;
    
    @Basic(optional = false)
    @Column(name = "nombrecomercial")
    @JsonProperty("comercial")
    private String nombrecomercial;
    
    @Basic(optional = false)
    @Column(name = "nroruc")
    @JsonProperty("ruc")
    private String nroruc;
    @Basic(optional = false)
    @Column(name = "nrodni")
    @JsonProperty("dni")
    private String nrodni;
    @Basic(optional = false)
    @Column(name = "direccionfiscal")
    @JsonProperty("dirFiscal")
    private String direccionfiscal;
    @Basic(optional = false)
    @Column(name = "direccioncomercial")
    @JsonProperty("dirComercial")
    private String direccioncomercial;
    @Basic(optional = false)
    @Column(name = "email")
    @JsonProperty("email")
    private String email;
    @Basic(optional = false)
    @Column(name = "nrotelefono1")
    @JsonProperty("telefono1")
    private String nrotelefono1;
    @Basic(optional = false)
    @Column(name = "nrotelefono2")
    @JsonProperty("telefono2")
    private String nrotelefono2;
    
/*    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<FacturaElectronica> facturaElectronicaSet;*/
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Transportista> transportistaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<GuiaRemision> guiaremisionSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Usuario> usuarioSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Liquidacion> liquidacionSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa")
    @JsonIgnore
    private Set<OrdenServicio> ordenservicioSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaIdempresa")
    @JsonIgnore
    private Set<Documento> documentoSet;
    
   
    public Empresa() {
    }

    public Empresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public Empresa(Integer idempresa, String razonsocial, String nombrecomercial, String nroruc, String nrodni, String direccionfiscal, String direccioncomercial, String email, String nrotelefono1, String nrotelefono2) {
        this.idempresa = idempresa;
        this.razonsocial = razonsocial;
        this.nombrecomercial = nombrecomercial;
        this.nroruc = nroruc;
        this.nrodni = nrodni;
        this.direccionfiscal = direccionfiscal;
        this.direccioncomercial = direccioncomercial;
        this.email = email;
        this.nrotelefono1 = nrotelefono1;
        this.nrotelefono2 = nrotelefono2;
    }

    public Integer getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getNombrecomercial() {
        return nombrecomercial;
    }

    public void setNombrecomercial(String nombrecomercial) {
        this.nombrecomercial = nombrecomercial;
    }

    public String getNroruc() {
        return nroruc;
    }

    public void setNroruc(String nroruc) {
        this.nroruc = nroruc;
    }

    public String getNrodni() {
        return nrodni;
    }

    public void setNrodni(String nrodni) {
        this.nrodni = nrodni;
    }

    public String getDireccionfiscal() {
        return direccionfiscal;
    }

    public void setDireccionfiscal(String direccionfiscal) {
        this.direccionfiscal = direccionfiscal;
    }

    public String getDireccioncomercial() {
        return direccioncomercial;
    }

    public void setDireccioncomercial(String direccioncomercial) {
        this.direccioncomercial = direccioncomercial;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNrotelefono1() {
        return nrotelefono1;
    }

    public void setNrotelefono1(String nrotelefono1) {
        this.nrotelefono1 = nrotelefono1;
    }

    public String getNrotelefono2() {
        return nrotelefono2;
    }

    public void setNrotelefono2(String nrotelefono2) {
        this.nrotelefono2 = nrotelefono2;
    }

/*    @XmlTransient
    public Set<FacturaElectronica> getFacturaElectronicaSet() {
        return facturaElectronicaSet;
    }

    public void setFacturaElectronicaSet(Set<FacturaElectronica> facturaElectronicaSet) {
        this.facturaElectronicaSet = facturaElectronicaSet;
    }*/

    @XmlTransient
    public Set<Transportista> getTransportistaSet() {
        return transportistaSet;
    }

    public void setTransportistaSet(Set<Transportista> transportistaSet) {
        this.transportistaSet = transportistaSet;
    }
    
    
    /**
	 * @return the guiaremisionSet
	 */
    @XmlTransient
	public Set<GuiaRemision> getGuiaremisionSet() {
		return guiaremisionSet;
	}

	/**
	 * @param guiaremisionSet the guiaremisionSet to set
	 */
	public void setGuiaremisionSet(Set<GuiaRemision> guiaremisionSet) {
		this.guiaremisionSet = guiaremisionSet;
	}

	/**
	 * @return the liquidacionSet
	 */
	@XmlTransient
	public Set<Liquidacion> getLiquidacionSet() {
		return liquidacionSet;
	}

	/**
	 * @param liquidacionSet the liquidacionSet to set
	 */
	public void setLiquidacionSet(Set<Liquidacion> liquidacionSet) {
		this.liquidacionSet = liquidacionSet;
	}
	
	

    @XmlTransient
    public Set<Documento> getDocumentoSet() {
        return documentoSet;
    }

    public void setDocumentoSet(Set<Documento> documentoSet) {
        this.documentoSet = documentoSet;
    }


    @XmlTransient
    public Set<OrdenServicio> getOrdenservicioSet() {
        return ordenservicioSet;
    }

    public void setOrdenservicioSet(Set<OrdenServicio> ordenservicioSet) {
        this.ordenservicioSet = ordenservicioSet;
    }
	

	@XmlTransient
    public Set<Usuario> getUsuarioSet() {
        return usuarioSet;
    }

    public void setUsuarioSet(Set<Usuario> usuarioSet) {
        this.usuarioSet = usuarioSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idempresa != null ? idempresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.idempresa == null && other.idempresa != null) || (this.idempresa != null && !this.idempresa.equals(other.idempresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Empresa[ idempresa=" + idempresa + " ]";
    }
    
}
