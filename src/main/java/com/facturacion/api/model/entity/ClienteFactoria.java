/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "clientefactoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClienteFactoria.findAll", query = "SELECT c FROM ClienteFactoria c")
    , @NamedQuery(name = "ClienteFactoria.findByIdclientefactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.idclientefactoria = :idclientefactoria")
    , @NamedQuery(name = "ClienteFactoria.findByCodigofactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.codigofactoria = :codigofactoria")
    , @NamedQuery(name = "ClienteFactoria.findByNombrefactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.nombrefactoria = :nombrefactoria")
    , @NamedQuery(name = "ClienteFactoria.findByDistritofactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.distritofactoria = :distritofactoria")
    , @NamedQuery(name = "ClienteFactoria.findByProvinciafactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.provinciafactoria = :provinciafactoria")
    , @NamedQuery(name = "ClienteFactoria.findByDepartamentofactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.departamentofactoria = :departamentofactoria")
    , @NamedQuery(name = "ClienteFactoria.findByReferenciafactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.referenciafactoria = :referenciafactoria")
    , @NamedQuery(name = "ClienteFactoria.findByEstadofactoria", query = "SELECT c FROM ClienteFactoria c WHERE c.estadofactoria = :estadofactoria")})
public class ClienteFactoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idclientefactoria")
    @JsonProperty("id")
    private Integer idclientefactoria;
    
    @Transient
    @JsonProperty("refLarga1")
    private String referenciaLargaFactoria1;
    
    @Transient
    @JsonProperty("refLarga2")
    private String referenciaLargaFactoria2;
    
    @Basic(optional = false)
    @Column(name = "codigofactoria")
    @JsonProperty("codigo")
    private String codigofactoria;
    
    @Basic(optional = false)
    @Column(name = "nombrefactoria")
    @JsonProperty("nombre")
    private String nombrefactoria;
    
    @Basic(optional = false)
    @Column(name = "distritofactoria")
    @JsonProperty("distrito")
    private String distritofactoria;
    
    @Basic(optional = false)
    @Column(name = "provinciafactoria")
    @JsonProperty("provincia")
    private String provinciafactoria;
    
    @Basic(optional = false)
    @Column(name = "departamentofactoria")
    @JsonProperty("departamento")
    private String departamentofactoria;
    
    @Basic(optional = false)
    @Column(name = "referenciafactoria")
    @JsonProperty("referencia")
    private String referenciafactoria;
    
    @Basic(optional = false)
    @Column(name = "estadofactoria")
    @JsonProperty("estado")
    private int estadofactoria;
   

    @JoinColumn(name = "cliente_idcliente", referencedColumnName = "idcliente")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("cliente")
    private Cliente clienteIdcliente;
    
    @Column(name = "rucfactoria")
    @JsonProperty("ruc")
    private String rucFactoria;
    
    @Column(name = "direfactoria")
    @JsonProperty("direccion")
    private String direFactoria;
    
    @Column(name = "tipofactoria")
    @JsonProperty("tipo")
    private String tipoFactoria;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientefactoriaIdorigen", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<TarifaRuta> tarifaRutaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientefactoriaIddestino", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<TarifaRuta> tarifaRutaSet1;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientefactoriaIddestinatario", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<GuiaRemision> guiaRemisionSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientefactoriaIdremitente", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<GuiaRemision> guiaRemisionSet1;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientefactoriaDestino", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Liquidacion> liquidacionSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientefactoriaOrigen", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Liquidacion> liquidacionSet1;

    public ClienteFactoria() {
    }

    public ClienteFactoria(Integer idclientefactoria) {
        this.idclientefactoria = idclientefactoria;
    }

    public ClienteFactoria(Integer idclientefactoria, String codigofactoria, String nombrefactoria, String distritofactoria, String provinciafactoria, String departamentofactoria, String referenciafactoria, int estadofactoria) {
        this.idclientefactoria = idclientefactoria;
        this.codigofactoria = codigofactoria;
        this.nombrefactoria = nombrefactoria;
        this.distritofactoria = distritofactoria;
        this.provinciafactoria = provinciafactoria;
        this.departamentofactoria = departamentofactoria;
        this.referenciafactoria = referenciafactoria;
        this.estadofactoria = estadofactoria;
    }

    public Integer getIdclientefactoria() {
        return idclientefactoria;
    }

    /**
	 * @return the referenciaLargaFactoria
	 */
	public String getReferenciaLargaFactoria1() {
		return (this.codigofactoria + " - " + this.nombrefactoria + " - " + this.distritofactoria );
	}

	/**
	 * @param referenciaLargaFactoria the referenciaLargaFactoria to set
	 */
	public void setReferenciaLargaFactoria1(String referenciaLargaFactoria1) {
		this.referenciaLargaFactoria1 = referenciaLargaFactoria1;
	}
	
    /**
	 * @return the referenciaLargaFactoria
	 */
	public String getReferenciaLargaFactoria2() {
		if (this.nombrefactoria.equals("?")) {
			return (this.clienteIdcliente.getRazonsocial());
		} else {
			return (this.clienteIdcliente.getRazonsocial() + " / " + this.nombrefactoria  );
		}
		
		
	}

	/**
	 * @param referenciaLargaFactoria the referenciaLargaFactoria to set
	 */
	public void setReferenciaLargaFactoria2(String referenciaLargaFactoria2) {
		this.referenciaLargaFactoria2 = referenciaLargaFactoria2;
	}

	public void setIdclientefactoria(Integer idclientefactoria) {
        this.idclientefactoria = idclientefactoria;
    }

    public String getCodigofactoria() {
        return codigofactoria;
    }

    public void setCodigofactoria(String codigofactoria) {
        this.codigofactoria = codigofactoria;
    }

    public String getNombrefactoria() {
        return nombrefactoria;
    }

    public void setNombrefactoria(String nombrefactoria) {
        this.nombrefactoria = nombrefactoria;
    }

    public String getDistritofactoria() {
        return distritofactoria;
    }

    public void setDistritofactoria(String distritofactoria) {
        this.distritofactoria = distritofactoria;
    }

    public String getProvinciafactoria() {
        return provinciafactoria;
    }

    public void setProvinciafactoria(String provinciafactoria) {
        this.provinciafactoria = provinciafactoria;
    }

    public String getDepartamentofactoria() {
        return departamentofactoria;
    }

    public void setDepartamentofactoria(String departamentofactoria) {
        this.departamentofactoria = departamentofactoria;
    }

    public String getReferenciafactoria() {
        return referenciafactoria;
    }

    public void setReferenciafactoria(String referenciafactoria) {
        this.referenciafactoria = referenciafactoria;
    }

    public int getEstadofactoria() {
        return estadofactoria;
    }

    public void setEstadofactoria(int estadofactoria) {
        this.estadofactoria = estadofactoria;
    }

	public Cliente getClienteIdcliente() {
        return clienteIdcliente;
    }

    public void setClienteIdcliente(Cliente clienteIdcliente) {
        this.clienteIdcliente = clienteIdcliente;
    }
    
    
    public String getRucFactoria() {
		return rucFactoria;
	}

	public void setRucFactoria(String rucFactoria) {
		this.rucFactoria = rucFactoria;
	}

	public String getDireFactoria() {
		return direFactoria;
	}


	public void setDireFactoria(String direFactoria) {
		this.direFactoria = direFactoria;
	}

	public String getTipoFactoria() {
		return tipoFactoria;
	}

	public void setTipoFactoria(String tipoFactoria) {
		this.tipoFactoria = tipoFactoria;
	}

    @XmlTransient
    public Set<TarifaRuta> getTarifaRutaSet() {
        return tarifaRutaSet;
    }

    public void setTarifaRutaSet(Set<TarifaRuta> tarifaRutaSet) {
        this.tarifaRutaSet = tarifaRutaSet;
    }

    @XmlTransient
    public Set<TarifaRuta> getTarifaRutaSet1() {
        return tarifaRutaSet1;
    }

    public void setTarifaRutaSet1(Set<TarifaRuta> tarifaRutaSet1) {
        this.tarifaRutaSet1 = tarifaRutaSet1;
    }

    @XmlTransient
    public Set<GuiaRemision> getGuiaRemisionSet() {
        return guiaRemisionSet;
    }

    public void setGuiaRemisionSet(Set<GuiaRemision> guiaRemisionSet) {
        this.guiaRemisionSet = guiaRemisionSet;
    }

    @XmlTransient
    public Set<GuiaRemision> getGuiaRemisionSet1() {
        return guiaRemisionSet1;
    }

    public void setGuiaRemisionSet1(Set<GuiaRemision> guiaRemisionSet1) {
        this.guiaRemisionSet1 = guiaRemisionSet1;
    }

    @XmlTransient
    public Set<Liquidacion> getLiquidacionSet() {
        return liquidacionSet;
    }

    public void setLiquidacionSet(Set<Liquidacion> liquidacionSet) {
        this.liquidacionSet = liquidacionSet;
    }

    @XmlTransient
    public Set<Liquidacion> getLiquidacionSet1() {
        return liquidacionSet1;
    }

    public void setLiquidacionSet1(Set<Liquidacion> liquidacionSet1) {
        this.liquidacionSet1 = liquidacionSet1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idclientefactoria != null ? idclientefactoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteFactoria)) {
            return false;
        }
        ClienteFactoria other = (ClienteFactoria) object;
        if ((this.idclientefactoria == null && other.idclientefactoria != null) || (this.idclientefactoria != null && !this.idclientefactoria.equals(other.idclientefactoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.ClienteFactoria[ idclientefactoria=" + idclientefactoria + " ]";
    }
    
}
