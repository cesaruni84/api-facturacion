/*
  * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "liquidacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Liquidacion.findAll", query = "SELECT l FROM Liquidacion l")
    , @NamedQuery(name = "Liquidacion.findByIdliquidacion", query = "SELECT l FROM Liquidacion l WHERE l.idliquidacion = :idliquidacion")
    , @NamedQuery(name = "Liquidacion.findByNrodocumentoliq", query = "SELECT l FROM Liquidacion l WHERE l.nrodocumentoliq = :nrodocumentoliq")
    , @NamedQuery(name = "Liquidacion.findByTipodocliq", query = "SELECT l FROM Liquidacion l WHERE l.tipodocliq = :tipodocliq")
    , @NamedQuery(name = "Liquidacion.findByFechaemisionliq", query = "SELECT l FROM Liquidacion l WHERE l.fechaemisionliq = :fechaemisionliq")
    , @NamedQuery(name = "Liquidacion.findByEstadoliq", query = "SELECT l FROM Liquidacion l WHERE l.estadoliq = :estadoliq")
    , @NamedQuery(name = "Liquidacion.findBySituacionliq", query = "SELECT l FROM Liquidacion l WHERE l.situacionliq = :situacionliq")
    , @NamedQuery(name = "Liquidacion.findByFechainitraslado", query = "SELECT l FROM Liquidacion l WHERE l.fechainitraslado = :fechainitraslado")
    , @NamedQuery(name = "Liquidacion.findByFechafintraslado", query = "SELECT l FROM Liquidacion l WHERE l.fechafintraslado = :fechafintraslado")
    , @NamedQuery(name = "Liquidacion.findByGlosaliq", query = "SELECT l FROM Liquidacion l WHERE l.glosaliq = :glosaliq")
    , @NamedQuery(name = "Liquidacion.findByMonedaliq", query = "SELECT l FROM Liquidacion l WHERE l.monedaliq = :monedaliq")
    , @NamedQuery(name = "Liquidacion.findByTotalcantidadliq", query = "SELECT l FROM Liquidacion l WHERE l.totalcantidadliq = :totalcantidadliq")
    , @NamedQuery(name = "Liquidacion.findBySubtotalliq", query = "SELECT l FROM Liquidacion l WHERE l.subtotalliq = :subtotalliq")
    , @NamedQuery(name = "Liquidacion.findByImporteigv", query = "SELECT l FROM Liquidacion l WHERE l.importeigv = :importeigv")
    , @NamedQuery(name = "Liquidacion.findByImportetotalliq", query = "SELECT l FROM Liquidacion l WHERE l.importetotalliq = :importetotalliq")
    , @NamedQuery(name = "Liquidacion.findByFecharegistrosistema", query = "SELECT l FROM Liquidacion l WHERE l.fecharegistrosistema = :fecharegistrosistema")
    , @NamedQuery(name = "Liquidacion.findByFechaactualizasistema", query = "SELECT l FROM Liquidacion l WHERE l.fechaactualizasistema = :fechaactualizasistema")
    , @NamedQuery(name = "Liquidacion.findByUsuarioregistro", query = "SELECT l FROM Liquidacion l WHERE l.usuarioregistro = :usuarioregistro")
    , @NamedQuery(name = "Liquidacion.findByUsuarioactualiza", query = "SELECT l FROM Liquidacion l WHERE l.usuarioactualiza = :usuarioactualiza")
    , @NamedQuery(name = "Liquidacion.findByNotas1", query = "SELECT l FROM Liquidacion l WHERE l.notas1 = :notas1")
    , @NamedQuery(name = "Liquidacion.findByNotas2", query = "SELECT l FROM Liquidacion l WHERE l.notas2 = :notas2")})

public class Liquidacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idliquidacion")
    @JsonProperty("id")
    private Integer idliquidacion;
    
    @Basic(optional = false)
    @Column(name = "tipodocliq")
    @JsonProperty("tipocod")
    private String tipodocliq;
    
    @Basic(optional = false)
    @Column(name = "nrodocumentoliq")
    @JsonProperty("nrodoc")
    private String nrodocumentoliq;
    
    @Basic(optional = false)
    @Column(name = "fechaemisionliq")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaEmision")
    private Date fechaemisionliq;
    
    @Basic(optional = false)
    @Column(name = "estadoliq")
    @JsonProperty("estado")
    private int estadoliq;
    
    @Basic(optional = false)
    @Column(name = "situacionliq")
    @JsonProperty("situacion")
    private int situacionliq;
    
    @Basic(optional = false)
    @Column(name = "fechainitraslado")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fecIniTraslado")
    private Date fechainitraslado;
    
    @Basic(optional = false)
    @Column(name = "fechafintraslado")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fecFinTraslado")
    private Date fechafintraslado;
    
    @Basic(optional = false)
    @Column(name = "glosaliq")
    @JsonProperty("glosa")
    private String glosaliq;
    
    @Basic(optional = false)
    @Column(name = "monedaliq")
    @JsonProperty("moneda")
    private int monedaliq;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "totalcantidadliq")
    @JsonProperty("totalCantidad")
    private BigDecimal totalcantidadliq;
    
    @Basic(optional = false)
    @Column(name = "subtotalliq")
    @JsonProperty("subTotalLiq")
    private BigDecimal subtotalliq;
    
    @Basic(optional = false)
    @Column(name = "importeigv")
    @JsonProperty("IGV")
    private BigDecimal importeigv;
    
    @Basic(optional = false)
    @Column(name = "importetotalliq")
    @JsonProperty("importeTotal")
    private BigDecimal importetotalliq;
    
    @Basic(optional = false)
    @Column(name = "fecharegistrosistema")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaRegistro")
    private Date fecharegistrosistema;
    
    @Basic(optional = false)
    @Column(name = "fechaactualizasistema")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("fechaActualiza")
    private Date fechaactualizasistema;
    
    @Basic(optional = false)
    @Column(name = "usuarioregistro")
    @JsonProperty("usuarioRegistro")
    private String usuarioregistro;
    
    @Basic(optional = false)
    @Column(name = "usuarioactualiza")
    @JsonProperty("usuarioActualiza")
    private String usuarioactualiza;
    
    @Column(name = "notas1")
    private String notas1;
    
    @Column(name = "notas2")
    private String notas2;
    
 
    @OneToMany(mappedBy = "liquidacionIdliquidacion", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonProperty("guias")
    @JsonIgnoreProperties("liquidacion")
    //@JsonIgnore
    private Set<GuiaRemision> guiaRemisionSet;
    
    @JoinColumn(name = "clientefactoria_origen", referencedColumnName = "idclientefactoria")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("origen")
    private ClienteFactoria clientefactoriaOrigen;
    
    @JoinColumn(name = "clientefactoria_destino", referencedColumnName = "idclientefactoria")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("destino")
    private ClienteFactoria clientefactoriaDestino;
    
    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne
    @JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    @JoinColumn(name = "motivotraslado_idmotivotraslado", referencedColumnName = "idmotivotraslado")
    @ManyToOne
    @JsonProperty("motivo")
    private MotivoTraslado motivotrasladoIdmotivotraslado;
    
    @JoinColumn(name = "ordenservicio_idordenservicio", referencedColumnName = "idordenservicio")
    @ManyToOne
    @JsonIgnoreProperties("liquidaciones") 
    //@JsonIgnoreProperties("liquidacionSet")
    @JsonProperty("ordenServicio")
    private OrdenServicio ordenservicioIdordenservicio;
    
    @JoinColumn(name = "documento_iddocumento", referencedColumnName = "iddocumento")
    @ManyToOne
    @JsonIgnoreProperties({"documentoitemSet","liquidacionSet","guiaRemisionSet"})
    @JsonProperty("documento")
    private Documento documentoIddocumento;
    

    public Liquidacion() {
    }

    public Liquidacion(Integer idliquidacion) {
        this.idliquidacion = idliquidacion;
    }

    public Liquidacion(Integer idliquidacion, String nrodocumentoliq, String tipodocliq, Date fechaemisionliq, int estadoliq, int situacionliq, Date fechainitraslado, Date fechafintraslado, String glosaliq, int monedaliq, BigDecimal totalcantidadliq, BigDecimal subtotalliq, BigDecimal importeigv, BigDecimal importetotalliq, Date fecharegistrosistema, Date fechaactualizasistema, String usuarioregistro, String usuarioactualiza) {
        this.idliquidacion = idliquidacion;
        this.nrodocumentoliq = nrodocumentoliq;
        this.tipodocliq = tipodocliq;
        this.fechaemisionliq = fechaemisionliq;
        this.estadoliq = estadoliq;
        this.situacionliq = situacionliq;
        this.fechainitraslado = fechainitraslado;
        this.fechafintraslado = fechafintraslado;
        this.glosaliq = glosaliq;
        this.monedaliq = monedaliq;
        this.totalcantidadliq = totalcantidadliq;
        this.subtotalliq = subtotalliq;
        this.importeigv = importeigv;
        this.importetotalliq = importetotalliq;
        this.fecharegistrosistema = fecharegistrosistema;
        this.fechaactualizasistema = fechaactualizasistema;
        this.usuarioregistro = usuarioregistro;
        this.usuarioactualiza = usuarioactualiza;
    }

    public Integer getIdliquidacion() {
        return idliquidacion;
    }

    public void setIdliquidacion(Integer idliquidacion) {
        this.idliquidacion = idliquidacion;
    }

    public String getNrodocumentoliq() {
        return nrodocumentoliq;
    }

    public void setNrodocumentoliq(String nrodocumentoliq) {
        this.nrodocumentoliq = nrodocumentoliq;
    }

    public String getTipodocliq() {
        return tipodocliq;
    }

    public void setTipodocliq(String tipodocliq) {
        this.tipodocliq = tipodocliq;
    }

    public Date getFechaemisionliq() {
        return fechaemisionliq;
    }

    public void setFechaemisionliq(Date fechaemisionliq) {
        this.fechaemisionliq = fechaemisionliq;
    }

    public int getEstadoliq() {
        return estadoliq;
    }

    public void setEstadoliq(int estadoliq) {
        this.estadoliq = estadoliq;
    }

    public int getSituacionliq() {
        return situacionliq;
    }

    public void setSituacionliq(int situacionliq) {
        this.situacionliq = situacionliq;
    }

    public Date getFechainitraslado() {
        return fechainitraslado;
    }

    public void setFechainitraslado(Date fechainitraslado) {
        this.fechainitraslado = fechainitraslado;
    }

    public Date getFechafintraslado() {
        return fechafintraslado;
    }

    public void setFechafintraslado(Date fechafintraslado) {
        this.fechafintraslado = fechafintraslado;
    }

    public String getGlosaliq() {
        return glosaliq;
    }

    public void setGlosaliq(String glosaliq) {
        this.glosaliq = glosaliq;
    }

    public int getMonedaliq() {
        return monedaliq;
    }

    public void setMonedaliq(int monedaliq) {
        this.monedaliq = monedaliq;
    }

    public BigDecimal getTotalcantidadliq() {
        return totalcantidadliq;
    }

    public void setTotalcantidadliq(BigDecimal totalcantidadliq) {
        this.totalcantidadliq = totalcantidadliq;
    }

    public BigDecimal getSubtotalliq() {
        return subtotalliq;
    }

    public void setSubtotalliq(BigDecimal subtotalliq) {
        this.subtotalliq = subtotalliq;
    }

    public BigDecimal getImporteigv() {
        return importeigv;
    }

    public void setImporteigv(BigDecimal importeigv) {
        this.importeigv = importeigv;
    }

    public BigDecimal getImportetotalliq() {
        return importetotalliq;
    }

    public void setImportetotalliq(BigDecimal importetotalliq) {
        this.importetotalliq = importetotalliq;
    }

    public Date getFecharegistrosistema() {
        return fecharegistrosistema;
    }

    public void setFecharegistrosistema(Date fecharegistrosistema) {
        this.fecharegistrosistema = fecharegistrosistema;
    }

    public Date getFechaactualizasistema() {
        return fechaactualizasistema;
    }

    public void setFechaactualizasistema(Date fechaactualizasistema) {
        this.fechaactualizasistema = fechaactualizasistema;
    }

    public String getUsuarioregistro() {
        return usuarioregistro;
    }

    public void setUsuarioregistro(String usuarioregistro) {
        this.usuarioregistro = usuarioregistro;
    }

    public String getUsuarioactualiza() {
        return usuarioactualiza;
    }

    public void setUsuarioactualiza(String usuarioactualiza) {
        this.usuarioactualiza = usuarioactualiza;
    }
    
    public String getNotas1() {
        return notas1;
    }

    public void setNotas1(String notas1) {
        this.notas1 = notas1;
    }

    public String getNotas2() {
        return notas2;
    }

    public void setNotas2(String notas2) {
        this.notas2 = notas2;
    }

    @XmlTransient
    public Set<GuiaRemision> getGuiaRemisionSet() {
        return guiaRemisionSet;
    }

    public void setGuiaRemisionSet(Set<GuiaRemision> guiaRemisionSet) {
        this.guiaRemisionSet = guiaRemisionSet;
    }

    public ClienteFactoria getClientefactoriaDestino() {
        return clientefactoriaDestino;
    }

    public void setClientefactoriaDestino(ClienteFactoria clientefactoriaDestino) {
        this.clientefactoriaDestino = clientefactoriaDestino;
    }
    
    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    public ClienteFactoria getClientefactoriaOrigen() {
        return clientefactoriaOrigen;
    }

    public void setClientefactoriaOrigen(ClienteFactoria clientefactoriaOrigen) {
        this.clientefactoriaOrigen = clientefactoriaOrigen;
    }

    public MotivoTraslado getMotivotrasladoIdmotivotraslado() {
        return motivotrasladoIdmotivotraslado;
    }

    public void setMotivotrasladoIdmotivotraslado(MotivoTraslado motivotrasladoIdmotivotraslado) {
        this.motivotrasladoIdmotivotraslado = motivotrasladoIdmotivotraslado;
    }
    
   
    public OrdenServicio getOrdenservicioIdordenservicio() {
        return ordenservicioIdordenservicio;
    }

    public void setOrdenservicioIdordenservicio(OrdenServicio ordenservicioIdordenservicio) {
        this.ordenservicioIdordenservicio = ordenservicioIdordenservicio;
    }
    
    public Documento getDocumentoIddocumento() {
        return documentoIddocumento;
    }

    public void setDocumentoIddocumento(Documento documentoIddocumento) {
        this.documentoIddocumento = documentoIddocumento;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idliquidacion != null ? idliquidacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Liquidacion)) {
            return false;
        }
        Liquidacion other = (Liquidacion) object;
        if ((this.idliquidacion == null && other.idliquidacion != null) || (this.idliquidacion != null && !this.idliquidacion.equals(other.idliquidacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Liquidacion[ idliquidacion=" + idliquidacion + " ]";
    }
    
}
