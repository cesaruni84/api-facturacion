/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "unidadmedida")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnidadMedida.findAll", query = "SELECT u FROM UnidadMedida u")
    , @NamedQuery(name = "UnidadMedida.findByIdunidadmedida", query = "SELECT u FROM UnidadMedida u WHERE u.idunidadmedida = :idunidadmedida")
    , @NamedQuery(name = "UnidadMedida.findByValor", query = "SELECT u FROM UnidadMedida u WHERE u.valor = :valor")
    , @NamedQuery(name = "UnidadMedida.findByDescripcion", query = "SELECT u FROM UnidadMedida u WHERE u.descripcion = :descripcion")})
public class UnidadMedida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idunidadmedida")
    @JsonProperty("id")
    private Integer idunidadmedida;
    
    @Basic(optional = false)
    @Column(name = "valor")
    @JsonProperty("valor")
    private String valor;
    
    @Basic(optional = false)
    @Column(name = "descripcion")
    @JsonProperty("descripcion")
    private String descripcion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadmedidaIdunidadmedida", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<GuiaRemisionProducto> guiaRemisionProductoSet;

    public UnidadMedida() {
    }

    public UnidadMedida(Integer idunidadmedida) {
        this.idunidadmedida = idunidadmedida;
    }

    public UnidadMedida(Integer idunidadmedida, String valor, String descripcion) {
        this.idunidadmedida = idunidadmedida;
        this.valor = valor;
        this.descripcion = descripcion;
    }

    public Integer getIdunidadmedida() {
        return idunidadmedida;
    }

    public void setIdunidadmedida(Integer idunidadmedida) {
        this.idunidadmedida = idunidadmedida;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Set<GuiaRemisionProducto> getGuiaRemisionProductoSet() {
        return guiaRemisionProductoSet;
    }

    public void setGuiaRemisionProductoSet(Set<GuiaRemisionProducto> guiaRemisionProductoSet) {
        this.guiaRemisionProductoSet = guiaRemisionProductoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idunidadmedida != null ? idunidadmedida.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnidadMedida)) {
            return false;
        }
        UnidadMedida other = (UnidadMedida) object;
        if ((this.idunidadmedida == null && other.idunidadmedida != null) || (this.idunidadmedida != null && !this.idunidadmedida.equals(other.idunidadmedida))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.UnidadMedida[ idunidadmedida=" + idunidadmedida + " ]";
    }
    
}
