/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
    , @NamedQuery(name = "Cliente.findByIdcliente", query = "SELECT c FROM Cliente c WHERE c.idcliente = :idcliente")
    , @NamedQuery(name = "Cliente.findByRazonsocial", query = "SELECT c FROM Cliente c WHERE c.razonsocial = :razonsocial")
    , @NamedQuery(name = "Cliente.findByNroruc", query = "SELECT c FROM Cliente c WHERE c.nroruc = :nroruc")
    , @NamedQuery(name = "Cliente.findByNombrecomercial", query = "SELECT c FROM Cliente c WHERE c.nombrecomercial = :nombrecomercial")
    , @NamedQuery(name = "Cliente.findByNrocontactotelef", query = "SELECT c FROM Cliente c WHERE c.nrocontactotelef = :nrocontactotelef")
    , @NamedQuery(name = "Cliente.findByEmailcliente", query = "SELECT c FROM Cliente c WHERE c.emailcliente = :emailcliente")
    , @NamedQuery(name = "Cliente.findByDirecliente", query = "SELECT c FROM Cliente c WHERE c.direcliente = :direcliente")
    , @NamedQuery(name = "Cliente.findByDistritodirecli", query = "SELECT c FROM Cliente c WHERE c.distritodirecli = :distritodirecli")
    , @NamedQuery(name = "Cliente.findByProvinciadirecli", query = "SELECT c FROM Cliente c WHERE c.provinciadirecli = :provinciadirecli")
    , @NamedQuery(name = "Cliente.findByDepartamentodirecli", query = "SELECT c FROM Cliente c WHERE c.departamentodirecli = :departamentodirecli")
    , @NamedQuery(name = "Cliente.findByFecharegistro", query = "SELECT c FROM Cliente c WHERE c.fecharegistro = :fecharegistro")})
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcliente")
    @JsonProperty("id")
    private Integer idcliente;
    
    @Basic(optional = false)
    @Column(name = "razonsocial")
    @JsonProperty("razonSocial")
    private String razonsocial;
    
    @Basic(optional = false)
    @Column(name = "nroruc")
    @JsonProperty("ruc")
    private String nroruc;
    
    @Basic(optional = false)
    @Column(name = "nombrecomercial")
    @JsonProperty("nombreComercial")
    private String nombrecomercial;
    
    @Basic(optional = false)
    @Column(name = "nrocontactotelef")
    @JsonProperty("telefono")
    private String nrocontactotelef;
    
    @Basic(optional = false)
    @Column(name = "emailcliente")
    @JsonProperty("email")
    private String emailcliente;
    
    @Basic(optional = false)
    @Column(name = "direcliente")
    @JsonProperty("direccion")
    private String direcliente;
    
    @Basic(optional = false)
    @Column(name = "distritodirecli")
    @JsonProperty("distrito")
    private String distritodirecli;
    
    @Basic(optional = false)
    @Column(name = "provinciadirecli")
    @JsonProperty("provincia")
    private String provinciadirecli;
    
    @Basic(optional = false)
    @Column(name = "departamentodirecli")
    @JsonProperty("departamento")
    private String departamentodirecli;
    
    @Basic(optional = false)
    @Column(name = "fecharegistro")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaRegistro")
    private Date fecharegistro;
    
    @JsonProperty("tipoDoc")
    @Column(name = "tipodoc")
    private Integer tipodoc;
    
/*    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clienteIdcliente", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<FacturaElectronica> facturaElectronicaSet;*/
    

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clienteIdcliente", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<ClienteFactoria> clienteFactoriaSet;

    public Cliente() {
    }

    public Cliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public Cliente(Integer idcliente, String razonsocial, String nroruc, String nombrecomercial, String nrocontactotelef, String emailcliente, String direcliente, String distritodirecli, String provinciadirecli, String departamentodirecli, Date fecharegistro) {
        this.idcliente = idcliente;
        this.razonsocial = razonsocial;
        this.nroruc = nroruc;
        this.nombrecomercial = nombrecomercial;
        this.nrocontactotelef = nrocontactotelef;
        this.emailcliente = emailcliente;
        this.direcliente = direcliente;
        this.distritodirecli = distritodirecli;
        this.provinciadirecli = provinciadirecli;
        this.departamentodirecli = departamentodirecli;
        this.fecharegistro = fecharegistro;
    }

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getNroruc() {
        return nroruc;
    }

    public void setNroruc(String nroruc) {
        this.nroruc = nroruc;
    }

    public String getNombrecomercial() {
        return nombrecomercial;
    }

    public void setNombrecomercial(String nombrecomercial) {
        this.nombrecomercial = nombrecomercial;
    }

    public String getNrocontactotelef() {
        return nrocontactotelef;
    }

    public void setNrocontactotelef(String nrocontactotelef) {
        this.nrocontactotelef = nrocontactotelef;
    }

    public String getEmailcliente() {
        return emailcliente;
    }

    public void setEmailcliente(String emailcliente) {
        this.emailcliente = emailcliente;
    }

    public String getDirecliente() {
        return direcliente;
    }

    public void setDirecliente(String direcliente) {
        this.direcliente = direcliente;
    }

    public String getDistritodirecli() {
        return distritodirecli;
    }

    public void setDistritodirecli(String distritodirecli) {
        this.distritodirecli = distritodirecli;
    }

    public String getProvinciadirecli() {
        return provinciadirecli;
    }

    public void setProvinciadirecli(String provinciadirecli) {
        this.provinciadirecli = provinciadirecli;
    }

    public String getDepartamentodirecli() {
        return departamentodirecli;
    }

    public void setDepartamentodirecli(String departamentodirecli) {
        this.departamentodirecli = departamentodirecli;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }
    
    public Integer getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(Integer tipodoc) {
        this.tipodoc = tipodoc;
    }

/*    @XmlTransient
    public Set<FacturaElectronica> getFacturaElectronicaSet() {
        return facturaElectronicaSet;
    }

    public void setFacturaElectronicaSet(Set<FacturaElectronica> facturaElectronicaSet) {
        this.facturaElectronicaSet = facturaElectronicaSet;
    }*/

    @XmlTransient
    public Set<ClienteFactoria> getClienteFactoriaSet() {
        return clienteFactoriaSet;
    }

    public void setClienteFactoriaSet(Set<ClienteFactoria> clienteFactoriaSet) {
        this.clienteFactoriaSet = clienteFactoriaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcliente != null ? idcliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.idcliente == null && other.idcliente != null) || (this.idcliente != null && !this.idcliente.equals(other.idcliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Cliente[ idcliente=" + idcliente + " ]";
    }
    
}
