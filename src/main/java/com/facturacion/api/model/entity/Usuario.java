/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByIdusuario", query = "SELECT u FROM Usuario u WHERE u.idusuario = :idusuario")
    , @NamedQuery(name = "Usuario.findByCodigousuario", query = "SELECT u FROM Usuario u WHERE u.codigousuario = :codigousuario")
    , @NamedQuery(name = "Usuario.findByNombres", query = "SELECT u FROM Usuario u WHERE u.nombres = :nombres")
    , @NamedQuery(name = "Usuario.findByApellidos", query = "SELECT u FROM Usuario u WHERE u.apellidos = :apellidos")
    , @NamedQuery(name = "Usuario.findByEmail", query = "SELECT u FROM Usuario u WHERE u.email = :email")
    , @NamedQuery(name = "Usuario.findByTelefono", query = "SELECT u FROM Usuario u WHERE u.telefono = :telefono")
    , @NamedQuery(name = "Usuario.findBySecretkey", query = "SELECT u FROM Usuario u WHERE u.secretkey = :secretkey")
    , @NamedQuery(name = "Usuario.findByCargo", query = "SELECT u FROM Usuario u WHERE u.cargo = :cargo")
    , @NamedQuery(name = "Usuario.findByFechaaltasistema", query = "SELECT u FROM Usuario u WHERE u.fechaaltasistema = :fechaaltasistema")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idusuario")
    @JsonProperty("id")
    private Integer idusuario;
    
    @Basic(optional = false)
    @Column(name = "codigousuario")
    @JsonProperty("codigo")
    private String codigousuario;
    
    @Basic(optional = false)
    @Column(name = "nombres")
    @JsonProperty("nombres")
    private String nombres;
    
    @Basic(optional = false)
    @Column(name = "apellidos")
    @JsonProperty("apellidos")
    private String apellidos;
    
    @Basic(optional = false)
    @Column(name = "email")
    @JsonProperty("email")
    private String email;
    
    @Basic(optional = false)
    @Column(name = "telefono")
    @JsonProperty("telefono")
    private String telefono;
    
    @Basic(optional = false)
    @Column(name = "secretkey")
    @JsonIgnore
    private String secretkey;
    
    @Basic(optional = false)
    @Column(name = "cargo")
    @JsonProperty("cargo")
    private String cargo;
    
    @Basic(optional = false)
    @Column(name = "fechaaltasistema")
    @Temporal(TemporalType.DATE)
    @JsonProperty("fechaAlta")
    private Date fechaaltasistema;


    @JoinColumn(name = "empresa_idempresa", referencedColumnName = "idempresa")
    @ManyToOne
	@JsonProperty("empresa")
    private Empresa empresaIdempresa;
    
    public Usuario() {
    }

    public Usuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public Usuario(Integer idusuario, String codigousuario, String nombres, String apellidos, String email, String telefono, String secretkey, String cargo, Date fechaaltasistema) {
        this.idusuario = idusuario;
        this.codigousuario = codigousuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.telefono = telefono;
        this.secretkey = secretkey;
        this.cargo = cargo;
        this.fechaaltasistema = fechaaltasistema;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public String getCodigousuario() {
        return codigousuario;
    }

    public void setCodigousuario(String codigousuario) {
        this.codigousuario = codigousuario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSecretkey() {
        return secretkey;
    }

    public void setSecretkey(String secretkey) {
        this.secretkey = secretkey;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Date getFechaaltasistema() {
        return fechaaltasistema;
    }

    public void setFechaaltasistema(Date fechaaltasistema) {
        this.fechaaltasistema = fechaaltasistema;
    }


    public Empresa getEmpresaIdempresa() {
        return empresaIdempresa;
    }

    public void setEmpresaIdempresa(Empresa empresaIdempresa) {
        this.empresaIdempresa = empresaIdempresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idusuario != null ? idusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.idusuario == null && other.idusuario != null) || (this.idusuario != null && !this.idusuario.equals(other.idusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.Usuario[ idusuario=" + idusuario + " ]";
    }
    
}
