/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturacion.api.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author CESAR
 */
@Entity
@Table(name = "motivotraslado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MotivoTraslado.findAll", query = "SELECT m FROM MotivoTraslado m")
    , @NamedQuery(name = "MotivoTraslado.findByIdmotivotraslado", query = "SELECT m FROM MotivoTraslado m WHERE m.idmotivotraslado = :idmotivotraslado")
    , @NamedQuery(name = "MotivoTraslado.findByCodmotivo", query = "SELECT m FROM MotivoTraslado m WHERE m.codmotivo = :codmotivo")
    , @NamedQuery(name = "MotivoTraslado.findByDescripmotivo", query = "SELECT m FROM MotivoTraslado m WHERE m.descripmotivo = :descripmotivo")})
public class MotivoTraslado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmotivotraslado")
    @JsonProperty("id")
    private Integer idmotivotraslado;
    
    @Basic(optional = false)
    @Column(name = "codmotivo")
    @JsonProperty("codigo")
    private String codmotivo;
    
    @Basic(optional = false)
    @Column(name = "descripmotivo")
    @JsonProperty("descripcion")
    private String descripmotivo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "motivotrasladoIdmotivotraslado", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Liquidacion> liquidacionSet;

    public MotivoTraslado() {
    }

    public MotivoTraslado(Integer idmotivotraslado) {
        this.idmotivotraslado = idmotivotraslado;
    }

    public MotivoTraslado(Integer idmotivotraslado, String codmotivo, String descripmotivo) {
        this.idmotivotraslado = idmotivotraslado;
        this.codmotivo = codmotivo;
        this.descripmotivo = descripmotivo;
    }

    public Integer getIdmotivotraslado() {
        return idmotivotraslado;
    }

    public void setIdmotivotraslado(Integer idmotivotraslado) {
        this.idmotivotraslado = idmotivotraslado;
    }

    public String getCodmotivo() {
        return codmotivo;
    }

    public void setCodmotivo(String codmotivo) {
        this.codmotivo = codmotivo;
    }

    public String getDescripmotivo() {
        return descripmotivo;
    }

    public void setDescripmotivo(String descripmotivo) {
        this.descripmotivo = descripmotivo;
    }

    @XmlTransient
    public Set<Liquidacion> getLiquidacionSet() {
        return liquidacionSet;
    }

    public void setLiquidacionSet(Set<Liquidacion> liquidacionSet) {
        this.liquidacionSet = liquidacionSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmotivotraslado != null ? idmotivotraslado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MotivoTraslado)) {
            return false;
        }
        MotivoTraslado other = (MotivoTraslado) object;
        if ((this.idmotivotraslado == null && other.idmotivotraslado != null) || (this.idmotivotraslado != null && !this.idmotivotraslado.equals(other.idmotivotraslado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.model.MotivoTraslado[ idmotivotraslado=" + idmotivotraslado + " ]";
    }
    
}
