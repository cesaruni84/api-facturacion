/**
 * 
 */
package com.facturacion.api.model.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author CESAR
 *
 */
public class GuiaRemisionDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("id")
	private Integer idGuiaRemision;
	
	@JsonProperty("nroguia")
	private String nroGuiaRemision;
	
	@JsonProperty("nroSecuencia")
	private String nroSecuencia;
	
	@JsonProperty("fechaEmision")
	private String fechaEmision;
	
	@JsonProperty("usuarioRegistra")
	private String usuarioRegistra;
	
	@JsonProperty("nroLiq")
	private String nroLiquidacionRemision;
	
	@JsonProperty("ordenServicio")
	private String nroOrdenServicio;

	@JsonProperty("remitente")
	private String razonSocialRemitente;
	
	@JsonProperty("destinatario")
	private String razonSocialDestinatario;
	
	@JsonProperty("estado")
	private String estadoGuiaRemision;
	
	@JsonProperty("producto")
	private String nombreProductoAsociado;
	
	@JsonProperty("cantidad")
	private String cantidadProductoAsociado;
	
	@JsonProperty("chofer")
	private String nombreChofer;
	
	@JsonProperty("nroGuiaCliente")
	private String nroGuiaRemisionCliente;

	@JsonProperty("fechaRecepcion")
	private String fechaRecepcionGuia;
	
	@JsonProperty("factura")
	private String codigoFactura;
	
	@JsonProperty("estadoFactura")
	private Integer estadoFactura;
	
	@JsonProperty("nemonicoEstadoFactura")
	private String nemonicoEstadoFactura;

	/**
	 * 
	 */
	public GuiaRemisionDTO() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the idGuiaRemision
	 */
	public Integer getIdGuiaRemision() {
		return idGuiaRemision;
	}


	/**
	 * @param idGuiaRemision the idGuiaRemision to set
	 */
	public void setIdGuiaRemision(Integer idGuiaRemision) {
		this.idGuiaRemision = idGuiaRemision;
	}


	/**
	 * @return the nroGuiaRemision
	 */
	public String getNroGuiaRemision() {
		return nroGuiaRemision;
	}


	/**
	 * @param nroGuiaRemision the nroGuiaRemision to set
	 */
	public void setNroGuiaRemision(String nroGuiaRemision) {
		this.nroGuiaRemision = nroGuiaRemision;
	}
	

	/**
	 * @return the nroSecuencia
	 */
	public String getNroSecuencia() {
		return nroSecuencia;
	}


	/**
	 * @param nroSecuencia the nroSecuencia to set
	 */
	public void setNroSecuencia(String nroSecuencia) {
		this.nroSecuencia = nroSecuencia;
	}


	/**
	 * @return the fechaEmision
	 */
	public String getFechaEmision() {
		return fechaEmision;
	}


	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}


	/**
	 * @return the usuarioRegistra
	 */
	public String getUsuarioRegistra() {
		return usuarioRegistra;
	}


	/**
	 * @param usuarioRegistra the usuarioRegistra to set
	 */
	public void setUsuarioRegistra(String usuarioRegistra) {
		this.usuarioRegistra = usuarioRegistra;
	}


	/**
	 * @return the nroLiquidacionRemision
	 */
	public String getNroLiquidacionRemision() {
		return nroLiquidacionRemision;
	}


	/**
	 * @param nroLiquidacionRemision the nroLiquidacionRemision to set
	 */
	public void setNroLiquidacionRemision(String nroLiquidacionRemision) {
		this.nroLiquidacionRemision = nroLiquidacionRemision;
	}


	/**
	 * @return the nroOrdenServicio
	 */
	public String getNroOrdenServicio() {
		return nroOrdenServicio;
	}


	/**
	 * @param nroOrdenServicio the nroOrdenServicio to set
	 */
	public void setNroOrdenServicio(String nroOrdenServicio) {
		this.nroOrdenServicio = nroOrdenServicio;
	}


	/**
	 * @return the razonSocialRemitente
	 */
	public String getRazonSocialRemitente() {
		return razonSocialRemitente;
	}


	/**
	 * @param razonSocialRemitente the razonSocialRemitente to set
	 */
	public void setRazonSocialRemitente(String razonSocialRemitente) {
		this.razonSocialRemitente = razonSocialRemitente;
	}


	/**
	 * @return the razonSocialDestinatario
	 */
	public String getRazonSocialDestinatario() {
		return razonSocialDestinatario;
	}


	/**
	 * @param razonSocialDestinatario the razonSocialDestinatario to set
	 */
	public void setRazonSocialDestinatario(String razonSocialDestinatario) {
		this.razonSocialDestinatario = razonSocialDestinatario;
	}


	/**
	 * @return the estadoGuiaRemision
	 */
	public String getEstadoGuiaRemision() {
		return estadoGuiaRemision;
	}


	/**
	 * @param estadoGuiaRemision the estadoGuiaRemision to set
	 */
	public void setEstadoGuiaRemision(String estadoGuiaRemision) {
		this.estadoGuiaRemision = estadoGuiaRemision;
	}


	/**
	 * @return the nombreProductoAsociado
	 */
	public String getNombreProductoAsociado() {
		return nombreProductoAsociado;
	}


	/**
	 * @param nombreProductoAsociado the nombreProductoAsociado to set
	 */
	public void setNombreProductoAsociado(String nombreProductoAsociado) {
		this.nombreProductoAsociado = nombreProductoAsociado;
	}


	/**
	 * @return the cantidadProductoAsociado
	 */
	public String getCantidadProductoAsociado() {
		return cantidadProductoAsociado;
	}


	/**
	 * @param cantidadProductoAsociado the cantidadProductoAsociado to set
	 */
	public void setCantidadProductoAsociado(String cantidadProductoAsociado) {
		this.cantidadProductoAsociado = cantidadProductoAsociado;
	}


	/**
	 * @return the nombreChofer
	 */
	public String getNombreChofer() {
		return nombreChofer;
	}


	/**
	 * @param nombreChofer the nombreChofer to set
	 */
	public void setNombreChofer(String nombreChofer) {
		this.nombreChofer = nombreChofer;
	}


	/**
	 * @return the fechaRecepcionGuia
	 */
	public String getFechaRecepcionGuia() {
		return fechaRecepcionGuia;
	}


	/**
	 * @param fechaRecepcionGuia the fechaRecepcionGuia to set
	 */
	public void setFechaRecepcionGuia(String fechaRecepcionGuia) {
		this.fechaRecepcionGuia = fechaRecepcionGuia;
	}


	/**
	 * @return the nroGuiaRemisionCliente
	 */
	public String getNroGuiaRemisionCliente() {
		return nroGuiaRemisionCliente;
	}


	/**
	 * @param nroGuiaRemisionCliente the nroGuiaRemisionCliente to set
	 */
	public void setNroGuiaRemisionCliente(String nroGuiaRemisionCliente) {
		this.nroGuiaRemisionCliente = nroGuiaRemisionCliente;
	}


	public String getCodigoFactura() {
		return codigoFactura;
	}


	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}


	public Integer getEstadoFactura() {
		return estadoFactura;
	}


	public void setEstadoFactura(Integer estadoFactura) {
		this.estadoFactura = estadoFactura;
	}


	public String getNemonicoEstadoFactura() {
		return nemonicoEstadoFactura;
	}


	public void setNemonicoEstadoFactura(String nemonicoEstadoFactura) {
		this.nemonicoEstadoFactura = nemonicoEstadoFactura;
	}
	
	
	
	

}
