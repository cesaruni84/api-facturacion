package com.facturacion.api.model.bo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.facturacion.api.model.entity.UnidadMedida;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Item implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("tipo")
	private TipoItem tipo;
	
	@JsonProperty("codigo")
	private String codigo;
	
	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("cantidad")
	private Double cantidad;
	
	@JsonProperty("unidadMedida")
	private UnidadMedida unidadMedida;
	
	@JsonProperty("tarifa")
	private BigDecimal tarifa;

	@JsonProperty("esOrdenServicio")
	private boolean esOrdenServicio;


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the tipo
	 */
	public TipoItem getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(TipoItem tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the cantidad
	 */
	public Double getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the unidadMedida
	 */
	public UnidadMedida getUnidadMedida() {
		return unidadMedida;
	}

	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(UnidadMedida unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	/**
	 * @return the tarifa
	 */
	public BigDecimal getTarifa() {
		return tarifa;
	}

	/**
	 * @param tarifa the tarifa to set
	 */
	public void setTarifa(BigDecimal tarifa) {
		this.tarifa = tarifa;
	}

	/**
	 * @return the esOrdenServicio
	 */
	public boolean isEsOrdenServicio() {
		return esOrdenServicio;
	}

	/**
	 * @param esOrdenServicio the esOrdenServicio to set
	 */
	public void setEsOrdenServicio(boolean esOrdenServicio) {
		this.esOrdenServicio = esOrdenServicio;
	}
	
	
	
	
	
	
	

}
