/**
 * 
 */
package com.facturacion.api.model.form;

import java.io.Serializable;

/**
 * @author CESAR
 *
 */
public class UsuarioForm implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String txtCodigoUsuario;
    private String txtClaveUsuario;
    private Integer txtCodigoEmpresa;
    
	/**
	 * @param txtCodigoUsuario
	 * @param txtClaveUsuario
	 */
	public UsuarioForm(String txtCodigoUsuario, String txtClaveUsuario, Integer txtCodigoEmpresa) {
		super();
		this.txtCodigoUsuario = txtCodigoUsuario;
		this.txtClaveUsuario = txtClaveUsuario;
		this.txtCodigoEmpresa = txtCodigoEmpresa;
	}

	/**
	 * 
	 */
	public UsuarioForm() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the txtCodigoUsuario
	 */
	public String getTxtCodigoUsuario() {
		return txtCodigoUsuario;
	}

	/**
	 * @param txtCodigoUsuario the txtCodigoUsuario to set
	 */
	public void setTxtCodigoUsuario(String txtCodigoUsuario) {
		this.txtCodigoUsuario = txtCodigoUsuario;
	}

	/**
	 * @return the txtClaveUsuario
	 */
	public String getTxtClaveUsuario() {
		return txtClaveUsuario;
	}

	/**
	 * @param txtClaveUsuario the txtClaveUsuario to set
	 */
	public void setTxtClaveUsuario(String txtClaveUsuario) {
		this.txtClaveUsuario = txtClaveUsuario;
	}

	/**
	 * @return the txtCodigoEmpresa
	 */
	public Integer getTxtCodigoEmpresa() {
		return txtCodigoEmpresa;
	}

	/**
	 * @param txtCodigoEmpresa the txtCodigoEmpresa to set
	 */
	public void setTxtCodigoEmpresa(Integer txtCodigoEmpresa) {
		this.txtCodigoEmpresa = txtCodigoEmpresa;
	}
    
    
    

}
