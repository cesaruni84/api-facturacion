package com.facturacion.api.model.filters;

import java.io.Serializable;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


public class FiltrosGuiaRemision implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Transient
	@JsonIgnore
	private Integer idEmpresaGuia;

	@JsonProperty("nroSerie")
	private String nroSerieRemisionFiltro;
	
	@JsonProperty("nroGuia")
	private String nroGuiaRemisionFiltro;
	
	@JsonProperty("fechaInicio")
	private String fechaInicioEmisionFiltro;
	
	@JsonProperty("fechaFin")
	private String fechaFinEmisionFiltro;
	
	@JsonProperty("estado")
	private Integer idEstadoFiltro;
	
	@JsonProperty("chofer")
	private Integer idChoferFiltro;
	
	@JsonProperty("destinatario")
	private Integer idDestinoFactoriaCliFiltro;

	@JsonProperty("facturado")
	private Integer indicadorFacturacionFiltro;

	
	public FiltrosGuiaRemision() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the idEmpresaGuia
	 */
	public Integer getIdEmpresaGuia() {
		return idEmpresaGuia;
	}



	/**
	 * @param idEmpresaGuia the idEmpresaGuia to set
	 */
	public void setIdEmpresaGuia(Integer idEmpresaGuia) {
		this.idEmpresaGuia = idEmpresaGuia;
	}



	/**
	 * @return the nroSerieRemisionFiltro
	 */
	public String getNroSerieRemisionFiltro() {
		return nroSerieRemisionFiltro;
	}



	/**
	 * @param nroSerieRemisionFiltro the nroSerieRemisionFiltro to set
	 */
	public void setNroSerieRemisionFiltro(String nroSerieRemisionFiltro) {
		this.nroSerieRemisionFiltro = nroSerieRemisionFiltro;
	}



	/**
	 * @return the nroGuiaRemisionFiltro
	 */
	public String getNroGuiaRemisionFiltro() {
		return nroGuiaRemisionFiltro;
	}



	/**
	 * @param nroGuiaRemisionFiltro the nroGuiaRemisionFiltro to set
	 */
	public void setNroGuiaRemisionFiltro(String nroGuiaRemisionFiltro) {
		this.nroGuiaRemisionFiltro = nroGuiaRemisionFiltro;
	}



	/**
	 * @return the fechaInicioEmisionFiltro
	 */
	public String getFechaInicioEmisionFiltro() {
		return fechaInicioEmisionFiltro;
	}



	/**
	 * @param fechaInicioEmisionFiltro the fechaInicioEmisionFiltro to set
	 */
	public void setFechaInicioEmisionFiltro(String fechaInicioEmisionFiltro) {
		this.fechaInicioEmisionFiltro = fechaInicioEmisionFiltro;
	}



	/**
	 * @return the fechaFinEmisionFiltro
	 */
	public String getFechaFinEmisionFiltro() {
		return fechaFinEmisionFiltro;
	}



	/**
	 * @param fechaFinEmisionFiltro the fechaFinEmisionFiltro to set
	 */
	public void setFechaFinEmisionFiltro(String fechaFinEmisionFiltro) {
		this.fechaFinEmisionFiltro = fechaFinEmisionFiltro;
	}



	/**
	 * @return the idEstadoFiltro
	 */
	public Integer getIdEstadoFiltro() {
		return idEstadoFiltro;
	}



	/**
	 * @param idEstadoFiltro the idEstadoFiltro to set
	 */
	public void setIdEstadoFiltro(Integer idEstadoFiltro) {
		this.idEstadoFiltro = idEstadoFiltro;
	}



	/**
	 * @return the idChoferFiltro
	 */
	public Integer getIdChoferFiltro() {
		return idChoferFiltro;
	}



	/**
	 * @param idChoferFiltro the idChoferFiltro to set
	 */
	public void setIdChoferFiltro(Integer idChoferFiltro) {
		this.idChoferFiltro = idChoferFiltro;
	}



	/**
	 * @return the idDestinoFactoriaCliFiltro
	 */
	public Integer getIdDestinoFactoriaCliFiltro() {
		return idDestinoFactoriaCliFiltro;
	}



	/**
	 * @param idDestinoFactoriaCliFiltro the idDestinoFactoriaCliFiltro to set
	 */
	public void setIdDestinoFactoriaCliFiltro(Integer idDestinoFactoriaCliFiltro) {
		this.idDestinoFactoriaCliFiltro = idDestinoFactoriaCliFiltro;
	}



	/**
	 * @return the indicadorFacturacionFiltro
	 */
	public Integer getIndicadorFacturacionFiltro() {
		return indicadorFacturacionFiltro;
	}



	/**
	 * @param indicadorFacturacionFiltro the indicadorFacturacionFiltro to set
	 */
	public void setIndicadorFacturacionFiltro(Integer indicadorFacturacionFiltro) {
		this.indicadorFacturacionFiltro = indicadorFacturacionFiltro;
	}
	
	
	
	
	
	

}
