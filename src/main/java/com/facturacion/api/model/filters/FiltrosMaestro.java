/**
 * 
 */
package com.facturacion.api.model.filters;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author CESAR
 *
 */
public class FiltrosMaestro implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonProperty("filtros_guias")
	private FiltrosGuiaRemision filtrosGuiaRemision;
	
	@JsonProperty("filtros_liq")
	private FiltrosLiquidacion filtrosLiquidacion;

	/**
	 * 
	 */
	public FiltrosMaestro() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the filtrosGuiaRemision
	 */
	public FiltrosGuiaRemision getFiltrosGuiaRemision() {
		return filtrosGuiaRemision;
	}


	/**
	 * @param filtrosGuiaRemision the filtrosGuiaRemision to set
	 */
	public void setFiltrosGuiaRemision(FiltrosGuiaRemision filtrosGuiaRemision) {
		this.filtrosGuiaRemision = filtrosGuiaRemision;
	}


	/**
	 * @return the filtrosLiquidacion
	 */
	public FiltrosLiquidacion getFiltrosLiquidacion() {
		return filtrosLiquidacion;
	}


	/**
	 * @param filtrosLiquidacion the filtrosLiquidacion to set
	 */
	public void setFiltrosLiquidacion(FiltrosLiquidacion filtrosLiquidacion) {
		this.filtrosLiquidacion = filtrosLiquidacion;
	}
	
	
	

}
