/**
 * 
 */
package com.facturacion.api.model.enums;

/**
 * @author CESAR
 *
 */
public enum SituacionLiquidacion {
	
	PENDIENTE(0), EN_PROCESO(1), PROCESADO(2);

	private Integer value;

	private SituacionLiquidacion(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }
}
