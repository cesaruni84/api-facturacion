/**
 * 
 */
package com.facturacion.api.model.enums;

/**
 * @author CESAR
 *
 */
public enum EstadoGuia {

	REGISTRADO(0), ANULADO(1) ,  BORRADO (2)  ;

	private Integer value;

	private EstadoGuia(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }
}
