package com.facturacion.api.model.enums;

public enum TipoDocumento {

	CON_ITEM(1), CON_LIQUIDACION(2), CON_GUIAREMISION(3);

	private Integer value;

	private TipoDocumento(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }
	
}
