package com.facturacion.api.model.enums;

public enum TipoFactura {
	
	TODOS(0), FACTURA(1), NOTA_CREDITO(2), NOTA_DEBITO(3), BOLETA_VENTA (4);

	private Integer value;

	private TipoFactura(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }
	

}
