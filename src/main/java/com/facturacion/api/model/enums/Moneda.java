/**
 * 
 */
package com.facturacion.api.model.enums;

/**
 * @author CESAR
 *
 */
public enum Moneda {

	SOLES(0), DOLARES(1), EUROS(2);

	private Integer value;

	private Moneda(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }

    public void setValue(Integer value) { 
    	this.value = value;
    }
    
}
