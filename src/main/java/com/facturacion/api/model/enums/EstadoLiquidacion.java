/**
 * 
 */
package com.facturacion.api.model.enums;

/**
 * @author CESAR
 *
 */
public enum EstadoLiquidacion {
	
	VIGENTE(1), VENCIDO(2);

	private Integer value;

	private EstadoLiquidacion(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }

}
