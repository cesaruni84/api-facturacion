package com.facturacion.api.model.enums;

public enum BusquedaGuias {
	
	TODOS(0), GUIAS_FACTURADAS(1), GUIAS_NO_FACTURADAS (2)  ;

	private Integer value;

	private BusquedaGuias(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }
}
