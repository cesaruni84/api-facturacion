package com.facturacion.api.model.enums;

public enum BusquedaLiquidacion {
	
	TODOS(0), LIQ_FACTURADAS(1) ,  LIQ_NO_FACTURADAS (2)  ;

	private Integer value;

	private BusquedaLiquidacion(Integer value) {
		this.value = value;
	}
	
    public int getValue() { 
    	return value; 
    }
}
