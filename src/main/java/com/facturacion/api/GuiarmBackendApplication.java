package com.facturacion.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuiarmBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuiarmBackendApplication.class, args);
	}
}
