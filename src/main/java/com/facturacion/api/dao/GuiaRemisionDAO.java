package com.facturacion.api.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.facturacion.api.model.entity.GuiaRemision;


@Transactional
@Repository
public interface GuiaRemisionDAO extends CrudRepository<GuiaRemision,Long>,JpaSpecificationExecutor<GuiaRemision>{

}
