/**
 * 
 */
package com.facturacion.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.facturacion.api.dao.IGuiaRemisionDAO;
import com.facturacion.api.model.entity.ClienteFactoria;
import com.facturacion.api.model.entity.Empresa;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Transportista;
import com.facturacion.api.service.liquidacion.impl.SearchCriteria;

/**
 * @author CESAR
 *
 */

@Transactional
@Repository
public class GuiaRemisionDAO implements IGuiaRemisionDAO {

	@PersistenceContext	
	private EntityManager entityManager;	
	
	
	private Root<GuiaRemision> rootGuiaRemision;
	
	/* (non-Javadoc)
	 * @see com.facturacion.api.dao.IGuiaRemisionDAO#buscarPorFiltros(java.util.List)
	 */
	@Override
	public List<GuiaRemision> buscarPorFiltros(List<SearchCriteria> params) {
		// TODO Auto-generated method stub
		
		CriteriaBuilder builder  = entityManager.getCriteriaBuilder(); 
	    CriteriaQuery<GuiaRemision> query = builder.createQuery(GuiaRemision.class);
	    rootGuiaRemision = query.from(GuiaRemision.class);		
		
	    //List<Predicate> predicates = new ArrayList<Predicate>(); 
	    Predicate myPredicate = getPredicateForBuilder(builder,params);
	    
	    //Join<GuiaRemision,ClienteFactoria> join  = rootGuiaRemision.join("clientefactoriaIddestinatario",JoinType.LEFT);
	    //Predicate myPredicate2= builder.and(builder.equal(join.get("idclientefactoria"), 1));
	    
	    //Prepara Query
	    query.select(rootGuiaRemision);
        query.where(myPredicate);
        query.orderBy(builder.desc(rootGuiaRemision.get("nroserieguia")),
        				builder.desc(rootGuiaRemision.get("nrosecuenciaguia")));
        
        //Prepara para lista de salida
        TypedQuery<GuiaRemision> typedQueryGuias = entityManager.createQuery(query);
        List<GuiaRemision> listaGuiaRemisionDTO = typedQueryGuias.getResultList();
        
        return listaGuiaRemisionDTO;
	}
	

	private Predicate getPredicateForBuilder(CriteriaBuilder builder,List<SearchCriteria> params) {
		
	    //Crea un predicado
	    Predicate predicate = builder.conjunction();
		
	    for (SearchCriteria param : params) {
            if (param.getOperation().equalsIgnoreCase(">")) {
                predicate = builder.and(predicate, 
                  builder.greaterThanOrEqualTo(rootGuiaRemision.get(param.getKey()), 
                  param.getValue().toString()));
            } else if (param.getOperation().equalsIgnoreCase("<")) {
                predicate = builder.and(predicate, 
                  builder.lessThanOrEqualTo(rootGuiaRemision.get(param.getKey()), 
                  param.getValue().toString()));
            } else if (param.getOperation().equalsIgnoreCase(":")) {
                if (rootGuiaRemision.get(param.getKey()).getJavaType() == String.class) {
                    predicate = builder.and(predicate, 
                      builder.like(rootGuiaRemision.get(param.getKey()), 
                      "%" + param.getValue() + "%"));
                } else {
                	if (param.getKey().contains("clientefactoria")) {
                	    Join<GuiaRemision,ClienteFactoria> join1  = rootGuiaRemision.join("clientefactoriaIddestinatario",JoinType.LEFT);
                	    //Predicate myPredicate2= builder.and(builder.equal(join.get("idclientefactoria"), 1));
                        predicate = builder.and(predicate, builder.equal(join1.get("idclientefactoria"), param.getValue()));
                	}else if (param.getKey().contains("transportista")){
                	    Join<GuiaRemision,Transportista> join2  = rootGuiaRemision.join("transportistaIdtransportista",JoinType.LEFT);
                	    //Predicate myPredicate2= builder.and(builder.equal(join.get("idclientefactoria"), 1));
                        predicate = builder.and(predicate, builder.equal(join2.get("idtransportista"), param.getValue()));
                	}else if (param.getKey().contains("empresa"))											{
                	    Join<GuiaRemision,Empresa> join3  = rootGuiaRemision.join("empresaIdempresa",JoinType.LEFT);
                	    //Predicate myPredicate2= builder.and(builder.equal(join.get("idclientefactoria"), 1));
                        predicate = builder.and(predicate, builder.equal(join3.get("idempresa"), param.getValue()));
                	}else	{
                		predicate = builder.and(predicate, builder.equal(rootGuiaRemision.get(param.getKey()), param.getValue()));
                	}

                }
            }
        }
	    
	    return predicate;
	}

}
