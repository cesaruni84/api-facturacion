/**
 * 
 */
package com.facturacion.api.dao;

import java.util.List;

import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.service.liquidacion.impl.SearchCriteria;

/**
 * @author CESAR
 *
 */
public interface IGuiaRemisionDAO {

	
	List<GuiaRemision> buscarPorFiltros(List<SearchCriteria> params);

}
