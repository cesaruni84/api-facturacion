/**
 * 
 */
package com.facturacion.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.facturacion.api.model.entity.UnidadMedida;

/**
 * @author CESAR
 *
 */
@Repository
public interface UnidadMedidaRepository extends JpaRepository <UnidadMedida, Integer>{

}
