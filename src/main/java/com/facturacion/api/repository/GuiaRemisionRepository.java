/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.facturacion.api.model.entity.GuiaRemision;

/**
 * @author CESAR
 *
 */
@Repository
public interface GuiaRemisionRepository extends JpaRepository<GuiaRemision, Integer>{
	
	@Query("SELECT g FROM GuiaRemision g"
			+ " WHERE g.empresaIdempresa.idempresa = ?1 "
			+ " ORDER BY g.nroserieguia DESC , g.nrosecuenciaguia DESC")
	List<GuiaRemision> listarPorEmpresa(Integer idEmpresa);
	
	@Query("SELECT g FROM GuiaRemision g WHERE g.idguiaremision = ?1 AND g.empresaIdempresa.idempresa = ?2")
	Optional<GuiaRemision> obtenerGuiaRemision(Integer idGuiaRemision, Integer idEmpresa);
	
	@Query("SELECT g FROM GuiaRemision g WHERE g.nroserieguia = ?1 AND g.nrosecuenciaguia = ?2 AND g.empresaIdempresa.idempresa = ?3")
	Optional<GuiaRemision> obtenerGuiaRemisionPorNroGuia(String nroGuia, String nroSecuencia, Integer idEmpresa);
	
	@Query("SELECT g FROM GuiaRemision g WHERE g.nroserieguiacliente = ?1 AND g.nrosecuenguiacliente = ?2 AND g.empresaIdempresa.idempresa = ?3")
	Optional<GuiaRemision> obtenerGuiasPorGuiaCliente(String nroGuiaCli, String nroSecuenciaClie, Integer idEmpresa);
	
	@Query("SELECT g FROM GuiaRemision g WHERE g.nroserieguiacliente LIKE %:nroSerieCli  AND g.empresaIdempresa.idempresa = :idEmpresa"
			+ " AND ( g.fecharemision >= :fechaIni	AND g.fecharemision <= :fechaFin) "
			+ " ORDER BY g.fecharemision DESC, g.nroserieguia DESC , g.nrosecuenciaguia DESC")
	List<GuiaRemision> listarGuiasPorSerieCliente(@Param("nroSerieCli")String nroSerieCli, 
													@Param("idEmpresa") Integer idEmpresa,
													@Param("fechaIni") Date fechaIni, 
													@Param("fechaFin") Date fechaFin);
	
	@Query("SELECT g FROM GuiaRemision g WHERE g.nroserieguia LIKE %:nroSerie  AND g.empresaIdempresa.idempresa = :idEmpresa"
			+ " AND ( g.fecharemision >= :fechaIni	AND g.fecharemision <= :fechaFin) "
			+ " ORDER BY g.fecharemision DESC, g.nroserieguia DESC , g.nrosecuenciaguia DESC")
	List<GuiaRemision> listarGuiasPorSerieGuia(@Param("nroSerie")String nroSerie, 
												@Param("idEmpresa") Integer idEmpresa,
												@Param("fechaIni") Date fechaIni, 
												@Param("fechaFin") Date fechaFin);
	
    @Transactional
    @Modifying(clearAutomatically = true)
	@Query("UPDATE GuiaRemision g SET g.liquidacionIdliquidacion.idliquidacion = NULL "
		+ "WHERE g.idguiaremision = ?1 AND g.liquidacionIdliquidacion.idliquidacion = ?2")
	void actualizarGuiaLiquidacion(Integer idGuiaRemision, Integer idliquidacion);
    
    @Transactional
    @Modifying(clearAutomatically = true)
	@Query("UPDATE GuiaRemision g SET g.liquidacionIdliquidacion.idliquidacion = NULL "
		+ "WHERE g.liquidacionIdliquidacion.idliquidacion = ?1")
	void liberarGuiasDeLiquidacion(Integer idliquidacion);
    
    
    @Transactional
    @Modifying(clearAutomatically = true)
	@Query("UPDATE GuiaRemision g SET g.documentoIddocumento.iddocumento = NULL "
		+ "WHERE g.documentoIddocumento.iddocumento = ?1")
	void liberarGuiaDocumento(Integer idDocumento);
	
	
	@Query("SELECT g FROM GuiaRemision g"
			+ " WHERE g.clientefactoriaIdremitente.idclientefactoria = :idOrigen "
			+ " AND g.clientefactoriaIddestinatario.idclientefactoria = :idDestino "
			+ " AND g.estadoguia = :estado "
			+ " AND ( g.fecharemision >= :fechaIni	AND g.fecharemision <= :fechaFin) "
			+ " AND g.liquidacionIdliquidacion.idliquidacion IS NULL "
			+ " AND g.empresaIdempresa.idempresa = :idEmpresa "
			+ " ORDER BY g.fecharemision DESC, g.nroserieguia DESC , g.nrosecuenciaguia DESC")
	List<GuiaRemision> listarGuiasNoLiquidadasxEmpresa(@Param("idOrigen") Integer idOrigen,
														@Param("idDestino") Integer idDestino,
														@Param("estado") Integer ESTADO,
														@Param("fechaIni") Date fechaIni, 
														@Param("fechaFin") Date fechaFin, 
														@Param("idEmpresa") Integer idEmpresa );
	
	@Query("SELECT g FROM GuiaRemision g"
			+ " WHERE (g.clientefactoriaIdremitente.idclientefactoria >= :idOrigenMin "
			+ " AND g.clientefactoriaIdremitente.idclientefactoria <= :idOrigenMax) "
			+ " AND (g.clientefactoriaIddestinatario.idclientefactoria >= :idDestinoMin "
			+ " AND g.clientefactoriaIddestinatario.idclientefactoria <= :idDestinoMax) "
			+ " AND g.estadoguia = 0 "
			+ " AND ( g.fecharemision >= :fechaIni	AND g.fecharemision <= :fechaFin) "
			+ " AND g.liquidacionIdliquidacion.idliquidacion IS NULL "
			+ " AND g.documentoIddocumento.iddocumento IS NULL "
			+ " AND g.empresaIdempresa.idempresa = :idEmpresa "
			+ " ORDER BY g.fecharemision DESC, g.nroserieguia DESC , g.nrosecuenciaguia DESC")
	List<GuiaRemision> listarGuiasNoFacturadasxEmpresa(@Param("idEmpresa") Integer idEmpresa,
														@Param("idOrigenMin") Integer idOrigenMin,@Param("idOrigenMax") Integer idOrigenMax,
														@Param("idDestinoMin") Integer idDestinoMin,@Param("idDestinoMax") Integer idDestinoMax,
														@Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin);



}
