/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.Cliente;

/**
 * @author CESAR
 *
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Integer>{
	
	@Query("SELECT c FROM Cliente c "
			+ "WHERE c.tipodoc = ?1  AND c.nroruc = ?2 ")
	Optional<Cliente> obtenerClientePorNroDOC(Integer tipoDoc, String nroDoc);

}
