/**
 * 
 */
package com.facturacion.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.facturacion.api.model.entity.Balanza;

/**
 * @author CESAR
 *
 */
@Repository
public interface BalanzaRepository extends JpaRepository<Balanza, Integer>{

}
