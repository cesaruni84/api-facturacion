/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.OrdenServicio;

/**
 * @author CESAR
 *
 */
@Repository
public interface OrdenServicioRepository extends JpaRepository<OrdenServicio, Integer>{
	
	@Query("SELECT o FROM OrdenServicio o WHERE o.nroorden = ?1 ")
	Optional<OrdenServicio> obtenerPorNro(String nroOrdenServicio);
	
	@Query("SELECT o FROM OrdenServicio o"
			+ " WHERE o.empresaIdempresa.idempresa = ?1"
			+ " ORDER BY o.nroorden DESC")
	List<OrdenServicio> listarPorEmpresa(Integer idEmpresa);
	

	@Query("SELECT o FROM OrdenServicio o"
			+ " WHERE o.empresaIdempresa.idempresa = :idEmpresa "
			+ " AND ( o.estado 	>= :idEstadoMin AND o.estado <= :idEstadoMax)"
			+ " AND ( o.fechaorden 	>= :fechaIni AND o.fechaorden <= :fechaFin)"
			+ " ORDER BY o.nroorden DESC")
	List<OrdenServicio> listarOrdenesServicioNoFacturadas(@Param("idEmpresa") Integer idEmpresa, 
															@Param("idEstadoMin") Integer idEstadoMin,
															@Param("idEstadoMax") Integer idEstadoMax,
															@Param("fechaIni") Date fechaIni, 
															@Param("fechaFin") Date fechaFin);

}
