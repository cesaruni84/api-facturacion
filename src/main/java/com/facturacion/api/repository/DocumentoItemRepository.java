package com.facturacion.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.DocumentoItem;


@Repository
public interface DocumentoItemRepository extends JpaRepository<DocumentoItem,Integer>{
	
}
