/**
 * 
 */
package com.facturacion.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.facturacion.api.model.entity.GuiaRemisionProducto;

/**
 * @author CESAR
 *
 */
@Repository
public interface GuiaRemisionProductoRepository extends JpaRepository<GuiaRemisionProducto, Integer>{

}
