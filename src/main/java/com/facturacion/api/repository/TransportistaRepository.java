/**
 * 
 */
package com.facturacion.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.Transportista;

/**
 * @author CESAR
 *
 */
@Repository
public interface TransportistaRepository extends JpaRepository<Transportista, Integer> {
	
	@Query("SELECT t FROM Transportista t "
			+ "WHERE t.empresaIdempresa.idempresa = ?1 "
			+ "ORDER BY t.nombres, t.apellidos ASC")
	List<Transportista> listarChoferesPorEmpresa(Integer idEmpresa);


}
