/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.facturacion.api.model.entity.Liquidacion;

/**
 * @author CESAR
 *
 */
@Repository
public interface LiquidacionRepository extends JpaRepository<Liquidacion, Integer> {
	

	@Query("SELECT l FROM Liquidacion l"
			+ " WHERE l.empresaIdempresa.idempresa = ?1 "
			+ " ORDER BY l.fechaemisionliq DESC")
	List<Liquidacion> listarPorEmpresa(Integer idEmpresa);
	
	@Query("SELECT l FROM Liquidacion l WHERE l.idliquidacion = ?1 AND l.empresaIdempresa.idempresa = ?2")
	Optional<Liquidacion> obtenerLiquidacionPorIdPorEmpresa(Integer idLiquidacion, Integer idEmpresa);
	
	@Query("SELECT l FROM Liquidacion l WHERE l.nrodocumentoliq = ?1 "
			+ "AND l.empresaIdempresa.idempresa = ?2 "
			+ "AND l.estadoliq = ?3 ")
	Optional<Liquidacion> obtenerLiquidacionPorNroDOC(String nroDocLiquidacion, Integer idEmpresa, Integer idEstado);
	
    @Transactional
    @Modifying(clearAutomatically = true)
	@Query("UPDATE Liquidacion l SET l.documentoIddocumento.iddocumento = NULL "
		+ "WHERE l.documentoIddocumento.iddocumento = ?1")
	void liberarLiquidacionDocumento(Integer idDocumento);
    
	@Query("SELECT l FROM Liquidacion l"
			+ " WHERE l.empresaIdempresa.idempresa = :idEmpresa"
			+ " AND (l.clientefactoriaOrigen.idclientefactoria >= :idOrigenMin"
			+ " AND  l.clientefactoriaOrigen.idclientefactoria <= :idOrigenMax)"
			+ " AND (l.clientefactoriaDestino.idclientefactoria >= :idDestinoMin"
			+ " AND  l.clientefactoriaDestino.idclientefactoria <= :idDestinoMax)"
			+ " AND (l.estadoliq >= :idEstadoMin AND l.estadoliq <= :idEstadoMax)"
			+ " AND (l.fechaemisionliq >= :fechaIni AND l.fechaemisionliq <= :fechaFin)"
			+ " AND l.documentoIddocumento.iddocumento IS NOT NULL"
			+ " ORDER BY l.fechaemisionliq DESC, l.idliquidacion DESC")
	List<Liquidacion> listarLiquidacionesFacturadasPorFiltro(@Param("idEmpresa") Integer idEmpresa,
													@Param("idOrigenMin") Integer idOrigenMin,@Param("idOrigenMax") Integer idOrigenMax,
													@Param("idDestinoMin") Integer idDestinoMin,@Param("idDestinoMax") Integer idDestinoMax,
													@Param("idEstadoMin") Integer idEstadoMin,@Param("idEstadoMax") Integer idEstadoMax,
													@Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin);
	
	
	@Query("SELECT l FROM Liquidacion l"
			+ " WHERE l.empresaIdempresa.idempresa = :idEmpresa"
			+ " AND (l.clientefactoriaOrigen.idclientefactoria >= :idOrigenMin"
			+ " AND  l.clientefactoriaOrigen.idclientefactoria <= :idOrigenMax)"
			+ " AND (l.clientefactoriaDestino.idclientefactoria >= :idDestinoMin"
			+ " AND  l.clientefactoriaDestino.idclientefactoria <= :idDestinoMax)"
			+ " AND (l.estadoliq >= :idEstadoMin AND l.estadoliq <= :idEstadoMax)"
			+ " AND (l.fechaemisionliq >= :fechaIni AND l.fechaemisionliq <= :fechaFin)"
			+ " AND l.documentoIddocumento.iddocumento IS NULL "
			+ " ORDER BY l.fechaemisionliq DESC , l.idliquidacion DESC")
	List<Liquidacion> listarLiquidacionesNoFacturadasPorFiltro(@Param("idEmpresa") Integer idEmpresa,
													@Param("idOrigenMin") Integer idOrigenMin,@Param("idOrigenMax") Integer idOrigenMax,
													@Param("idDestinoMin") Integer idDestinoMin,@Param("idDestinoMax") Integer idDestinoMax,
													@Param("idEstadoMin") Integer idEstadoMin,@Param("idEstadoMax") Integer idEstadoMax,
													@Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin);
			
}
