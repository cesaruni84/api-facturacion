/**
 * 
 */
package com.facturacion.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.Usuario;


/**
 * @author CESAR
 *
 */
@Repository
public interface UsuarioRepository extends JpaRepository <Usuario,Integer>{
	
	@Query("SELECT u FROM Usuario u WHERE u.codigousuario = ?1 AND u.secretkey = ?2")
	Optional<Usuario> findByCodigoUsuarioAndKey(String codigoUsuario, String secretkey);
	
	@Query("SELECT u FROM Usuario u WHERE u.codigousuario = ?1 "
									+ "AND u.secretkey = ?2 "
									+ "AND u.empresaIdempresa.idempresa =?3")
	Usuario findByCodigoUsuarioAndKeyAndShip(String codigoUsuario, String secretkey, Integer idEmpresa);
	
	
	@Query("SELECT u FROM Usuario u WHERE u.codigousuario = ?1 AND u.secretkey = ?2")
	List<Usuario> findByCodigoUsuarioAndKeyWithShip(String codigoUsuario, String secretkey);


}
