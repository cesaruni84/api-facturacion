/**
 * 
 */
package com.facturacion.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.facturacion.api.model.entity.MotivoTraslado;

/**
 * @author CESAR
 *
 */

@Repository
public interface MotivoTrasladoRepository extends JpaRepository<MotivoTraslado, Integer> {

}
