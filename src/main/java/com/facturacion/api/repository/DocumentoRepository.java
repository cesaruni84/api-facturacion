/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.Documento;

/**
 * @author CESAR
 *
 */
@Repository
public interface DocumentoRepository extends JpaRepository<Documento,Integer>{
	
	
	@Query("SELECT d FROM Documento d"
			+ " WHERE d.empresaIdempresa.idempresa = ?1 "
			+ " ORDER BY d.fechaemision DESC, d.iddocumento DESC")
	List<Documento> listarDocumentosPorEmpresa(Integer idEmpresa);
	
	
	@Query("SELECT d FROM Documento d"
			+ " WHERE d.empresaIdempresa.idempresa = ?1 "
			+ " AND d.clienteIdcliente.idcliente = ?2 "
			+ " ORDER BY d.fechaemision DESC, d.iddocumento DESC")
	List<Documento> listarDocumentosPorEmpresaCliente(Integer idEmpresa, Integer idCliente);
	
	/*Actualización 09/08 */
	@Query(value = "SELECT * FROM public.documento"
			+ " WHERE estado IN (1,2)"
			+ " AND notasadicionales= :subTipoDocumento"
			+ " AND (fechaemision >= :fechaIni AND fechaemision <= :fechaFin)"
			+ " AND empresa_idempresa= :idEmpresa"
			+ " EXCEPT "
			+ " SELECT d.* FROM public.liquidacion l,public.documento d" 
			+ " WHERE l.documento_iddocumento = d.iddocumento"  
			+ " AND d.estado IN (1,2) AND d.notasadicionales= :subTipoDocumento" 
			+ " AND (d.fechaemision >= :fechaIni AND d.fechaemision <= :fechaFin)"
			+ " AND d.empresa_idempresa= :idEmpresa",
			nativeQuery = true)
	List<Documento> listarDocumentosVigentesIncongruentes(@Param("idEmpresa") Integer idEmpresa,
												  @Param("subTipoDocumento") String subTipoDocumento,
												  @Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin);
	
	/*Actualización 09/08 */
	@Query("SELECT d FROM Documento d "
			+ "WHERE d.tipodocumento = ?1 "
			+ " AND d.serie = ?2  AND d.secuencia = ?3 AND d.empresaIdempresa.idempresa = ?4")
	Optional<Documento> obtenerComprobantePorNroDocTipo(Integer tipoDocumento, String nroSerie, String nroSecuencia, Integer idEmpresa);
	
	/*Actualización 09/08 */
	@Query("SELECT d FROM Documento d "
			+ "WHERE d.serie = ?1  AND d.secuencia = ?2 AND d.empresaIdempresa.idempresa = ?3")
	Optional<Documento> obtenerComprobantePorNroDOC(String nroSerie, String nroSecuencia, Integer idEmpresa);
	
	
	
	
	
    @Modifying(clearAutomatically = true)
	@Query("UPDATE Liquidacion l SET l.documentoIddocumento.iddocumento = NULL "
		+ "WHERE l.documentoIddocumento.iddocumento = ?1")
	void liberarLiquidacionDocumento(Integer idDocumento);
    
    @Modifying(clearAutomatically = true)
	@Query("UPDATE GuiaRemision g SET g.liquidacionIdliquidacion.idliquidacion = NULL "
			+ "WHERE g.liquidacionIdliquidacion.idliquidacion = ?1")
	void liberarGuiaLiquidacion(Integer idLiquidacion);
    
    @Modifying(clearAutomatically = true)
	@Query("UPDATE GuiaRemision g SET g.documentoIddocumento.iddocumento = NULL "
		+ "WHERE g.documentoIddocumento.iddocumento = ?1")
	void liberarGuiaDocumento(Integer idDocumento);
  
	@Query("SELECT d FROM Documento d "
			+ " WHERE d.empresaIdempresa.idempresa = :idEmpresa"
			+ " AND d.tipodocumento = :idTipoDocumento"
			+ " AND (d.clienteIdcliente.idcliente >= :idClienteMin"
			+ " AND  d.clienteIdcliente.idcliente <= :idClienteMax)"
			+ " AND (d.estado >= :idEstadoMin AND d.estado <= :idEstadoMax)"
			+ " AND (d.fechaemision >= :fechaIni AND d.fechaemision <= :fechaFin)"
			+ " ORDER BY d.fechaemision DESC, d.iddocumento DESC")
	List<Documento> listarDocumentosPorFiltro(@Param("idEmpresa") Integer idEmpresa,
													@Param("idClienteMin") Integer idClienteMin,@Param("idClienteMax") Integer idClienteMax,
													@Param("idEstadoMin") Integer idEstadoMin,@Param("idEstadoMax") Integer idEstadoMax,
													@Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin, @Param("idTipoDocumento") Integer idTipoDocumento);
	

}
