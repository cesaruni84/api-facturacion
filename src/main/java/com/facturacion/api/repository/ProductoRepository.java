/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.Producto;

/**
 * @author CESAR
 *
 */
@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer>{
	
	@Query("SELECT p FROM Producto p WHERE p.codigo = :codigo ")
	Optional<Producto> findByCodigo(@Param("codigo") String codigo);

}
