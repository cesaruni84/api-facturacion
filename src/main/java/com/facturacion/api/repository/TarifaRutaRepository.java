/**
 * 
 */
package com.facturacion.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.TarifaRuta;

/**
 * @author CESAR
 *
 */
@Repository
public interface TarifaRutaRepository extends JpaRepository<TarifaRuta, Integer>{
	
	@Query("SELECT t FROM TarifaRuta t"
			+ " WHERE t.clientefactoriaIdorigen.idclientefactoria = :idOrigen "
			+ " AND t.clientefactoriaIddestino.idclientefactoria = :idDestino"
			+ " AND t.turnoIdturno.idturno = :idTurno")
	Optional<TarifaRuta> obtenerTarifaTurnoAsociada(@Param("idOrigen") Integer  idOrigen, 
													@Param("idDestino") Integer idDestino,
													@Param("idTurno") Integer idTurno);

}
