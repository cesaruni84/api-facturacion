/**
 * 
 */
package com.facturacion.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.facturacion.api.model.entity.ClienteFactoria;
import com.facturacion.api.model.entity.GuiaRemision;

/**
 * @author CESAR
 *
 */
@Repository
public interface ClienteFactoriaRepository extends JpaRepository<ClienteFactoria, Integer>{
	
	
	@Query("SELECT c FROM ClienteFactoria c WHERE c.tipoFactoria IN (?1 , 'A') ORDER BY c.idclientefactoria DESC")
	List<ClienteFactoria> listarFactoriasPorTipo(String tipoFactoria);
	
	@Query("SELECT c FROM ClienteFactoria c ORDER BY c.nombrefactoria ASC")
	List<ClienteFactoria> listarTodoFactoria();
	

}
