package com.facturacion.api.controller.util;

import java.io.Serializable;

public class CustomErrorType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codeMessage;
    private String errorMessage;

    public CustomErrorType(String codeMessage, String errorMessage){
        this.codeMessage = codeMessage;
        this.errorMessage = errorMessage;

    }

	/**
	 * @return the codeMessage
	 */
	public String getCodeMessage() {
		return codeMessage;
	}

	/**
	 * @param codeMessage the codeMessage to set
	 */
	public void setCodeMessage(String codeMessage) {
		this.codeMessage = codeMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}



}
