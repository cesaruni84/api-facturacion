/**
 * 
 */
package com.facturacion.api.controller.util;
import java.io.Serializable;

import org.apache.logging.log4j.Logger;
/**
 * @author CESAR
 *
 */



public class CustomResponseType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codeMessage;
    private String alertMessage;
    private Logger log;

    public CustomResponseType(String codeMessage, String alertMessage){
        this.codeMessage = codeMessage;
        this.alertMessage = alertMessage;
    }

	public CustomResponseType(Logger logger ) {
		this.log = logger;
	}
	
	public CustomResponseType( ) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the codeMessage
	 */
	public String getCodeMessage() {
		return codeMessage;
	}

	/**
	 * @param codeMessage the codeMessage to set
	 */
	public void setCodeMessage(String codeMessage) {
		this.codeMessage = codeMessage;
	}

	/**
	 * @return the alertMessage
	 */
	public String getAlertMessage() {
		return alertMessage;
	}

	/**
	 * @param alertMessage the alertMessage to set
	 */
	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
		if (this.log != null) {
			log.info(codeMessage + " - " +  alertMessage);
			log.info("-------------------------------------------------" );
		}
	}





}
