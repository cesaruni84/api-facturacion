package com.facturacion.api.controller.util;


public class Constantes {
	
	public static String URL_GET_TOKEN = "https://apiprod.vnforapps.com/api.security/v1/security";
	public static String URL_POST_SERVICE_PAGO = "https://apiprod.vnforapps.com/api.visadirect/v2/p2p/341198210";
	public static String USER_API_VISANET = "info@quiputech.com" ;
	public static String PWD_API_VISANET  = "Pa$$w0rd";
	public static String URL_SOURCE_JSON  = "D:\\\\requestVisanetP2P.json";
	
	//URI DE SERVICIOS DEL API
	public static String YUPI_URI_API_COMERCIOS  = "/v1/bbva/yupi/comercios";
	public static String YUPI_URI_API_PERSONAS  = "/v1/bbva/yupi/personas";
	public static String YUPI_URI_API_EVENTOS  = "/v1/bbva/yupi/eventos";
	public static String YUPI_URI_API_PEDIDOS  = "/v1/bbva/yupi/comercios";

	

}
