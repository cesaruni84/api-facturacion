package com.facturacion.api.controller.util;

public class UtilitarioDocumentos {
	
	
	private static final String retornarLiteralTipoDoc(final Integer codigoTipoDoc) {
		String nemonicoLiteralTipoDoc = "?";
		switch (codigoTipoDoc) {
		case 1:
			nemonicoLiteralTipoDoc = "Factura";
			break;
		case 2:
			nemonicoLiteralTipoDoc = "Nota de Crédito";
			break;
		case 3:
			nemonicoLiteralTipoDoc = "Nota de Débito";
			break;
		case 4:
			nemonicoLiteralTipoDoc = "Boleta de Venta";
			break;
		default:
			break;
		}
		return nemonicoLiteralTipoDoc;	
	}

}
