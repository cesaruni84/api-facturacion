package com.facturacion.api.controller.liquidacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.model.entity.MotivoTraslado;
import com.facturacion.api.service.liquidacion.MotivoTrasladoService;


@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class MotivoTrasladoController {

	@Autowired
	private MotivoTrasladoService motivoTrasladoService ;

	@GetMapping(value = "/motivosTraslado", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MotivoTraslado> listarComboMotivosTraslado() {
		return motivoTrasladoService.listarTodosLosMotivosDeTrasladoBienes();

	}

}
