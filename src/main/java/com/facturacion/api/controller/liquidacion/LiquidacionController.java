/**
 * 
 */
package com.facturacion.api.controller.liquidacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Empresa;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.enums.BusquedaLiquidacion;
import com.facturacion.api.service.liquidacion.LiquidacionService;
import com.facturacion.api.service.remision.GuiaRemisionService;

/**
 * @author CESAR
 *
 */
@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class LiquidacionController {

	
	public static final Logger logger = LogManager.getLogger(LiquidacionController.class);

	
	@Autowired
	private LiquidacionService liquidacionService ;
	
	@Autowired
	private GuiaRemisionService guiaRemisionService;

	@GetMapping(value = "/empresas/{id-empresa}/liquidaciones", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Liquidacion> listarLiquidaciones(@PathVariable("id-empresa") Integer idEmpresa) {
		return liquidacionService.listTodasLiquidacionesPorEmpresa(idEmpresa);
	}
	
	@GetMapping(value = "/empresas/{id-empresa}/liquidaciones/SRV1", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerLiquidacionPorNroDoc(@PathVariable("id-empresa") Integer idEmpresa,
															 @RequestParam(name="nroDocLiq" ) String nroDocLiq) 
	{

		try {

			Optional<Liquidacion> liquidacionQuery = liquidacionService.obtenerLiquidacionPorNroDoc(nroDocLiq,idEmpresa);
			
			if (liquidacionQuery.isPresent()) {
				return new ResponseEntity<Liquidacion>(liquidacionQuery.get(), 
							HeaderUtil.createEntityCreationAlert("liquidacion.query", String.valueOf(liquidacionQuery.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFE3003", "Nro de documento de liquidación no existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("liquidacion", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/liquidaciones/SRV0", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarLiquidacionesPorFiltro(@PathVariable("id-empresa") Integer idEmpresa,
													@RequestParam(name="nroDocLiq" ) String nroDocLiq,
													 @RequestParam(name="origen" ) Integer idOrigen,
													 @RequestParam(name="destino") Integer idDestino,
													 @RequestParam(name="estado") Integer estado,
													 @RequestParam(name="tipoBusqueda") Integer tipoBusqueda,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		
		try {
			List<Liquidacion> listaLiquidacionesFiltro = new ArrayList<>();
			Optional<Liquidacion> liquidacionQuery = null;
			if (nroDocLiq == null || nroDocLiq == "") {
				 listaLiquidacionesFiltro = liquidacionService.listarLiquidacionesPorFiltroCriteria(idEmpresa, idOrigen, idDestino, estado, fechaIni, fechaFin, tipoBusqueda);
			}else {
				liquidacionQuery = liquidacionService.obtenerLiquidacionPorNroDoc(nroDocLiq, idEmpresa);
				if (liquidacionQuery.isPresent()) {
					if (tipoBusqueda ==  BusquedaLiquidacion.LIQ_NO_FACTURADAS.getValue()) {
						if (liquidacionQuery.get().getDocumentoIddocumento() != null) {
							return new ResponseEntity<CustomErrorType>(
									new CustomErrorType("MFE3003", "Nro de liquidación " +  nroDocLiq  + " ya se encuentra asociada a una factura."),
									HeaderUtil.createFailureAlert("liquidaciones", "", ""),
									HttpStatus.NOT_FOUND);
						} else {
							listaLiquidacionesFiltro.add(liquidacionQuery.get());
						}
						
					}else {
						listaLiquidacionesFiltro.add(liquidacionQuery.get());
					}
				} else {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFE3003", "Nro de liquidación " +  nroDocLiq  + " no existe en el sistema."),
							HeaderUtil.createFailureAlert("liquidaciones", "", ""),
							HttpStatus.NOT_FOUND);
				}		
			}	
		    if (!listaLiquidacionesFiltro.isEmpty()) {
				return new ResponseEntity<List<Liquidacion>>(listaLiquidacionesFiltro, 
						HeaderUtil.createEntityCreationAlert("liquidaciones.query", "grilla"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen liquidaciones registradas para los filtros indicados."),
							HeaderUtil.createFailureAlert("liquidaciones", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
	}
	
	@GetMapping(value = "/empresas/{id-empresa}/liquidaciones/SRV5", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarNroLiquidacionEmpresa(@PathVariable("id-empresa") Integer idEmpresa,
													@RequestParam(name="nroDocLiq" ) String nroDocLiq) {
		
		CustomResponseType responseType = new CustomResponseType(logger);
		
		logger.info("-----------validarNroLiquidacionEmpresa -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Liquidación: " + nroDocLiq );
		
		
		try {
			Optional<Liquidacion> liquidacionQuery = liquidacionService.obtenerLiquidacionPorNroDoc(nroDocLiq,idEmpresa);
	
		    if (liquidacionQuery.isPresent()) {
				responseType.setCodeMessage("MFA1001");
				responseType.setAlertMessage("Nro. de Liquidación: " + nroDocLiq + " ya existe en el sistema.");
		     }else{
				responseType.setCodeMessage("MFA1002");
				responseType.setAlertMessage("Nro. de Liquidación: " + nroDocLiq + " no existe en el sistema.");
		     }	
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		} 
			
		return new ResponseEntity<CustomResponseType>(responseType,
					HeaderUtil.createEntityCreationAlert("liquidacion.created", nroDocLiq),
					HttpStatus.OK);
	
	}
	
	@PostMapping(value = "/empresas/{id-empresa}/liquidaciones", 
				consumes = MediaType.APPLICATION_JSON_VALUE, 
				produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<?> registrarLiquidacion(@PathVariable("id-empresa") Integer idEmpresa,
													@Valid @RequestBody Liquidacion liquidacionForm) {

		CustomResponseType responseType = new CustomResponseType(logger);
		Liquidacion liquidacionCreada = null;
		
		logger.info("-----------registrarLiquidacion -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Liquidación: " + liquidacionForm.getNrodocumentoliq() );
		
		try {
			// Valida que la liquidacion ingresada no exista en el sistema
			Optional<Liquidacion> liquidacionQuery = liquidacionService.obtenerLiquidacionPorNroDoc(liquidacionForm.getNrodocumentoliq(), idEmpresa);

			if (liquidacionQuery.isPresent()) {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA1001",
											"Nro. de Liquidación: " + liquidacionQuery.get().getNrodocumentoliq()
											+ " ya existe en el sistema."),
						HeaderUtil.createFailureAlert("liquidaciones", "", ""), HttpStatus.CONFLICT);
			} else {
				Empresa empresa = new Empresa();
				empresa.setIdempresa(idEmpresa);
				liquidacionForm.setEmpresaIdempresa(empresa);
				
				//Graba Liquidacion en el sistema
				// Set <GuiaRemision> listaGuias = liquidacionForm.getGuiaRemisionSet();
				liquidacionCreada = liquidacionService.registrarLiquidacion(liquidacionForm);
				
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Liquidación : " + liquidacionCreada.getNrodocumentoliq() 
											+" registrada correctamente en el sistema.");
			}

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("liquidacion", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("liquidacion.created", String.valueOf(liquidacionCreada.hashCode())),
				HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/empresas/{id-empresa}/liquidaciones/{id-liquidacion}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarLiquidacion(@PathVariable("id-empresa") Integer idEmpresa,
													@PathVariable("id-liquidacion") Integer idLiquidacion,
													@Valid @RequestBody Liquidacion liquidacionUpdate) {
		

		Empresa empresa = new Empresa();
		empresa.setIdempresa(idEmpresa);
		liquidacionUpdate.setEmpresaIdempresa(empresa);
		liquidacionUpdate.setIdliquidacion(idLiquidacion);
		CustomResponseType responseType = new CustomResponseType(logger);
		Liquidacion liquidacion = null;
		
		logger.info("-----------actualizarLiquidacion -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Liquidación: " + liquidacionUpdate.getNrodocumentoliq() );
		
		try {
			
				//Actualiza Liquidacion
				liquidacion = liquidacionService.actualizarLiquidacion(liquidacionUpdate);
				
				//Prepara mensaje
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Liquidación : " + liquidacion.getNrodocumentoliq() 
													+ " actualizada correctamente en el sistema.");

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("liquidaciones", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("liquidacion.updated", String.valueOf(liquidacion.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping(value = "/empresas/{id-empresa}/liquidaciones/{nro-liquidacion}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eliminarGuiaRemision(@PathVariable("id-empresa") Integer idEmpresa, 
												  @PathVariable("nro-liquidacion") String nroLiquidacion) {

		CustomResponseType responseType = new CustomResponseType();
		logger.info("-----------eliminarLiquidacion -------" );
		logger.info("nro-liquidacion: " +  nroLiquidacion);
	

		try {
			Optional<Liquidacion> liquidacionBD = liquidacionService.obtenerLiquidacionPorNroDoc(nroLiquidacion, idEmpresa);
						
			if (liquidacionBD.isPresent()) {		
				if (liquidacionBD.get().getDocumentoIddocumento()!= null) {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFA1003", "Liquidacion "+ liquidacionBD.get().getNrodocumentoliq() +  " tiene una Factura asociada. No procede la eliminación" ),
							HeaderUtil.createFailureAlert("liquidacion", "", ""),
							HttpStatus.CONFLICT);
				} else {
					//Actualiza Guias Asociadas
					guiaRemisionService.liberarGuiasDeLiquidacion(liquidacionBD.get().getIdliquidacion());
					
					// Elimina Guia de base de datos, siempre y cuando no tenga relación con : factura
					liquidacionService.eliminarLiquidacionBD(liquidacionBD.get().getIdliquidacion());
					logger.info("Liquidacion: " + liquidacionBD.get().getNrodocumentoliq() + " eliminada  de la BD.");
					logger.info("-------------------------------------------------" );
			
				}
						
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA1002", "Liquidacion no existe en el sistema."),
						HeaderUtil.createFailureAlert("liquidacion", "", ""),
						HttpStatus.NOT_FOUND);
			}
			
			// Prepara mensaje
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Liquidacion " + nroLiquidacion + " eliminada correctamente de la base de datos !!");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("liquidacion", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("liquidacion.delete", nroLiquidacion.toString()),
				HttpStatus.OK);
	}

	
	

}
