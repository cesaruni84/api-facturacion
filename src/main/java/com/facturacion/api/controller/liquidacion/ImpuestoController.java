package com.facturacion.api.controller.liquidacion;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Impuesto;
import com.facturacion.api.service.liquidacion.ImpuestoService;


@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class ImpuestoController {
	
	
	@Autowired
	private ImpuestoService impuestoService ;

	@GetMapping(value = "/impuestos", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Impuesto> listarComboMotivosTraslado() {
		return impuestoService.listarTodosLosImpuestos();

	}
	
	
	@GetMapping(value = "/impuestos/{id-impuesto}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerImpuestoAplicarPorId(@PathVariable("id-impuesto") Integer idImpuesto) 
	{

		try {

			Optional<Impuesto> impuesto = impuestoService.obtenerImpuestoPorId(idImpuesto);
			
			if (impuesto.isPresent()) {
				return new ResponseEntity<Impuesto>(impuesto.get(), 
							HeaderUtil.createEntityCreationAlert("guia.query", String.valueOf(impuesto.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT114", "Código de impuesto no existe en el sistema."),
						HeaderUtil.createFailureAlert("impuesto", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT311", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("impuesto", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}

}
