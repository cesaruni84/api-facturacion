package com.facturacion.api.controller.liquidacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.entity.OrdenServicio;
import com.facturacion.api.service.liquidacion.OrdenServicioService;

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class OrdenServicioController {
	
	public static final Logger logger = LoggerFactory.getLogger(OrdenServicioController.class);
	
	@Autowired
	private OrdenServicioService ordenServicioService;
	

	@GetMapping(value = "/empresas/{id-empresa}/ordenes-servicio", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OrdenServicio> listarOrdenesServicio(@PathVariable("id-empresa") Integer idEmpresa) {
		return ordenServicioService.listarOrdenesServicioPorEmpresa(idEmpresa);
	}
	
	
	
	@SuppressWarnings("unused")
	@GetMapping(value = "/empresas/{id-empresa}/ordenes-servicio/SRV0", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarOrdenServicioPorFiltro(@PathVariable("id-empresa") Integer idEmpresa,
													@RequestParam(name="nroOrden" ) String nroOrden,
													 @RequestParam(name="estado") Integer estado,
													 @RequestParam(name="facturado") Integer conFactura,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		
		try {
			List<OrdenServicio> listaOrdenesServicio = new ArrayList<>();
			Optional<OrdenServicio> ordenServicioQuery = null;
			if (nroOrden == null || nroOrden == "") {
				listaOrdenesServicio = ordenServicioService.listarOrdenesServicioFiltro(idEmpresa, estado, conFactura, fechaIni, fechaFin);
	
			}else {
				ordenServicioQuery = ordenServicioService.obteneOrdenServicioPorNro(nroOrden);
				if (ordenServicioQuery.isPresent()) {
					listaOrdenesServicio.add(ordenServicioQuery.get());
				} else {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFE3003", "Nro. de orden de servicio no existe en el sistema."),
							HeaderUtil.createFailureAlert("liquidaciones", "", ""),
							HttpStatus.NOT_FOUND);
				}		
			}	
		    if (!listaOrdenesServicio.isEmpty()) {
				return new ResponseEntity<List<OrdenServicio>>(listaOrdenesServicio, 
						HeaderUtil.createEntityCreationAlert("orden-servicio.query", "grilla"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen liquidaciones registradas para los filtros indicados."),
							HeaderUtil.createFailureAlert("liquidaciones", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("orden-servicio", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/ordenes-servicio/SRV1", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerOrdenServicioPorNro(@PathVariable("id-empresa") Integer idEmpresa,
											@RequestParam(name="nroOrdenServicio" ) String nroOrdenServicio) 
	{

		try {

			Optional<OrdenServicio> ordenServicioQuery = ordenServicioService.obteneOrdenServicioPorNro(nroOrdenServicio);
			
			if (ordenServicioQuery.isPresent()) {
				return new ResponseEntity<OrdenServicio>(ordenServicioQuery.get(), 
							HeaderUtil.createEntityCreationAlert("ordenes-servicio.query", String.valueOf(ordenServicioQuery.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFE3003", "Nro de orden de servicio no existe en el sistema."),
						HeaderUtil.createFailureAlert("ordenes-servicio", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("ordenes-servicio", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	

	@GetMapping(value = "/empresas/{id-empresa}/ordenes-servicio/SRV2", produces = MediaType.APPLICATION_JSON_VALUE)
	public  ResponseEntity<?> retornarGuiasRemisionPorOrdenServicio(@PathVariable("id-empresa") Integer idEmpresa,
																	@RequestParam(name="nroOrdenServicio") String nroOrdenServicio) 
	{
		try {
			Optional<OrdenServicio> ordenServicioQuery = ordenServicioService.obteneOrdenServicioPorNro(nroOrdenServicio);
			
			if (ordenServicioQuery.isPresent()) {
				List<GuiaRemision> listaGuias = ordenServicioService.obtenerGuiasAsociadasOrdenServicio(ordenServicioQuery.get());
				return new ResponseEntity<List<GuiaRemision>>(listaGuias, 
							HeaderUtil.createEntityCreationAlert("ordenes-servicio.query", String.valueOf(ordenServicioQuery.get().hashCode())), 
							HttpStatus.OK);

			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFE3003", "Nro de orden de servicio no existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("ordenes-servicio", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}
	}
	
	
	
	@PostMapping(value = "/empresas/{id-empresa}/ordenes-servicio", 
				consumes = MediaType.APPLICATION_JSON_VALUE,  
				produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<?> registrarOrdenServicio(@PathVariable("id-empresa") Integer idEmpresa,
													@Valid @RequestBody OrdenServicio ordenServicioForm) {

		CustomResponseType responseType = new CustomResponseType();
		OrdenServicio ordenServicioCreada = null;
		try {
			// Valida que la liquidacion ingresada no exista en el sistema
			Optional<OrdenServicio> ordenServicioQuery = ordenServicioService.obteneOrdenServicioPorNro(ordenServicioForm.getNroorden());

			if (ordenServicioQuery.isPresent()) {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA1001",
											"Nro. de Orden: " + ordenServicioQuery.get().getNroorden()
											+ " ya existe en el sistema."),
						HeaderUtil.createFailureAlert("ordenes-servicio", "", ""), HttpStatus.CONFLICT);
			} else {
				
				//Graba Liquidacion en el sistema
				// Set <GuiaRemision> listaGuias = ordenServicioForm.getGuiaRemisionSet();
				ordenServicioCreada = ordenServicioService.registrarOrdenServicio(ordenServicioForm);
				
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Orden de Servicio : " + ordenServicioCreada.getNroorden()
											+" registrada correctamente en el sistema.");
			}

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("orden-servicio", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("orden-servicio.created", String.valueOf(ordenServicioCreada.hashCode())),
				HttpStatus.CREATED);
	}
	
/*	@PutMapping(value = "/empresas/{id-empresa}/liquidaciones/{id-liquidacion}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarOrdenServicio(@PathVariable("id-empresa") Integer idEmpresa,
			@PathVariable("id-liquidacion") Integer idLiquidacion, @Valid @RequestBody Liquidacion liquidacionUpdate) {

		Empresa empresa = new Empresa();
		empresa.setIdempresa(idEmpresa);
		liquidacionUpdate.setEmpresaIdempresa(empresa);
		liquidacionUpdate.setIdliquidacion(idLiquidacion);

		CustomResponseType responseType = new CustomResponseType();
		Liquidacion liquidacion = null;
		try {

			// Actualiza Liquidacion
			// liquidacion = liquidacionService.actualizarLiquidacion(liquidacionUpdate);

			// Prepara mensaje
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage(
					"Liquidación : " + liquidacion.getNrodocumentoliq() + " actualizada correctamente en el sistema.");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("liquidaciones", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("liquidacion.updated", String.valueOf(liquidacion.hashCode())),
				HttpStatus.ACCEPTED);
	}*/

}
