/**
 * 
 */
package com.facturacion.api.controller.facturacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Documento;
import com.facturacion.api.model.entity.Empresa;
import com.facturacion.api.service.facturacion.DocumentoService;

/**
 * @author CESAR
 *
 */

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class FacturaController {
	
	public static final Logger logger = LogManager.getLogger(FacturaController.class);

	@Autowired
	private DocumentoService documentoService ;
	
	@GetMapping(value = "/empresas/{id-empresa}/documentos/SRV0", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarDocumentosElectronicosEmpresa(@PathVariable("id-empresa") Integer idEmpresa) 
	{
		
		try {
			List<Documento> listaDocumentosElectronicos = new ArrayList<>();
			listaDocumentosElectronicos = documentoService.listTodosComprobantesPorEmpresa(idEmpresa);

		    if (!listaDocumentosElectronicos.isEmpty()) {
				return new ResponseEntity<List<Documento>>(listaDocumentosElectronicos, 
						HeaderUtil.createEntityCreationAlert("comprobantes.query", "listado"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen documentos para la empresa seleccionada."),
							HeaderUtil.createFailureAlert("comprobantes", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("comprobantes", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/clientes/{id-cliente}/documentos/SRV0", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarDocumentosElectronicosEmpresaCliente(@PathVariable("id-empresa") Integer idEmpresa,
																		@PathVariable("id-cliente") Integer idCliente) 
	{
		
		try {
			List<Documento> listaDocumentosElectronicos = new ArrayList<>();
			listaDocumentosElectronicos = documentoService.listTodosComprobantesPorEmpresaCliente(idEmpresa, idCliente);
			
		    if (!listaDocumentosElectronicos.isEmpty()) {
				return new ResponseEntity<List<Documento>>(listaDocumentosElectronicos, 
						HeaderUtil.createEntityCreationAlert("comprobantes.query", "listado"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen documentos registrados para el cliente seleccionado."),
							HeaderUtil.createFailureAlert("comprobantes", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("comprobantes", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
	}
	
	@GetMapping(value = "/empresas/{id-empresa}/documentos/error", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarDocumentosElectronicosConErrores(@PathVariable("id-empresa") Integer idEmpresa,
																	 @RequestParam(name="subTipoDocumento") Integer idSubTipoDocumento,
																	 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
																	 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) 
	{
		
		try {
			List<Documento> listaDocumentosElectronicosErrores = new ArrayList<>();
			listaDocumentosElectronicosErrores = documentoService.listarDocumentosVigentesIncongruentes(idEmpresa, idSubTipoDocumento,fechaIni,fechaFin);
			
		    if (!listaDocumentosElectronicosErrores.isEmpty()) {
				return new ResponseEntity<List<Documento>>(listaDocumentosElectronicosErrores, 
						HeaderUtil.createEntityCreationAlert("comprobantes.query", "listado-errores"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen documentos electrónicos con errores en el rango seleccionado."),
							HeaderUtil.createFailureAlert("comprobantes", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("comprobantes", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
	}
	

	
	@GetMapping(value = "/empresas/{id-empresa}/documentos/SRV1", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerDocumentoElectronicoPorEmpresa(@PathVariable("id-empresa") Integer idEmpresa,
																	@RequestParam(name="tipoDocumento" ) Integer tipoDocumento,
																	@RequestParam(name="nroSerie" ) String serie, 
																	@RequestParam(name="nroSecuencia" ) String secuencia) 
	{

		try {

			Optional<Documento> documentoQuery = null;
			documentoQuery = documentoService.obtenerComprobantePorNroDoc(tipoDocumento, serie, secuencia, idEmpresa);
						
			if (documentoQuery.isPresent()) {
				return new ResponseEntity<Documento>(documentoQuery.get(), 
							HeaderUtil.createEntityCreationAlert("documentos-electronicos.query", String.valueOf(documentoQuery.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFE3003", "Nro. de Comprobante no existe en el sistema."),
						HeaderUtil.createFailureAlert("documentos-electronicos", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos-electronicos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/documentos/SRV2", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarDocumentosElectronicosPorEmpresaFiltro(@PathVariable("id-empresa") Integer idEmpresa,
													@RequestParam(name="nroSerie" ) String serie,
													@RequestParam(name="nroSecuencia" ) String secuencia,
													@RequestParam(name="tipoDocumento" ) Integer tipoDocumento,
													 @RequestParam(name="estado") Integer estado,
													 @RequestParam(name="cliente") Integer idCliente,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		
		try {
			List<Documento> listaDocumentosElectronicos = new ArrayList<>();
			Optional<Documento> documentoQuery = null;
			if ((serie != null && serie != "") && (secuencia != null && secuencia != "")){
				documentoQuery = documentoService.obtenerComprobantePorNroDoc(tipoDocumento, serie, secuencia, idEmpresa);
				if (documentoQuery.isPresent()) {
					listaDocumentosElectronicos.add(documentoQuery.get());
				} else {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFE3003", "Nro de documento electrónico " + serie + " - "+  secuencia +  " no existe en el sistema."),
							HeaderUtil.createFailureAlert("documento", "", ""),
							HttpStatus.NOT_FOUND);
				}
			} else {
				// Listar Por Filtros.
				listaDocumentosElectronicos = documentoService.listarDocumentosPorFiltro(idEmpresa, tipoDocumento, serie, secuencia, idCliente, estado, fechaIni, fechaFin);

			}
		    if (!listaDocumentosElectronicos.isEmpty()) {
				return new ResponseEntity<List<Documento>>(listaDocumentosElectronicos, 
						HeaderUtil.createEntityCreationAlert("documentos.query", "listado"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen documentos electrónicos para los filtros seleccionados."),
							HeaderUtil.createFailureAlert("documentos", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}     
	}
	
	@GetMapping(value = "/empresas/{id-empresa}/documentos/SRV5", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarDocumentosElectronico(@PathVariable("id-empresa") Integer idEmpresa,
													@RequestParam(name="nroSerie" ) String serie,
													@RequestParam(name="nroSecuencia" ) String secuencia,
													@RequestParam(name="tipoDocumento" ) Integer tipoDocumento) {
		
		CustomResponseType responseType = new CustomResponseType();
		logger.info("-----------validarDocumentosElectronico -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("tipoDocumento: " + tipoDocumento);
		logger.info("Documento: " +  serie+ '-' + secuencia );
		
		try {
			// Valida que la factura ingresada no exista en el sistema
			Optional<Documento> documentoQuery = documentoService.obtenerComprobantePorNroDoc(tipoDocumento,serie, secuencia, idEmpresa);

			if (documentoQuery.isPresent()) {
				responseType.setCodeMessage("MFA1001");
				responseType.setAlertMessage(retornarLiteralTipoDoc(tipoDocumento) + serie + '-' + secuencia + " ya existe en el sistema.");
			} else {
				responseType.setCodeMessage("MFA01212");
				responseType.setAlertMessage(retornarLiteralTipoDoc(tipoDocumento) + serie + '-' + secuencia + " no existe en el sistema.");
			}

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos-electronicos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}
		logger.info("Documento: " +  serie + '-' + secuencia + " validado correctamente." );
		logger.info("-------------------------------------------------" );
		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("documentos-electronicos.validated", serie + '-' + secuencia),
				HttpStatus.OK);
 
	}
	
	@PostMapping(value = "/empresas/{id-empresa}/documentos", 
				consumes = MediaType.APPLICATION_JSON_VALUE, 
				produces = MediaType.APPLICATION_JSON_VALUE )
	@Transactional
	public ResponseEntity<?> registrarDocumentoElectronico(@PathVariable("id-empresa") Integer idEmpresa,
													 		 @RequestBody Documento documentoForm) {

		CustomResponseType responseType = new CustomResponseType();
		Documento documentoCreate = null;
		logger.info("-----------registrarDocumentoElectronico -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Documento: " +  documentoForm.getSerie() + '-' + documentoForm.getSecuencia() );

		try {
			// Valida que la factura ingresada no exista en el sistema
			Optional<Documento> documentoQuery = documentoService.obtenerComprobantePorNroDoc(
					documentoForm.getTipodocumento(), documentoForm.getSerie(), documentoForm.getSecuencia(), idEmpresa);

			if (documentoQuery.isPresent()) {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA1001",
								retornarLiteralTipoDoc(documentoQuery.get().getTipodocumento()) + documentoQuery.get().getSerie() + '-' + documentoQuery.get().getSecuencia()
											+ " ya existe en el sistema."),
						HeaderUtil.createFailureAlert("documentos-electronicos", "", ""), HttpStatus.CONFLICT);
			} else {
				Empresa empresa = new Empresa();
				empresa.setIdempresa(idEmpresa);
				documentoForm.setEmpresaIdempresa(empresa);
				
				//Graba Comprobante en el sistema
				documentoCreate = documentoService.registrarDocumento(documentoForm);
			
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage(retornarLiteralTipoDoc(documentoCreate.getTipodocumento())  + documentoCreate.getSerie() + '-' + documentoCreate.getSecuencia()
											+" registrada correctamente en el sistema.");
				
				logger.info("Documento: " +  documentoForm.getSerie() + '-' + documentoForm.getSecuencia() + " registrado correctamente." );
				logger.info("-------------------------------------------------" );

			}

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos-electronicos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("documentos-electronicos.created", String.valueOf(documentoCreate.hashCode())),
				HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/empresas/{id-empresa}/documentos/{id-documento}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarDocumentoElectronico(@PathVariable("id-empresa") Integer idEmpresa,
													@PathVariable("id-documento") Integer idDocumento,
													@Valid @RequestBody Documento documentoUpdate) {
		

		Empresa empresa = new Empresa();
		empresa.setIdempresa(idEmpresa);
		documentoUpdate.setEmpresaIdempresa(empresa);
		documentoUpdate.setIddocumento(idDocumento);
		
		logger.info("-----------actualizarDocumentoElectronico -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Documento: " +  documentoUpdate.getSerie() + '-' + documentoUpdate.getSecuencia() );
		
		CustomResponseType responseType = new CustomResponseType();
		Documento documentoBD = null;
		try {
			
			//Actualiza documento
			documentoBD = documentoService.actualizarDocumento(documentoUpdate);
			
			if (documentoBD != null) {
				//Prepara mensaje
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Documento : " + documentoBD.getSerie() + "-" + documentoBD.getSecuencia()
								+ " actualizado correctamente en el sistema.");
			} else {
				//Prepara mensaje
				responseType.setCodeMessage("MFE3002");
				responseType.setAlertMessage("Documento : " + documentoUpdate.getSerie() + "-" + documentoUpdate.getSecuencia()
								+ " no existe en el sistema.");
				
				logger.info("Documento: " +  documentoUpdate.getSerie() + '-' + documentoUpdate.getSecuencia() + " actualizado correctamente." );
				logger.info("-------------------------------------------------" );

			}		
		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos-electronicos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("documentos-electronicos.updated", String.valueOf(documentoBD.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	
	@PutMapping(value = "/empresas/{id-empresa}/documentos/{id-documento}/extorno", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> anularDocumentoElectronico(@PathVariable("id-empresa") Integer idEmpresa,
													@PathVariable("id-documento") Integer idDocumento,
													@Valid @RequestBody Documento documentoUpdate) {
		

		Empresa empresa = new Empresa();
		empresa.setIdempresa(idEmpresa);
		documentoUpdate.setEmpresaIdempresa(empresa);
		documentoUpdate.setIddocumento(idDocumento);
		CustomResponseType responseType = new CustomResponseType();
		Documento documentoBD = null;
		
		logger.info("-----------anularDocumentoElectronico -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Documento: " +  documentoUpdate.getSerie() + '-' + documentoUpdate.getSecuencia() );
		
		try {
			
				//Anula documento
				documentoBD = documentoService.extornarDocumento(documentoUpdate);
				
				if (documentoBD != null) {
					//Prepara mensaje
					responseType.setCodeMessage("MFA0000");
					responseType.setAlertMessage("Documento : " + documentoBD.getSerie() + "-" + documentoBD.getSecuencia()
									+ " ha sido anulado correctamente en el sistema.");
					
					logger.info("Documento: " +  documentoUpdate.getSerie() + '-' + documentoUpdate.getSecuencia() + " anulado correctamente." );
					logger.info("-------------------------------------------------" );
				} else {
					//Prepara mensaje
					responseType.setCodeMessage("MFE3002");
					responseType.setAlertMessage("Documento : " + documentoUpdate.getSerie() + "-" + documentoUpdate.getSecuencia()
									+ " no existe en el sistema.");
				}
				



		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos-electronicos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("documentos-electronicos.updated", String.valueOf(documentoBD.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	@PutMapping(value = "/empresas/{id-empresa}/documentos/{id-documento}/cancelacion", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cancelarDocumentoElectronico(@PathVariable("id-empresa") Integer idEmpresa,
													@PathVariable("id-documento") Integer idDocumento,
													@Valid @RequestBody Documento documentoUpdate) {
		

		Empresa empresa = new Empresa();
		empresa.setIdempresa(idEmpresa);
		documentoUpdate.setEmpresaIdempresa(empresa);
		documentoUpdate.setIddocumento(idDocumento);
		CustomResponseType responseType = new CustomResponseType();
		Documento documentoBD = null;
		
		logger.info("-----------cancelarDocumentoElectronico -------" );
		logger.info("idEmpresa: " + idEmpresa);
		logger.info("Documento: " +  documentoUpdate.getSerie() + '-' + documentoUpdate.getSecuencia() );
		
		try {
			
				//Anula documento
				documentoBD = documentoService.cancelarDocumento(documentoUpdate);
				
				if (documentoBD != null) {
					//Prepara mensaje
					responseType.setCodeMessage("MFA0000");
					responseType.setAlertMessage("Documento : " + documentoBD.getSerie() + "-" + documentoBD.getSecuencia()
									+ " ha sido cancelado correctamente en el sistema.");
					
					logger.info("Documento: " +  documentoUpdate.getSerie() + '-' + documentoUpdate.getSecuencia() + " cancelado correctamente." );
					logger.info("-------------------------------------------------" );
				} else {
					//Prepara mensaje
					responseType.setCodeMessage("MFE3002");
					responseType.setAlertMessage("Documento : " + documentoUpdate.getSerie() + "-" + documentoUpdate.getSecuencia()
									+ " no existe en el sistema.");
				}
				


		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("documentos-electronicos", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("documentos-electronicos.updated", String.valueOf(documentoBD.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	private static final String retornarLiteralTipoDoc(final Integer codigoTipoDoc) {
		String nemonicoLiteralTipoDoc = "?";
		switch (codigoTipoDoc) {
		case 1:
			nemonicoLiteralTipoDoc = "Factura ";
			break;
		case 2:
			nemonicoLiteralTipoDoc = "Nota de Crédito ";
			break;
		case 3:
			nemonicoLiteralTipoDoc = "Nota de Débito ";
			break;
		case 4:
			nemonicoLiteralTipoDoc = "Boleta de Venta ";
			break;
		default:
			break;
		}
		return nemonicoLiteralTipoDoc;	
	}


}
