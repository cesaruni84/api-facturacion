package com.facturacion.api.controller.facturacion;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Cliente;
import com.facturacion.api.service.facturacion.ClienteService;

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping(value = "/empresas/{id-empresa}/clientes", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Cliente> listarClientes(@PathVariable("id-empresa") Integer idEmpresa) {
		return clienteService.listarTodosLosClientes(idEmpresa);

	}

	@PostMapping(value = "/empresas/clientes", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarCliente(@Valid @RequestBody Cliente clienteForm) {

		CustomResponseType responseType = new CustomResponseType();
		Cliente cliente = null;
		try {

			Optional<Cliente> clienteQuery = clienteService.obtenerClientePorNroDoc(clienteForm.getTipodoc(),
					clienteForm.getNroruc());

			if (clienteQuery.isPresent()) {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA1001",
								"Cliente " + clienteQuery.get().getTipodoc() + '-' + clienteQuery.get().getNroruc()
										+ " ya existe en el sistema."),
						HeaderUtil.createFailureAlert("clientes", "", ""), HttpStatus.CONFLICT);
			} else {
				// TimeStamp
				Timestamp ts = new Timestamp(System.currentTimeMillis());
				Date fechaTimestamp = new Date(ts.getTime());
				clienteForm.setFecharegistro(fechaTimestamp);
				cliente = clienteService.registrarCliente(clienteForm);
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Cliente registrado correctamente.");
			}

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("cliente", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("cliente.created", String.valueOf(cliente.hashCode())),
				HttpStatus.CREATED);
	}

	@PutMapping(value = "/empresas/clientes/{id-cliente}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarCliente(@PathVariable("id-cliente") Integer idCliente,
			@Valid @RequestBody Cliente clienteForm) {

		CustomResponseType responseType = new CustomResponseType();
		Cliente cliente = null;
		try {

			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			clienteForm.setFecharegistro(fechaTimestamp);
			cliente = clienteService.actualizarCliente(clienteForm);
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Cliente actualizado correctamente.");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("cliente", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("cliente.updated", String.valueOf(cliente.hashCode())),
				HttpStatus.CREATED);
	}

}