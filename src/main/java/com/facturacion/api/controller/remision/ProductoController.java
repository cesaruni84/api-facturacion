/**
 * 
 */
package com.facturacion.api.controller.remision;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Producto;
import com.facturacion.api.service.remision.ProductoService;

/**
 * @author CESAR
 *
 */
@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class ProductoController {

	@Autowired
	private ProductoService productoService;

	@GetMapping(value = "/productos", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Producto> listarComboFactorias() {
		return productoService.listarTodosLosProductos();
	}
	
	@GetMapping(value = "/empresas/{id-empresa}/productos-servicios/SRV1", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<?> listarItemsProductosServicios(@PathVariable("id-empresa") Integer idEmpresa) {
		return productoService.listarTodosLosProductosServicios(idEmpresa);
	}
	
	@PostMapping(value = "/empresas/productos", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarProducto(@Valid @RequestBody Producto productoForm) {

		CustomResponseType responseType = new CustomResponseType();
		Producto producto = null;
		try {

			Optional<Producto> productoQuery = productoService.obtenerProductoPorCodigo(productoForm.getCodigo());

			if (productoQuery.isPresent()) {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA1001",
								"Producto " + productoQuery.get().getCodigo() +  " ya existe en el sistema."),
						HeaderUtil.createFailureAlert("productos", "", ""), HttpStatus.CONFLICT);
			} else {
				// TimeStamp
				Timestamp ts = new Timestamp(System.currentTimeMillis());
				Date fechaTimestamp = new Date(ts.getTime());
				productoForm.setFechaingreso(fechaTimestamp);
				productoForm.setFechaactualizacion(fechaTimestamp);
				producto = productoService.registrarProducto(productoForm);
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Producto registrado correctamente.");
			}

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("producto", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("producto.created", String.valueOf(producto.hashCode())),
				HttpStatus.CREATED);
	}

	@PutMapping(value = "/empresas/productos/{id-producto}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarCliente(@PathVariable("id-producto") Integer idProducto,
			@Valid @RequestBody Producto productoForm) {

		CustomResponseType responseType = new CustomResponseType();
		Producto producto = null;
		try {

			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			productoForm.setFechaactualizacion(fechaTimestamp);
			producto = productoService.actualizarProducto(productoForm);
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Producto actualizado correctamente.");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("producto", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("producto.updated", String.valueOf(producto.hashCode())),
				HttpStatus.CREATED);
	}


}