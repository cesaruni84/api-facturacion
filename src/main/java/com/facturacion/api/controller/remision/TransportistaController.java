/**
 * 
 */
package com.facturacion.api.controller.remision;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.ClienteFactoria;
import com.facturacion.api.model.entity.Transportista;
import com.facturacion.api.service.remision.TransportistaService;

/**
 * @author CESAR
 *
 */
@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class TransportistaController {
	
	@Autowired
	private TransportistaService transportistaService;
	
	@GetMapping(value = "/empresas/{id-empresa}/choferes")
	public List<Transportista> listarComboChoferesPorEmpresa(@PathVariable("id-empresa") Integer idEmpresa) {
		List<Transportista> listaChoferesPorEmpresa = transportistaService.listarChoferesPorEmpresa(idEmpresa);
		return listaChoferesPorEmpresa;
	}
	
	
	@PostMapping(value = "/empresas/{id-empresa}/choferes", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarTransportista(@Valid @RequestBody Transportista transportistaForm) {

		CustomResponseType responseType = new CustomResponseType();
		Transportista transportista = null;
		try {
			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			transportistaForm.setFecharegistro(fechaTimestamp);
			transportistaForm.setFechaupdate(fechaTimestamp);
			transportista = transportistaService.registrarTransportista(transportistaForm);
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Chofer registrado correctamente");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("transportista", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("transportista.created", String.valueOf(transportista.hashCode())),
				HttpStatus.CREATED);
	}
	
	

	@PutMapping(value = "/empresas/{id-empresa}/choferes/{id-chofer}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarTransportista(@PathVariable("id-chofer") Integer idChofer,
													@Valid @RequestBody Transportista transportistaForm) {
		
		transportistaForm.setIdtransportista(idChofer);
		CustomResponseType responseType = new CustomResponseType();
		Transportista transportista = null;
		try {
				// TimeStamp
				Timestamp ts = new Timestamp(System.currentTimeMillis());
				Date fechaTimestamp = new Date(ts.getTime());
				transportistaForm.setFechaupdate(fechaTimestamp);
			
				//Actualiza unidad de transporte
				transportista = transportistaService.actualizaTransportista(transportistaForm);
				
				//Prepara mensaje
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Chofer actualizado correctamente");


		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("transportista", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("transportista.updated", String.valueOf(transportista.hashCode())),
				HttpStatus.CREATED);
	}

}
