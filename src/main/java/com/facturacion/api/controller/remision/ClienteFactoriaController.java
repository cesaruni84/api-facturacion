package com.facturacion.api.controller.remision;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.ClienteFactoria;
import com.facturacion.api.service.remision.ClienteFactoriaService;

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class ClienteFactoriaController {
	
	@Autowired
	private ClienteFactoriaService clienteFactoriaService;

	@GetMapping(value = "/clientes/factorias",  produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ClienteFactoria> listarComboFactorias() {
		return clienteFactoriaService.listarTodasLasFactorias();

	}
	
	@GetMapping(value = "/clientes/factorias/SRV1",  produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ClienteFactoria> listarComboFactoriasPorTipo(@RequestParam(name="tipoFactoria") String tipoFactoria) {
		return clienteFactoriaService.listarFactoriasPorTipo(tipoFactoria);
	}
	
	@PostMapping(value = "/clientes/factorias", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarFactoria(@Valid @RequestBody ClienteFactoria clienteFactoriaForm) {

		CustomResponseType responseType = new CustomResponseType();
		ClienteFactoria clienteFactoria = null;
		try {

			clienteFactoria = clienteFactoriaService.registrarFactoria(clienteFactoriaForm);
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Planta " + clienteFactoriaForm.getClienteIdcliente().getRazonsocial()+ "/"
						+ clienteFactoriaForm.getNombrefactoria() + " registrada correctamente.");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("factoria", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("factoria.created", String.valueOf(clienteFactoria.hashCode())),
				HttpStatus.CREATED);
	}
	
	

	@PutMapping(value = "/clientes/factorias/{id-factoria}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarFactoria(@PathVariable("id-factoria") Integer idFactoria,
													@Valid @RequestBody ClienteFactoria clienteFactoriaForm) {
		
		clienteFactoriaForm.setIdclientefactoria(idFactoria);
		CustomResponseType responseType = new CustomResponseType();
		ClienteFactoria clienteFactoria = null;
		try {
			
				//Actualiza Factoria
				clienteFactoria = clienteFactoriaService.actualizarFactoria(clienteFactoriaForm);
				
				//Prepara mensaje
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Planta " + clienteFactoriaForm.getClienteIdcliente().getRazonsocial()+ "/"
							+ clienteFactoriaForm.getNombrefactoria() + " actualizada correctamente.");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("factoria", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("factoria.updated", String.valueOf(clienteFactoria.hashCode())),
				HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/clientes/factorias/{id-factoria}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eliminarFactoria(@PathVariable("id-factoria") Integer idFactoria) {

		CustomResponseType responseType = new CustomResponseType();

		try {
			// Elimina factoria de base de datos
			clienteFactoriaService.eliminarFactoria(idFactoria);

			// Prepara mensaje
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Planta eliminada correctamente de la base de datos !!");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("factorias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("factorias.delete", idFactoria.toString()),
				HttpStatus.OK);
	}


}
