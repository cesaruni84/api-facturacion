/**
 * 
 */
package com.facturacion.api.controller.remision;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.model.entity.Balanza;
import com.facturacion.api.service.remision.BalanzaService;

/**
 * @author CESAR
 *
 */
@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class BalanzaController {
	
	@Autowired
	private BalanzaService balanzaService;

	@GetMapping(value = "/balanzas", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Balanza> listarComboBalanzas() {
		return balanzaService.listarTodasBalanzas();

	}

}
