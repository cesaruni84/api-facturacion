/**
 * 
 */
package com.facturacion.api.controller.remision;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.bo.GuiaRemisionDTO;
import com.facturacion.api.model.entity.ClienteFactoria;
import com.facturacion.api.model.entity.Empresa;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.enums.BusquedaGuias;
import com.facturacion.api.model.enums.EstadoGuia;
import com.facturacion.api.model.filters.FiltrosGuiaRemision;
import com.facturacion.api.model.filters.FiltrosMaestro;
import com.facturacion.api.service.remision.GuiaRemisionService;

/**
 * @author CESAR
 *
 */

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class GuiaRemisionController {

	public static final Logger logger = LogManager.getLogger(GuiaRemisionController.class);

	@Autowired
	private GuiaRemisionService guiaRemisionService;

	@GetMapping(value = "/empresas/{id-empresa}/guias", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GuiaRemision> listarGuiasRemisionPorEmpresa(@PathVariable("id-empresa") Integer idEmpresa) {
		return guiaRemisionService.listarTodasLasGuiasPorEmpresaNativo(idEmpresa);
	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV0", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarGuiasNoLiquidadas(@PathVariable("id-empresa") Integer idEmpresa,
													 @RequestParam(name="origen" ) Integer idOrigen,
													 @RequestParam(name="destino") Integer idDestino,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		try {
			Empresa empresa=new Empresa();
			empresa.setIdempresa(idEmpresa);
			ClienteFactoria cliOrigen = new ClienteFactoria();
			cliOrigen.setIdclientefactoria(idOrigen);
			ClienteFactoria cliDestino = new ClienteFactoria();
			cliDestino.setIdclientefactoria(idDestino);

			Liquidacion liquidacion = new Liquidacion();
			liquidacion.setEmpresaIdempresa(empresa);
			liquidacion.setClientefactoriaOrigen(cliOrigen);
			liquidacion.setClientefactoriaDestino(cliDestino);
			liquidacion.setFechainitraslado(fechaIni);
			liquidacion.setFechafintraslado(fechaFin);
			
			List<GuiaRemision> grilla = guiaRemisionService.listarGuiasParaLiquidacion(liquidacion);
		
		    if (!grilla.isEmpty()) {
				return new ResponseEntity<List<GuiaRemision>>(grilla, 
						HeaderUtil.createEntityCreationAlert("guia.query", "listaGrilla"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("GRT108", "No existen guias registradas para los parametros seleccionados."),
							HeaderUtil.createFailureAlert("guias", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
		
	
	}
	
	/* Busqueda generica para listado de Guias y como llave serie y secuencia de Guia de la Empresa */
	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV2", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarGuiasPorFiltro(@PathVariable("id-empresa") Integer idEmpresa,
													 @RequestParam(name="nroSerie" ) String nroSerie,
													 @RequestParam(name="nroSecuencia" ) String nroSecuencia,
													 @RequestParam(name="chofer" ) Integer idChofer,
													 @RequestParam(name="origen" ) Integer idOrigen,
													 @RequestParam(name="destino") Integer idDestino,
													 @RequestParam(name="estado") Integer idEstado,
													 @RequestParam(name="conFormateo" ) boolean conFormateo,
													 @RequestParam(name="tipoBusqueda" ) Integer tipoBusqueda,
													 //@RequestParam(name="guiasSinLiqFact" ) boolean guiasSinLiqFact,
													 // @RequestParam(name="soloFacturadas" ) boolean soloGuiasFacturadas,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		try {
			List<GuiaRemision> listaGuiasRemisionFiltro = new ArrayList<>();
			List<GuiaRemisionDTO> grillaFinalGuias = new ArrayList<>();
			Optional<GuiaRemision> guiaRemisionQuery = null;
			
			// Busqueda exacta por nroSerie y nroSecuencia Empresa
			if ((nroSerie != null && nroSerie != "") && (nroSecuencia != null && nroSecuencia != "")){
				guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaEmpresa(nroSerie, nroSecuencia, idEmpresa);
				if (guiaRemisionQuery.isPresent()) {					
					if (tipoBusqueda == BusquedaGuias.GUIAS_NO_FACTURADAS.getValue()) {
						// No mostrar guias liquidadas o facturadas
						if (guiaRemisionQuery.get().getDocumentoIddocumento()!= null || guiaRemisionQuery.get().getLiquidacionIdliquidacion() != null) {
							return new ResponseEntity<CustomErrorType>(
									new CustomErrorType("MFE3003", "Guia " + nroSerie + "-" + nroSecuencia+  " se encuentra asociada a una liquidación/factura."),
									HeaderUtil.createFailureAlert("guia-remision", "", ""),
									HttpStatus.NOT_FOUND);
						} else {
							// No mostrar guias que esten anuladas
							if (guiaRemisionQuery.get().getEstadoguia() == EstadoGuia.ANULADO.getValue()) {
								return new ResponseEntity<CustomErrorType>(
										new CustomErrorType("MFE3003", "Guia " + nroSerie + "-" + nroSecuencia+  "  se encuentra anulada !!"),
										HeaderUtil.createFailureAlert("guia-remision", "", ""),
										HttpStatus.NOT_FOUND);
							} else {
								listaGuiasRemisionFiltro.add(guiaRemisionQuery.get());
							}
							
							
						}
					} else {
						listaGuiasRemisionFiltro.add(guiaRemisionQuery.get());
					}

				} else {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFE3003", "Guia " + nroSerie + "-" + nroSecuencia+  " no existe en el sistema."),
							HeaderUtil.createFailureAlert("guia-remision", "", ""),
							HttpStatus.NOT_FOUND);
				}		
			} else {
				// Busqueda por coincidencia otros filtros incluidos serie y secuencia.
				listaGuiasRemisionFiltro = guiaRemisionService.listarGuiasPorFiltrosConCriteria(nroSerie, nroSecuencia,
						idEmpresa, idEstado, idChofer, idOrigen, idDestino, fechaIni, fechaFin, tipoBusqueda);
			}
				
		    if (!listaGuiasRemisionFiltro.isEmpty()) {
		    	if (conFormateo) {
					grillaFinalGuias = guiaRemisionService.obtenerDatosParaGrilla(listaGuiasRemisionFiltro,tipoBusqueda);
					return new ResponseEntity<List<GuiaRemisionDTO>>(grillaFinalGuias, 
							HeaderUtil.createEntityCreationAlert("guia-remision.query", "grilla"), 
							HttpStatus.OK);
		    	} else {
					return new ResponseEntity<List<GuiaRemision>>(guiaRemisionService.actualizarTarifasEnListado(listaGuiasRemisionFiltro), 
							HeaderUtil.createEntityCreationAlert("guia-remision.query", "nativo"), 
							HttpStatus.OK);
		    	}
		     }else{
	
		    	if (tipoBusqueda == BusquedaGuias.GUIAS_NO_FACTURADAS.getValue()) {  // guiasSinLiqFact
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3003", "No existen guias registradas o pendientes de liquidar/facturar para los filtros indicados."),
							HeaderUtil.createFailureAlert("guia-remision", "", ""),
							HttpStatus.NOT_FOUND);
		    	} else {
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen guias registradas para los filtros indicados."),
							HeaderUtil.createFailureAlert("guia-remision", "", ""),
							HttpStatus.NOT_FOUND);
		    	}
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
		
	}
	
	
	/* Busqueda generica para listado de Guias y como llave serie y secuencia de Guia del Cliente */
	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV6", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarGuiasPorGuiaDelCliente(@PathVariable("id-empresa") Integer idEmpresa,
													 @RequestParam(name="nroSerieCli" ) String nroSerieCli,
													 @RequestParam(name="nroSecuenciaCli" ) String nroSecuenciaCli,
													 @RequestParam(name="chofer" ) Integer idChofer,
													 @RequestParam(name="origen" ) Integer idOrigen,
													 @RequestParam(name="destino") Integer idDestino,
													 @RequestParam(name="estado") Integer idEstado,
													 @RequestParam(name="conFormateo" ) boolean conFormateo,
													 @RequestParam(name="tipoBusqueda" ) Integer tipoBusqueda,
													 //@RequestParam(name="guiasSinLiqFact" ) boolean busquedaSinLiqFac,
													 //@RequestParam(name="soloFacturadas" ) boolean soloGuiasFacturadas,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		try {
			List<GuiaRemision> listaGuiasRemisionFiltro = new ArrayList<>();
			List<GuiaRemisionDTO> grillaFinalGuias = new ArrayList<>();
			Optional<GuiaRemision> guiaRemisionQuery = null;
			
			if((nroSerieCli != null && nroSerieCli != "") && (nroSecuenciaCli != null && nroSecuenciaCli != "")) {
				//Búsqueda exacta por nro de Serie y Secuencia Guia del Cliente
				guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaCliente(nroSerieCli, nroSecuenciaCli, idEmpresa);
				if (guiaRemisionQuery.isPresent()) {
					if (tipoBusqueda == BusquedaGuias.GUIAS_NO_FACTURADAS.getValue()) {  // busquedaSinLiqFac
						// No mostrar guias que ya esten liquidadas o facturadas
						if (guiaRemisionQuery.get().getDocumentoIddocumento()!= null || guiaRemisionQuery.get().getLiquidacionIdliquidacion() != null) {
							return new ResponseEntity<CustomErrorType>(
									new CustomErrorType("MFE3003", "Guia Cliente " + nroSerieCli + "-" + nroSecuenciaCli+  " se encuentra asociada a una liquidación/factura."),
									HeaderUtil.createFailureAlert("guia-remision", "", ""),
									HttpStatus.NOT_FOUND);
						} else {
							// Guias Anuladas no deben mostrarse en este listado, ya que solo se quiere mostrar guias que no esten liquidadas o facturadas
							if (guiaRemisionQuery.get().getEstadoguia() == EstadoGuia.ANULADO.getValue()) {
								return new ResponseEntity<CustomErrorType>(
										new CustomErrorType("MFE3003", "Guia " + guiaRemisionQuery.get().getNroserieguia() + "-" + guiaRemisionQuery.get().getNrosecuenciaguia()+  " se encuentra anulada !!"),
										HeaderUtil.createFailureAlert("guia-remision", "", ""),
										HttpStatus.NOT_FOUND);
							} else {
								listaGuiasRemisionFiltro.add(guiaRemisionQuery.get());
							}
						}
					} else {
						listaGuiasRemisionFiltro.add(guiaRemisionQuery.get());
					}
					
				} else {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFE3003", "Guia Cliente " + nroSerieCli + "-" + nroSecuenciaCli+  " no existe en el sistema."),
							HeaderUtil.createFailureAlert("guia-remision", "", ""),
							HttpStatus.NOT_FOUND);
				}	
				
			} else {
				//Búsqueda por coincidencia con otros Filtros
				listaGuiasRemisionFiltro = guiaRemisionService.listarGuiasPorCoincidenciaNroSerieSecuenciaGuiaCliente(idEmpresa,nroSerieCli,nroSecuenciaCli,idEstado,idChofer,idOrigen,idDestino,fechaIni,fechaFin, tipoBusqueda);
			}
	
		    if (!listaGuiasRemisionFiltro.isEmpty()) {
		    	if (conFormateo) {
		    		grillaFinalGuias = guiaRemisionService.obtenerDatosParaGrilla(listaGuiasRemisionFiltro,tipoBusqueda);
					return new ResponseEntity<List<GuiaRemisionDTO>>(grillaFinalGuias, 
							HeaderUtil.createEntityCreationAlert("guia-remision.query", "grilla"), 
							HttpStatus.OK);
		    	} else {
					return new ResponseEntity<List<GuiaRemision>>(guiaRemisionService.actualizarTarifasEnListado(listaGuiasRemisionFiltro), 
							HeaderUtil.createEntityCreationAlert("guia-remision.query", "lista-nativa"), 
							HttpStatus.OK);		    	
				}

		    }else{
		    	if (tipoBusqueda == BusquedaGuias.GUIAS_NO_FACTURADAS.getValue()) { // busquedaSinLiqFac
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3003", "No existen guias registradas o pendientes de liquidar/facturar para los filtros indicados."),
							HeaderUtil.createFailureAlert("guia-remision", "", ""),
							HttpStatus.NOT_FOUND);
		    	} else {
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("MFE3002", "No existen guias registradas para los filtros indicados."),
							HeaderUtil.createFailureAlert("guia-remision", "", ""),
							HttpStatus.NOT_FOUND);
		    	}

		    }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
		
	}
	

	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV4", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarGuiasPorFacturar(@PathVariable("id-empresa") Integer idEmpresa,
													 @RequestParam(name="origen" ) Integer idOrigen,
													 @RequestParam(name="destino") Integer idDestino,
													 @RequestParam(name="fechaIni" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaIni,
													 @RequestParam(name="fechaFin" )@DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		
		try {


			List<GuiaRemision> guiasPorFacturar = guiaRemisionService.listarGuiasPorFacturar(idEmpresa,idOrigen,idDestino,fechaIni,fechaFin);
		
		    if (!guiasPorFacturar.isEmpty()) {
				return new ResponseEntity<List<GuiaRemision>>(guiasPorFacturar, 
						HeaderUtil.createEntityCreationAlert("guia.query", "listaGrilla"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("GRT108", "No existen guias registradas para los parametros seleccionados."),
							HeaderUtil.createFailureAlert("guias", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
		
	
	}
	

	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV1", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarGuiasParaGrilla(@PathVariable("id-empresa") Integer idEmpresa) {
		
		try {
			List<GuiaRemisionDTO> grilla = guiaRemisionService.listarTodasLasGuiasPorEmpresa(idEmpresa);
		
		    if (!grilla.isEmpty()) {
				return new ResponseEntity<List<GuiaRemisionDTO>>(grilla, 
						HeaderUtil.createEntityCreationAlert("guia.query", "listaGrilla"), 
						HttpStatus.OK);
		     }else{
					return new ResponseEntity<CustomErrorType>(new CustomErrorType("GRT105", "No existen guias registradas en el sistema para Empresa."),
							HeaderUtil.createFailureAlert("guias", "", ""),
							HttpStatus.NOT_FOUND);
		     }
		
		}catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}    
		
	
	}

	@GetMapping(value = "/empresas/{id-empresa}/guias/{id-guia}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerGuiaRemisionPorId(@PathVariable("id-empresa") Integer idEmpresa,
													  @PathVariable("id-guia") Integer idGuiaParam) {

		try {
			Optional<GuiaRemision> guiaRemisionOpt = guiaRemisionService.obtenerGuiaRemisionPorId(idGuiaParam,
					idEmpresa);

			if (guiaRemisionOpt.isPresent()) {
				return new ResponseEntity<GuiaRemision>(guiaRemisionOpt.get(), 
							HeaderUtil.createEntityCreationAlert("guia.query", String.valueOf(guiaRemisionOpt.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT104", "Guia de remisión no existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV3", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerGuiaRemisionPorNroSerie(@PathVariable("id-empresa") Integer idEmpresa,
															 @RequestParam(name="nroSerie" ) String nroSerie,
															 @RequestParam(name="nroSecuencia") String nroSecuencia) 
	{

		try {

			Optional<GuiaRemision> guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaEmpresa(nroSerie, nroSecuencia,idEmpresa);
			
			if (guiaRemisionQuery.isPresent()) {
				return new ResponseEntity<GuiaRemision>(guiaRemisionQuery.get(), 
							HeaderUtil.createEntityCreationAlert("guia.query", String.valueOf(guiaRemisionQuery.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT104", "Guia de remisión no existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/guias/SRV5", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarExisteGuiaRemisionPorNroSerie(@PathVariable("id-empresa") Integer idEmpresa,
															 @RequestParam(name="nroSerie" ) String nroSerie,
															 @RequestParam(name="nroSecuencia") String nroSecuencia) 
	{
		CustomResponseType responseType = new CustomResponseType();
		logger.info("-----------validarExisteGuiaRemisionPorNroSerieCliente -------" );
		logger.info("Empresa: " +  idEmpresa);
		logger.info("GuiaRemision  : " +  nroSerie + '-'+ nroSecuencia);
		
		try {

			// Valida que Guia ingresada no exista en el sistema
			Optional<GuiaRemision> guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaEmpresa(nroSerie, nroSecuencia, idEmpresa);

			if (guiaRemisionQuery.isPresent()) {
				responseType.setCodeMessage("GRT102");
				responseType.setAlertMessage("Guia: " + nroSerie + "-" + nroSecuencia + " ya existe en el sistema.");
			} else {
				responseType.setCodeMessage("MFE3003");
				responseType.setAlertMessage("Guia: " + nroSerie + "-" + nroSecuencia+  " no existe en el sistema.");
			}
			
			logger.info("GuiaRemision  : " +  nroSerie + '-'+ nroSecuencia + " validada correctamente");
			logger.info("-------------------------------------------------" );
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("guia.validated", "OK"),HttpStatus.ACCEPTED);

	}
	
	@GetMapping(value = "/empresas/{id-empresa}/guias-cliente/SRV5", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarExisteGuiaRemisionPorNroSerieCliente(@PathVariable("id-empresa") Integer idEmpresa,
															 @RequestParam(name="nroSerie" ) String nroSerieCliente,
															 @RequestParam(name="nroSecuencia") String nroSecuenciaCliente) 
	{
		CustomResponseType responseType = new CustomResponseType();
		logger.info("-----------validarExisteGuiaRemisionPorNroSerieCliente -------" );
		logger.info("Empresa: " +  idEmpresa);
		logger.info("GuiaRemision Cliente : " +  nroSerieCliente + '-'+ nroSecuenciaCliente);
	
		
		try {

			// Valida que Guia ingresada no exista en el sistema
			Optional<GuiaRemision> guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaCliente(nroSerieCliente, nroSecuenciaCliente, idEmpresa);

			if (guiaRemisionQuery.isPresent()) {
				responseType.setCodeMessage("GRT102");
				responseType.setAlertMessage("Guia Cliente: " + nroSerieCliente + "-" + nroSecuenciaCliente + " ya existe en el sistema.");
			} else {
				responseType.setCodeMessage("MFE3003");
				responseType.setAlertMessage("Guia Cliente: " + nroSerieCliente + "-" + nroSecuenciaCliente+  " no existe en el sistema.");
			}
			
			logger.info("GuiaRemision Cliente : " +  nroSerieCliente + '-'+ nroSecuenciaCliente +  " validada correctamente");
			logger.info("-------------------------------------------------" );
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("guia.validated", "OK"),HttpStatus.ACCEPTED);

	}
	
	
	@GetMapping(value = "/empresas/{id-empresa}/guia-cliente/SRV3", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerGuiaRemisionPorNroSerieCliente(@PathVariable("id-empresa") Integer idEmpresa,
															 @RequestParam(name="nroSerieCli" ) String nroSerieCli,
															 @RequestParam(name="nroSecuenciaCli") String nroSecuenciaCli) 
	{

		try {

			Optional<GuiaRemision> guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaCliente(nroSerieCli, nroSecuenciaCli, idEmpresa);
			
			if (guiaRemisionQuery.isPresent()) {
				return new ResponseEntity<GuiaRemision>(guiaRemisionQuery.get(), 
							HeaderUtil.createEntityCreationAlert("guia.query", String.valueOf(guiaRemisionQuery.get().hashCode())), 
							HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT104", "Guia de remisión no existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.NOT_FOUND);
			}
		}

		catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	
	
	@PostMapping(value = "/guias", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarGuia(@Valid @RequestBody GuiaRemision guiaRemisionForm) {

		CustomResponseType responseType = new CustomResponseType();
		GuiaRemision guiaRemision = null;
		logger.info("-----------registrarGuia -------" );
		logger.info("Empresa: " +  guiaRemisionForm.getEmpresaIdempresa().getIdempresa());
		logger.info("GuiaRemision: " +  guiaRemisionForm.getNroserieguia() + '-'+ guiaRemisionForm.getNrosecuenciaguia());
	
		try {
			// Valida que Guia ingresada no exista en el sistema
			Optional<GuiaRemision> guiaRemisionQuery = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaEmpresa(
					guiaRemisionForm.getNroserieguia(), guiaRemisionForm.getNrosecuenciaguia(),guiaRemisionForm.getEmpresaIdempresa().getIdempresa());

			if (guiaRemisionQuery.isPresent()) {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT102",
											"Guia de remisión: " + guiaRemisionForm.getNroserieguia() + "-"
											+ guiaRemisionForm.getNrosecuenciaguia() 
											+ " ya existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""), HttpStatus.CONFLICT);
			} else {

				// Valida que Guia Cliente ingresada no exista en el sistema
				Optional<GuiaRemision> guiaRemisionQueryCliente = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaCliente(
						guiaRemisionForm.getNroserieguiacliente(), guiaRemisionForm.getNrosecuenguiacliente(),guiaRemisionForm.getEmpresaIdempresa().getIdempresa());

				if (guiaRemisionQueryCliente.isPresent()) {
					if (guiaRemisionQueryCliente.get().getEstadoguia() == EstadoGuia.REGISTRADO.getValue()) {
						return new ResponseEntity<CustomErrorType>(
								new CustomErrorType("GRT102",
													"Guia Cliente : " + guiaRemisionForm.getNroserieguiacliente() + "-"
													+ guiaRemisionForm.getNrosecuenguiacliente()
													+ " ya existe en el sistema."),
								HeaderUtil.createFailureAlert("guias", "", ""), HttpStatus.CONFLICT);
					}
				}
				guiaRemision = guiaRemisionService.registrarGuiaRemision(guiaRemisionForm);
				responseType.setCodeMessage("GRT000");
				responseType.setAlertMessage("Guia de remisión: " + guiaRemisionForm.getNroserieguia() + "-"
						+ guiaRemisionForm.getNrosecuenciaguia() + " registrada correctamente.");
			
				logger.info("Guia de remisión: " + guiaRemisionForm.getNroserieguia() + "-"
						+ guiaRemisionForm.getNrosecuenciaguia() + " registrada correctamente.");
				logger.info("-------------------------------------------------" );
				
			}

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("guia.created", String.valueOf(guiaRemision.hashCode())),
				HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/guias/{id-guia}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarGuia(@PathVariable("id-guia") Integer idGuiaRem,
											@Valid @RequestBody GuiaRemision guiaRemisionUpdate) {
		
		guiaRemisionUpdate.setIdguiaremision(idGuiaRem);
		CustomResponseType responseType = new CustomResponseType();
		GuiaRemision guiaRemision = null;
		
		logger.info("-----------actualizarGuia -------" );
		logger.info("Empresa: " +  guiaRemisionUpdate.getEmpresaIdempresa().getIdempresa());
		logger.info("GuiaRemision: " +  guiaRemisionUpdate.getNroserieguia() + '-'+ guiaRemisionUpdate.getNrosecuenciaguia());
	
		try {
			
				//Actualiza Guia de Remision
				guiaRemision = guiaRemisionService.actualizarGuiaRemision(guiaRemisionUpdate);
				
				//Prepara mensaje
				responseType.setCodeMessage("GRT000");
				responseType.setAlertMessage("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " actualizada correctamente.");
				
				logger.info("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " actualizada  correctamente.");
				logger.info("-------------------------------------------------" );

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("guia.updated", String.valueOf(guiaRemision.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	@PutMapping(value = "/guias/{id-guia}/tarifa", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarTarifaGuia(@PathVariable("id-guia") Integer idGuiaRem,
											@Valid @RequestBody GuiaRemision guiaRemisionUpdate) {
		
		guiaRemisionUpdate.setIdguiaremision(idGuiaRem);
		CustomResponseType responseType = new CustomResponseType();
		GuiaRemision guiaRemision = null;
		
		logger.info("-----------actualizarTarifaGuia -------" );
		logger.info("Empresa: " +  guiaRemisionUpdate.getEmpresaIdempresa().getIdempresa());
		logger.info("GuiaRemision: " +  guiaRemisionUpdate.getNroserieguia() + '-'+ guiaRemisionUpdate.getNrosecuenciaguia());
	
		try {
			
				//Actualiza Guia de Remision
				guiaRemision = guiaRemisionService.actualizarTarifaGuiaRemision(guiaRemisionUpdate);
				
				//Prepara mensaje
				responseType.setCodeMessage("GRT000");
				responseType.setAlertMessage("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " actualizada correctamente.");
				
				logger.info("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " actualizada  correctamente.");
				logger.info("-------------------------------------------------" );

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("guia.updated", String.valueOf(guiaRemision.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	
	@PutMapping(value = "/guias/{id-guia}/extorno", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> extornarGuiaRemision(@PathVariable("id-guia") Integer idGuiaRem,
											@Valid @RequestBody GuiaRemision guiaRemisionExtornar) {
		
		// guiaRemisionExtornar.setIdguiaremision(idGuiaRem);
		CustomResponseType responseType = new CustomResponseType();
		GuiaRemision guiaRemision = null;
		
		logger.info("-----------extornarGuiaRemision -------" );
		logger.info("Empresa: " +  guiaRemisionExtornar.getEmpresaIdempresa().getIdempresa());
		logger.info("GuiaRemision: " +  guiaRemisionExtornar.getNroserieguia() + '-'+ guiaRemisionExtornar.getNrosecuenciaguia());
	
		try {
			
				//Extorna Guia de Remision
				guiaRemision = guiaRemisionService.extornarGuiaRemision(guiaRemisionExtornar);
				
				//Prepara mensaje
				responseType.setCodeMessage("GRT000");
				responseType.setAlertMessage("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " anulada correctamente.");
				logger.info("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " anulada  correctamente.");
				logger.info("-------------------------------------------------" );

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("guia.updated", String.valueOf(guiaRemision.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	@PutMapping(value = "/liquidaciones/{id-liquidacion}/guias/{id-guia}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarGuiaLiquidacion(@PathVariable("id-guia") Integer idGuiaRem,
														@PathVariable("id-liquidacion") Integer idLiquidacion,
											@Valid @RequestBody GuiaRemision guiaRemisionUpdate) {
		
		guiaRemisionUpdate.setIdguiaremision(idGuiaRem);
		CustomResponseType responseType = new CustomResponseType();
		GuiaRemision guiaRemision = null;
		logger.info("-----------actualizarGuiaLiquidacion -------" );
		logger.info("id-guia: " +  idGuiaRem);
		logger.info("id-liquidacion: " +  idLiquidacion);
		logger.info("GuiaRemision: " +  guiaRemisionUpdate.getNroserieguia() + '-'+ guiaRemisionUpdate.getNrosecuenciaguia());
	
		
		try {
	
				//Actualiza Guia de Remision
				guiaRemision = guiaRemisionService.actualizarGuiaRemisionLiquidacion(idGuiaRem,idLiquidacion);
				
				//Prepara mensaje
				responseType.setCodeMessage("GRT000");
				responseType.setAlertMessage("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " actualizada correctamente.");
				
				logger.info("Guia de remisión: " + guiaRemision.getNroserieguia() + "-"
						+ guiaRemision.getNrosecuenciaguia() + " actualizada  correctamente.");
				logger.info("-------------------------------------------------" );

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityUpdateAlert("guia.updated", String.valueOf(guiaRemision.hashCode())),
				HttpStatus.ACCEPTED);
	}
	
	
	@PostMapping(value = "/empresas/{id-empresa}/guias/filtros", 
				consumes = MediaType.APPLICATION_JSON_VALUE, 
				produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<?> listarGuiasRemisionPorFiltro(@PathVariable("id-empresa") Integer idEmpresa,
			@Valid @RequestBody FiltrosMaestro filtrosMaestro) {

		try {
			FiltrosGuiaRemision filtrosGuiaRemision = filtrosMaestro.getFiltrosGuiaRemision();
			filtrosGuiaRemision.setIdEmpresaGuia(idEmpresa);

			List<GuiaRemisionDTO> lista = guiaRemisionService.listarTodasLasGuiasPorFiltro(filtrosGuiaRemision);

			if (!lista.isEmpty()) {
				return new ResponseEntity<List<GuiaRemisionDTO>>(lista,
						HeaderUtil.createEntityCreationAlert("guia.query", "listaGrillaFiltro"), HttpStatus.OK);
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT106", "No existen guias registradas con los filtros seleccionados."),
						HeaderUtil.createFailureAlert("guias", "", ""), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getClass().getName(),e);
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("GRT301", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

	}
	
	@DeleteMapping(value = "/empresas/{id-empresa}/guias/{serie}/{secuencia}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eliminarGuiaRemision(@PathVariable("id-empresa") Integer idEmpresa, 
												  @PathVariable("serie") String serie,
		  										  @PathVariable("secuencia") String secuencia) {

		CustomResponseType responseType = new CustomResponseType();
		logger.info("-----------eliminarGuiaRemision -------" );
		logger.info("serie: " +  serie);
		logger.info("secuencia: " +  secuencia);
	

		try {
			Optional<GuiaRemision> guiaRemisionBD = guiaRemisionService.obtenerGuiaRemisionPorNroGuiaEmpresa(serie, secuencia, idEmpresa);
			
			if (guiaRemisionBD.isPresent()) {					
				if (guiaRemisionBD.get().getDocumentoIddocumento()!= null) {
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("GRT109", "Guia de remision "+ guiaRemisionBD.get().getNroserieguia() + "-" + guiaRemisionBD.get().getNrosecuenciaguia()+ " tiene una Factura asociada. No procede la eliminación" ),
							HeaderUtil.createFailureAlert("guias", "", ""),
							HttpStatus.CONFLICT);
				} else {
					if (guiaRemisionBD.get().getLiquidacionIdliquidacion() != null) {
						return new ResponseEntity<CustomErrorType>(
								new CustomErrorType("GRT109", "Guia de remision "+ guiaRemisionBD.get().getNroserieguia() + "-" + guiaRemisionBD.get().getNrosecuenciaguia()+ " tiene una Liquidación asociada. No procede la eliminación" ),
								HeaderUtil.createFailureAlert("guias", "", ""),
								HttpStatus.CONFLICT);
					} else {
						// Elimina Guia de base de datos, siempre y cuando no tenga relación con : liquidacion o factura
						guiaRemisionService.eliminarGuiaBD(guiaRemisionBD.get().getIdguiaremision());
						logger.info("Guia de remisión: " + guiaRemisionBD.get().getNroserieguia() + "-"
								+ guiaRemisionBD.get().getNrosecuenciaguia() + " eliminada  de la BD.");
						logger.info("-------------------------------------------------" );
					}
				}
						
			} else {
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("GRT104", "Guia de remisión no existe en el sistema."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.NOT_FOUND);
			}
			
			// Prepara mensaje
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Guia de Remisión " + serie + "-" + secuencia + " eliminada correctamente de la base de datos !!");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("guias.delete", serie.toString()),
				HttpStatus.OK);
	}

}
