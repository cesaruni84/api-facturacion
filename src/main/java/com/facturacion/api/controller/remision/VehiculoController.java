package com.facturacion.api.controller.remision;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Vehiculo;
import com.facturacion.api.service.remision.VehiculoService;

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class VehiculoController {

	
	@Autowired
	private VehiculoService vehiculoService;

	@GetMapping(value = "/empresas/vehiculos",  produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Vehiculo> listarTodosLosVehiculos() {
		return vehiculoService.listarTodasLosVehiculos();

	}
	@PostMapping(value = "/empresas/vehiculos", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarVehiculo(@Valid @RequestBody Vehiculo vehiculoForm) {

		CustomResponseType responseType = new CustomResponseType();
		Vehiculo vehiculo = null;
		try {
			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			vehiculoForm.setFecharegistro(fechaTimestamp);
			vehiculoForm.setFechaactualiza(fechaTimestamp);

			vehiculo = vehiculoService.registrarVehiculo(vehiculoForm);
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Vehiculo registrado correctamente");
		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("vehiculo", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("vehiculo.created", String.valueOf(vehiculo.hashCode())),
				HttpStatus.CREATED);
	}
	
	

	@PutMapping(value = "/empresas/vehiculos/{id-vehiculo}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarVehiculo(@PathVariable("id-vehiculo") Integer idVehiculo,
													@Valid @RequestBody Vehiculo vehiculoForm) {
		
		vehiculoForm.setIdvehiculo(idVehiculo);
		CustomResponseType responseType = new CustomResponseType();
		Vehiculo vehiculo = null;
		try {
			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			vehiculoForm.setFechaactualiza(fechaTimestamp);
		
			//Actualiza vehiculo
			vehiculo = vehiculoService.actualizarVehiculo(vehiculoForm);
			
			//Prepara mensaje
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Vehiculo actualizado correctamente");


		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("vehiculo", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("vehiculo.updated", String.valueOf(vehiculo.hashCode())),
				HttpStatus.CREATED);
	}
}
