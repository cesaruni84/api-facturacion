/**
 * 
 */
package com.facturacion.api.controller.remision;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.model.entity.UnidadMedida;
import com.facturacion.api.service.remision.UnidadMedidaService;

/**
 * @author CESAR
 *
 */
@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class UnidadMedidaController {

	@Autowired
	private UnidadMedidaService unidadMedidaService;

	@GetMapping(value = "/unidadesMedida", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UnidadMedida> listarComboFactorias() {
		return unidadMedidaService.listarTodasLasUnidadesMedidas();

	}
	
	
}
