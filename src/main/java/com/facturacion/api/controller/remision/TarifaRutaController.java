package com.facturacion.api.controller.remision;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.CustomResponseType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.TarifaRuta;
import com.facturacion.api.service.remision.TarifaRutaService;

@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class TarifaRutaController {
	
	
	@Autowired
	private TarifaRutaService tarifaRutaService;
	
	@GetMapping(value = "/empresas/{id-empresa}/tarifas")
	public List<TarifaRuta> listarTarifaPorRuta() {
		List<TarifaRuta> listadoTarifas = tarifaRutaService.listarTarifas();
		return listadoTarifas;
	}
	

	@PostMapping(value = "/empresas/{id-empresa}/tarifas", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarTarifaRuta(@Valid @RequestBody TarifaRuta tarifaForm) {

		CustomResponseType responseType = new CustomResponseType();
		TarifaRuta tarifa = null;
		try {
			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			tarifaForm.setFecharegistrosistema(fechaTimestamp);
			tarifaForm.setFechaactualizasistema(fechaTimestamp);
			tarifa = tarifaRutaService.registrarTarifa(tarifaForm);
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Tarifa registrada correctamente");

		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Tarifa ingresada ya existe en el sistema."),
					HeaderUtil.createFailureAlert("tarifa", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
			
		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("tarifa", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("tarifa.created", String.valueOf(tarifa.hashCode())),
				HttpStatus.CREATED);
	}
	
	

	@PutMapping(value = "/empresas/{id-empresa}/tarifas/{id-tarifa}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> actualizarTarifaRuta(@PathVariable("id-tarifa") Integer idTarifa,
													@Valid @RequestBody TarifaRuta tarifaForm) {
		
		// tarifaForm.setTarifaRutaPK(tarifaRutaPK);
		CustomResponseType responseType = new CustomResponseType();
		TarifaRuta tarifa = null;
		
		try {
				// TimeStamp
				Timestamp ts = new Timestamp(System.currentTimeMillis());
				Date fechaTimestamp = new Date(ts.getTime());
				tarifaForm.setFechaactualizasistema(fechaTimestamp);
			
				//Actualiza tarifa
				tarifa = tarifaRutaService.actualizarTarifa(tarifaForm);
				
				//Prepara mensaje
				responseType.setCodeMessage("MFA0000");
				responseType.setAlertMessage("Tarifa actualizada correctamente");


		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("tarifa", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		} 
		
		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("tarifa.updated", String.valueOf(tarifa.hashCode())),
				HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/empresas/{id-empresa}/tarifas/{id-tarifa}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eliminarTarifaRuta(@PathVariable("id-tarifa") Integer idTarifaRuta) {

		CustomResponseType responseType = new CustomResponseType();

		try {
			// Elimina tarifa de base de datos
			tarifaRutaService.eliminarTarifaBD(idTarifaRuta);

			// Prepara mensaje
			responseType.setCodeMessage("MFA0000");
			responseType.setAlertMessage("Tarifa eliminada correctamente de la base de datos !!");

		} catch (Exception e) {
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("tarifa", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<CustomResponseType>(responseType,
				HeaderUtil.createEntityCreationAlert("tarifa.delete", idTarifaRuta.toString()),
				HttpStatus.OK);
	}

}
