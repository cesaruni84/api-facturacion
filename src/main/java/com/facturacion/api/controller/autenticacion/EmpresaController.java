/**
 * 
 */
package com.facturacion.api.controller.autenticacion;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.model.entity.Empresa;
import com.facturacion.api.service.autenticacion.EmpresaService;


/**
 * @author CESAR
 *
 */
@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class EmpresaController {
	
	@Autowired
	private EmpresaService empresaService;

	@GetMapping(value = "/empresas")
	public List<Empresa> listarComboEmpresas() {
		return empresaService.listarEmpresas();

	}
	
	@GetMapping(value = "empresas/{id-empresa}")
	public ResponseEntity<Empresa> listarPorId(@PathVariable("id-empresa") Integer idEmpresa) {

		return empresaService.obtenerEmpresaPorId(idEmpresa).map(ResponseEntity::ok)
				.orElseGet(() -> ResponseEntity.notFound().build());

	}
}
