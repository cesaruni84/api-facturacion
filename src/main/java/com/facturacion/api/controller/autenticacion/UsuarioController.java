package com.facturacion.api.controller.autenticacion;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.facturacion.api.controller.util.CustomErrorType;
import com.facturacion.api.controller.util.HeaderUtil;
import com.facturacion.api.model.entity.Usuario;
import com.facturacion.api.model.form.UsuarioForm;
import com.facturacion.api.service.autenticacion.UsuarioService;


@RestController
@RequestMapping(value = "/v1/sisyfac/api")
public class UsuarioController {


	@Autowired
	private UsuarioService usuarioService;

	@PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarLoginUsuario(@Valid @RequestBody UsuarioForm usuarioForm) {
		
		
		//Optional<Usuario> usuarioOpt = null;
		List<Usuario> listaUsuarioxEmpresa = null;
		Usuario usuarioDTO = null;
		boolean accesoAEmpresa = false;
		
		try {
			
			// Accesos de Usuario por Empresa
			listaUsuarioxEmpresa = usuarioService.validarLoginUsuarioEmpresa(usuarioForm);
			
			if (!listaUsuarioxEmpresa.isEmpty()) {
				Integer IdEmpresaSelect = usuarioForm.getTxtCodigoEmpresa();
				for (Usuario usuarioOpt : listaUsuarioxEmpresa) {
					Integer idEmpresa = usuarioOpt.getEmpresaIdempresa().getIdempresa();
					if (idEmpresa.equals(IdEmpresaSelect)) {
						 accesoAEmpresa = true;
						 usuarioDTO = usuarioOpt;
						 break;
					}
				}
				
				if (!accesoAEmpresa) {
					//Mensaje de retorno de empresa no accesible a usuario
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFA2001", "Usuario no tiene tiene permiso para acceder a empresa seleccionada."),
							HeaderUtil.createFailureAlert("guias", "", ""),
							HttpStatus.NOT_FOUND);
				}
				
			} else {
				//mostrar mensaje usuario o contraseña incorrecta
				return new ResponseEntity<CustomErrorType>(
						new CustomErrorType("MFA2002", "Usuario y/o contraseña incorrectos. Intente nuevamente."),
						HeaderUtil.createFailureAlert("guias", "", ""),
						HttpStatus.FORBIDDEN);
			}
			
/*			
			for (Usuario usuarioOpt : listaUsuarioxEmpresa) {
				if (usuarioOpt != null) {
					//usuarioDTO = usuarioOpt.get();
					Integer idEmpresa = usuarioOpt.getEmpresaIdempresa().getIdempresa();
					Integer IdEmpresaSelect = usuarioForm.getTxtCodigoEmpresa();
					
					if (!idEmpresa.equals(IdEmpresaSelect)) {
						//Mensaje de retorno de empresa no relacionada a usuario
						return new ResponseEntity<CustomErrorType>(
								new CustomErrorType("MFA2001", "Usuario no tiene tiene permiso para acceder a empresa seleccionada."),
								HeaderUtil.createFailureAlert("guias", "", ""),
								HttpStatus.NOT_FOUND);
					}
		
				}else {
					//mostrar mensaje usuario o contraseña incorrecta
					return new ResponseEntity<CustomErrorType>(
							new CustomErrorType("MFA2002", "Usuario y/o contraseña incorrectos. Intente nuevamente."),
							HeaderUtil.createFailureAlert("guias", "", ""),
							HttpStatus.FORBIDDEN);
				}
			}*/
			
			// usuarioOpt = usuarioService.validarLoginUsuario(usuarioForm);


			
		} catch (Exception e) {
			//e.printStackTrace();
			return new ResponseEntity<CustomErrorType>(
					new CustomErrorType("MFE3001", "Ocurrió un error inesperado. Contacte al administrador."),
					HeaderUtil.createFailureAlert("guias", e.getMessage(), e.getLocalizedMessage()),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Usuario>(usuarioDTO, 
										HeaderUtil.createEntityCreationAlert("usuario.consulta", usuarioDTO.getCodigousuario()),
										HttpStatus.OK);
	}
	

}
