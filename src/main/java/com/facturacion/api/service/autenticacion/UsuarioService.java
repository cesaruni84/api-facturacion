/**
 * 
 */
package com.facturacion.api.service.autenticacion;

import java.util.List;
import java.util.Optional;

import com.facturacion.api.model.entity.Usuario;
import com.facturacion.api.model.form.UsuarioForm;

/**
 * @author CESAR
 *
 */
public interface UsuarioService {
	
	
	Optional<Usuario> validarLoginUsuario(UsuarioForm username) throws Exception;
	
	
	List<Usuario> validarLoginUsuarioEmpresa(UsuarioForm username) throws Exception;



}
