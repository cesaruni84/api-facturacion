package com.facturacion.api.service.autenticacion;

import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.entity.Empresa;


public interface EmpresaService {
	
	List<Empresa> listarEmpresas();
	Optional<Empresa> obtenerEmpresaPorId(Integer idEmpresa);


}
