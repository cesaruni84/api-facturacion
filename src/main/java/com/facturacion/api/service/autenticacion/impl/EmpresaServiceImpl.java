/**
 * 
 */
package com.facturacion.api.service.autenticacion.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.facturacion.api.model.entity.Empresa;
import com.facturacion.api.repository.EmpresaRepository;
import com.facturacion.api.repository.UsuarioRepository;
import com.facturacion.api.service.autenticacion.EmpresaService;

/**
 * @author CESAR
 *
 */
@Service
public class EmpresaServiceImpl implements EmpresaService {
	
	@Autowired
	private EmpresaRepository empresaRepository;
	

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.autenticacion.EmpresaService#listarEmpresas()
	 */
	@Override
	public List<Empresa> listarEmpresas() {
		// TODO Auto-generated method stub
		return empresaRepository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.autenticacion.EmpresaService#obtenerEmpresaPorId(java.lang.Integer)
	 */
	@Override
	public Optional<Empresa> obtenerEmpresaPorId(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return empresaRepository.findById(idEmpresa);
	}

}
