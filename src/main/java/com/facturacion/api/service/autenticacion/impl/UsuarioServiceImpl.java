/**
 * 
 */
package com.facturacion.api.service.autenticacion.impl;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.facturacion.api.model.entity.Usuario;
import com.facturacion.api.model.form.UsuarioForm;
import com.facturacion.api.repository.UsuarioRepository;
import com.facturacion.api.security.CryptographyGenerator;
import com.facturacion.api.service.autenticacion.UsuarioService;

/**
 * @author CESAR
 *
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.facturacion.api.service.autenticacion.UsuarioService#validarLoginUsuario(
	 * com.facturacion.api.model.form.UsuarioForm)
	 */

	@Override
	public Optional<Usuario>  validarLoginUsuario(UsuarioForm username) throws Exception {
		// TODO Auto-generated method stub
		return this.usuarioRepository.findByCodigoUsuarioAndKey(username.getTxtCodigoUsuario(),
				CryptographyGenerator.generateSHA256(username.getTxtClaveUsuario()));
	}


	@Override
	public List<Usuario> validarLoginUsuarioEmpresa(UsuarioForm username) throws Exception {
		// TODO Auto-generated method stub
		return this.usuarioRepository.findByCodigoUsuarioAndKeyWithShip(username.getTxtCodigoUsuario(),
				CryptographyGenerator.generateSHA256(username.getTxtClaveUsuario()));
	}

}
