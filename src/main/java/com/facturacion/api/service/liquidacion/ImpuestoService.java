package com.facturacion.api.service.liquidacion;

import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.entity.Impuesto;


public interface ImpuestoService {
	
	List<Impuesto> listarTodosLosImpuestos();
	Optional<Impuesto> obtenerImpuestoPorId(Integer idImpuesto);


}
