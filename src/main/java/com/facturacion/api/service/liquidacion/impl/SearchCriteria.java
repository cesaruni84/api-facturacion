/**
 * 
 */
package com.facturacion.api.service.liquidacion.impl;

/**
 * @author CESAR
 *
 */
public class SearchCriteria {
    private String key;
    private String operation;
    private Object value;
    
    
    
    
	/**
	 * @param key
	 * @param operation
	 * @param value
	 */
	public SearchCriteria(String key, String operation, Object value) {
		super();
		this.key = key;
		this.operation = operation;
		this.value = value;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
    
    
    
}
