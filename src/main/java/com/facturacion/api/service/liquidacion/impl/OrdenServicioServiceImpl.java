package com.facturacion.api.service.liquidacion.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.entity.OrdenServicio;
import com.facturacion.api.repository.LiquidacionRepository;
import com.facturacion.api.repository.OrdenServicioRepository;
import com.facturacion.api.service.liquidacion.OrdenServicioService;

@Service
public class OrdenServicioServiceImpl implements OrdenServicioService {
	
	
	@Autowired
	private OrdenServicioRepository ordenServicioRepository;
	
	@Autowired
	private LiquidacionRepository liquidacionRepository;


	@Override
	public Optional<OrdenServicio> obteneOrdenServicioPorNro(String nroOrdenServicio) {
		// TODO Auto-generated method stub
		return this.ordenServicioRepository.obtenerPorNro(nroOrdenServicio);
	}

	@Override
	public OrdenServicio registrarOrdenServicio(OrdenServicio ordenServicioCreate) {
		// TODO Auto-generated method stub
		
		// TimeStamp
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date fechaTimestamp = new Date(ts.getTime());

		// Fecha Registro y Actualizacion
		ordenServicioCreate.setFecharegistro(fechaTimestamp);
		ordenServicioCreate.setFechaactualiza(fechaTimestamp);
		
		Set<Liquidacion> listaLiquidaciones_post = new HashSet<Liquidacion>();
		Set<Liquidacion> listaLiquidacionesAsociadas = ordenServicioCreate.getLiquidacionSet();
		
		for (Liquidacion liquidacion : listaLiquidacionesAsociadas) {
			Optional<Liquidacion> liquidacionBD = liquidacionRepository.findById(liquidacion.getIdliquidacion());
			Liquidacion liquidacionBD_ = liquidacionBD.get();
			liquidacionBD_.setOrdenservicioIdordenservicio(ordenServicioCreate);
			listaLiquidaciones_post.add(liquidacionBD_);
		}

		ordenServicioCreate.setLiquidacionSet(listaLiquidaciones_post);
		return this.ordenServicioRepository.save(ordenServicioCreate);
	}

	@Override
	public OrdenServicio actualizarOrdenServicio(OrdenServicio ordenServicioUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<OrdenServicio> listarOrdenesServicioPorEmpresa(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return this.ordenServicioRepository.listarPorEmpresa(idEmpresa);
	}

	@Override
	public List<GuiaRemision> obtenerGuiasAsociadasOrdenServicio(OrdenServicio ordenServicioQuery) {
		// TODO Auto-generated method stub
		List<GuiaRemision> guiasAsociadasAOrden = new ArrayList<>();
		Set<Liquidacion> liquidacionesPorOrden = ordenServicioQuery.getLiquidacionSet();
		
		for (Liquidacion liquidacion : liquidacionesPorOrden) {
			Set<GuiaRemision> guiasRemisionBD = liquidacion.getGuiaRemisionSet();
			// guiasRemisionBD.addAll(guiasRemisionBD);	
			guiasAsociadasAOrden.addAll(guiasRemisionBD);
		}

		return guiasAsociadasAOrden;
	}

	@Override
	public List<OrdenServicio> listarOrdenesServicioFiltro(Integer idEmpresa, Integer estado, Integer conFactura,
			Date fechaIni, Date fechaFin) {
		// TODO Auto-generated method stub
		
		Integer idEstadoMin, idEstadoMax = 0;

		if (estado == 0) {
			idEstadoMin = estado;
			idEstadoMax = 99;
		} else {
			idEstadoMin = estado;
			idEstadoMax = estado;
		}

		return this.ordenServicioRepository.listarOrdenesServicioNoFacturadas(idEmpresa, idEstadoMin, idEstadoMax, fechaIni, fechaFin);
		

	}

}
