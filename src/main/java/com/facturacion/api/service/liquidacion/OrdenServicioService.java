package com.facturacion.api.service.liquidacion;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.entity.OrdenServicio;

public interface OrdenServicioService {
	
	
	Optional<OrdenServicio> obteneOrdenServicioPorNro(String nroOrdenServicio);

	OrdenServicio registrarOrdenServicio(OrdenServicio ordenServicioCreate);
	
	OrdenServicio actualizarOrdenServicio(OrdenServicio ordenServicioUpdate);
	
	List<OrdenServicio> listarOrdenesServicioPorEmpresa(Integer idEmpresa);
	
	List<GuiaRemision> obtenerGuiasAsociadasOrdenServicio(OrdenServicio ordenServicioQuery);
	
	List<OrdenServicio> listarOrdenesServicioFiltro( Integer idEmpresa,
			Integer estado,
			Integer conFactura,
			Date fechaIni,
			Date fechaFin);


}
