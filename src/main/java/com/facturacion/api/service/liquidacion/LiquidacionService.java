/**
 * 
 */
package com.facturacion.api.service.liquidacion;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.bo.LiquidacionGrillaBO;
import com.facturacion.api.model.entity.Liquidacion;

/**
 * @author CESAR
 *
 */
public interface LiquidacionService {
	
	
	//Busqueda Default
	List<Liquidacion> listTodasLiquidacionesPorEmpresa(Integer idEmpresa);
	
	//Busqueda para la Grilla
	List<LiquidacionGrillaBO> listarTodasLasLiquidacionesPorEmpresa2(Integer idEmpresa);
	
	//Busqueda por Defecto
	List<Liquidacion> listarTodasLasLiquidacionesPorEmpresaNativo(Integer idEmpresa);
	
	List<Liquidacion> listarLiquidacionesPorFiltro( Integer idEmpresa,
													Integer idOrigen,
													Integer idDestino,
													Integer estado,
													Integer conFactura,
													Date fechaIni,
													Date fechaFin);
	
	List<Liquidacion> listarLiquidacionesPorFiltroCriteria( Integer idEmpresa,
			Integer idOrigen,
			Integer idDestino,
			Integer estado,
			Date fechaIni,
			Date fechaFin,
			Integer tipoBusqueda);

	Optional<Liquidacion> obtenerLiquidacionPorId(Integer idLiquidacion, Integer idEmpresa);
	
	Optional<Liquidacion> obtenerLiquidacionPorNroDoc(String nroDocLIQ, Integer idEmpresa);

	Liquidacion registrarLiquidacion(Liquidacion liquidacionCreate);
	
	Liquidacion actualizarLiquidacion(Liquidacion liquidacionUpdate);
	
	void eliminarLiquidacionBD(Integer idLiquidacion);



}
