/**
 * 
 */
package com.facturacion.api.service.liquidacion.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.facturacion.api.dao.LiquidacionDAO;
import com.facturacion.api.model.bo.LiquidacionGrillaBO;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.enums.BusquedaLiquidacion;
import com.facturacion.api.model.enums.EstadoLiquidacion;
import com.facturacion.api.repository.GuiaRemisionRepository;
import com.facturacion.api.repository.LiquidacionRepository;
import com.facturacion.api.service.liquidacion.LiquidacionService;

/**
 * @author CESAR
 *
 */
@Service
public class LiquidacionServiceImpl implements LiquidacionService {

	/**
	 * 
	 */
	@Autowired
	private LiquidacionRepository liquidacionRepository;

	@Autowired
	private GuiaRemisionRepository guiaRemisionRepository;
	
	@Autowired
	private LiquidacionDAO liquidacionDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * listTodasLiquidacionesPorEmpresa(java.lang.Integer)
	 */
	@Override
	public List<Liquidacion> listTodasLiquidacionesPorEmpresa(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return this.liquidacionRepository.listarPorEmpresa(idEmpresa);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * listarTodasLasLiquidacionesPorEmpresa2(java.lang.Integer)
	 */
	@Override
	public List<LiquidacionGrillaBO> listarTodasLasLiquidacionesPorEmpresa2(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * listarTodasLasLiquidacionesPorEmpresaNativo(java.lang.Integer)
	 */
	@Override
	public List<Liquidacion> listarTodasLasLiquidacionesPorEmpresaNativo(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * obtenerLiquidacionPorId(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Optional<Liquidacion> obtenerLiquidacionPorId(Integer idLiquidacion, Integer idEmpresa) {
		// TODO Auto-generated method stub
		return this.liquidacionRepository.obtenerLiquidacionPorIdPorEmpresa(idLiquidacion, idEmpresa);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * obtenerLiquidacionPorNroDoc(java.lang.String)
	 */
	@Override
	public Optional<Liquidacion> obtenerLiquidacionPorNroDoc(String nroDocLIQ, Integer idEmpresa) {
		return this.liquidacionRepository.obtenerLiquidacionPorNroDOC(nroDocLIQ, idEmpresa, EstadoLiquidacion.VIGENTE.getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * registrarLiquidacion(com.facturacion.api.model.entity.Liquidacion)
	 */
	@Override
	public Liquidacion registrarLiquidacion(Liquidacion liquidacionCreate) {
		// TODO Auto-generated method stub

		Set<GuiaRemision> listaGuiasAsociadasLiq_post = new HashSet<GuiaRemision>();
		Set<GuiaRemision> listaGuiasAsociadasLiq = liquidacionCreate.getGuiaRemisionSet();

		// TimeStamp
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date fechaTimestamp = new Date(ts.getTime());
		
		// Fecha Registro y Actualizacion
		liquidacionCreate.setFecharegistrosistema(fechaTimestamp);
		liquidacionCreate.setFechaactualizasistema(fechaTimestamp);

		// Actualiza Estado y Situacion
		// liquidacionCreate.setEstadoliq(EstadoLiquidacion.VIGENTE.getValue());
		// liquidacionCreate.setSituacionliq(SituacionLiquidacion.PENDIENTE.getValue());
		for (GuiaRemision guiaRemision : listaGuiasAsociadasLiq) {

			Optional<GuiaRemision> guiaRemisionBD = guiaRemisionRepository.findById(guiaRemision.getIdguiaremision());
			GuiaRemision guiaRemisionBD_ = guiaRemisionBD.get();
			guiaRemisionBD_.setLiquidacionIdliquidacion(liquidacionCreate);
			listaGuiasAsociadasLiq_post.add(guiaRemisionBD_);
		}

		liquidacionCreate.setGuiaRemisionSet(listaGuiasAsociadasLiq_post);

		return liquidacionRepository.save(liquidacionCreate);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.facturacion.api.service.liquidacion.LiquidacionService#
	 * actualizarLiquidacion(com.facturacion.api.model.entity.Liquidacion)
	 */
	@Override
	public Liquidacion actualizarLiquidacion(Liquidacion liquidacionUpdate) {
		// TODO Auto-generated method stub
		
		
		Optional<Liquidacion> liquidacionQuery = liquidacionRepository.findById(liquidacionUpdate.getIdliquidacion());

		if (liquidacionQuery.isPresent()) {

			Set<GuiaRemision> listaGuiaRemision_ = new HashSet<GuiaRemision>();
			Set<GuiaRemision> listaGuiaRemisionUpdated = liquidacionUpdate.getGuiaRemisionSet();

			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			liquidacionUpdate.setFechaactualizasistema(fechaTimestamp);
			liquidacionUpdate.setFecharegistrosistema(liquidacionQuery.get().getFecharegistrosistema());

			for (GuiaRemision guiaRemision : listaGuiaRemisionUpdated) {
				 Optional<GuiaRemision> guiaRemisionBD = guiaRemisionRepository.findById(guiaRemision.getIdguiaremision());
				 GuiaRemision guiaRemisionBD_ = guiaRemisionBD.get();
				 guiaRemisionBD_.setLiquidacionIdliquidacion(liquidacionUpdate);
				//Set<GuiaRemisionProducto> listaGuiaRemisionProducto_ = new HashSet<GuiaRemisionProducto>();
				//Set<GuiaRemisionProducto> listaGuiaRemisionProductoUpdated = guiaRemisionBD.get().getGuiaRemisionProductoSet();
			
				//guiaRemision.setFechaactualizasistema(guiaRemisionBD.get().getFechaactualizasistema());
				//guiaRemision.setFecharegistrosistema(guiaRemisionBD.get().getFecharegistrosistema());
				
/*				for (GuiaRemisionProducto guiaRemisionProducto : listaGuiaRemisionProductoUpdated) {
					guiaRemisionProducto.setGuiaremisionIdguiaremision(guiaRemision);
					listaGuiaRemisionProducto_.add(guiaRemisionProducto);
				}*/
				// guiaRemision.setGuiaRemisionProductoSet(listaGuiaRemisionProducto_);
				//guiaRemision.setLiquidacionIdliquidacion(liquidacionUpdate);
				listaGuiaRemision_.add(guiaRemisionBD_);
			}
			liquidacionUpdate.setGuiaRemisionSet(listaGuiaRemision_);
			
			return liquidacionRepository.save(liquidacionUpdate);
		}
		return new Liquidacion();
		
	}

	@Override
	public List<Liquidacion> listarLiquidacionesPorFiltro(Integer idEmpresa, Integer idOrigen, Integer idDestino,
			Integer estado, Integer conFactura, Date fechaIni, Date fechaFin) {
		// TODO Auto-generated method stub
		Integer idOrigenMin, idOrigenMax = 0;
		Integer idDestinoMin, idDestinoMax = 0;
		Integer idEstadoMin, idEstadoMax = 0;

		if (idOrigen == 0) {
			idOrigenMin = idOrigen;
			idOrigenMax = 999;
		} else {
			idOrigenMin = idOrigen;
			idOrigenMax = idOrigen;
		}

		if (idDestino == 0) {
			idDestinoMin = idDestino;
			idDestinoMax = 999;
		} else {
			idDestinoMin = idDestino;
			idDestinoMax = idDestino;
		}

		if (estado == 0) {
			idEstadoMin = estado;
			idEstadoMax = 99;
		} else {
			idEstadoMin = estado;
			idEstadoMax = estado;
		}

		// No facturados
		if (conFactura == 0) {
			return this.liquidacionRepository.listarLiquidacionesNoFacturadasPorFiltro(idEmpresa, idOrigenMin,
					idOrigenMax, idDestinoMin, idDestinoMax, idEstadoMin, idEstadoMax, fechaIni, fechaFin);
		} else {
			return this.liquidacionRepository.listarLiquidacionesFacturadasPorFiltro(idEmpresa, idOrigenMin,
					idOrigenMax, idDestinoMin, idDestinoMax, idEstadoMin, idEstadoMax, fechaIni, fechaFin);

		}

	}

	@Override
	public List<Liquidacion> listarLiquidacionesPorFiltroCriteria(Integer idEmpresa, Integer idOrigen,
			Integer idDestino, Integer estado, Date fechaIni, Date fechaFin, Integer tipoBusqueda) {
		return liquidacionDAO.findAll(new Specification<Liquidacion>() {
			private static final long serialVersionUID = 1L;

			@Override
            public Predicate toPredicate(Root<Liquidacion> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                
                // WHERE
                if(idEmpresa > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("empresaIdempresa"), idEmpresa)));
                }
                
                if(estado > 0 && estado < 99) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estadoliq"), estado)));
                }

                if(idOrigen > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clientefactoriaOrigen"), idOrigen)));
                }
                if(idDestino> 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clientefactoriaDestino"), idDestino)));
                }               
                if (tipoBusqueda == BusquedaLiquidacion.LIQ_NO_FACTURADAS.getValue()) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.isNull(root.get("documentoIddocumento"))));
                } else {
                	if (tipoBusqueda == BusquedaLiquidacion.LIQ_FACTURADAS.getValue()) {
                        predicates.add(criteriaBuilder.and(criteriaBuilder.isNotNull(root.get("documentoIddocumento"))));
                	}
                }
                
                if(fechaIni!= null && fechaFin!= null){
                    predicates.add(criteriaBuilder.and(
                    	//	criteriaBuilder.and (
                            		criteriaBuilder.greaterThanOrEqualTo(root.get("fechaemisionliq"), fechaIni),
                            		criteriaBuilder.lessThanOrEqualTo(root.get("fechaemisionliq"), fechaFin)
                    			)
                    	//	)
                    );

                }
                
                //ORDER BY
                query.orderBy(criteriaBuilder.desc(root.get("fechaemisionliq")),
                		  criteriaBuilder.desc(root.get("nrodocumentoliq"))); 
                
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });	
	}

	@Override
	public void eliminarLiquidacionBD(Integer idLiquidacion) {
		this.liquidacionRepository.deleteById(idLiquidacion);
	}

}
