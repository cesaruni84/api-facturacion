/**
 * 
 */
package com.facturacion.api.service.liquidacion.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.MotivoTraslado;
import com.facturacion.api.repository.MotivoTrasladoRepository;
import com.facturacion.api.service.liquidacion.MotivoTrasladoService;


/**
 * @author CESAR
 *
 */
@Service
public class MotivoTrasladoServiceImpl implements MotivoTrasladoService {
	
	@Autowired
	private MotivoTrasladoRepository motivoTrasladoRepository;


	/* (non-Javadoc)
	 * @see com.facturacion.api.service.liquidacion.MotivoTrasladoService#listarTodosLosMotivosDeTrasladoBienes()
	 */
	@Override
	public List<MotivoTraslado> listarTodosLosMotivosDeTrasladoBienes() {
		// TODO Auto-generated method stub
		return motivoTrasladoRepository.findAll();
	}

}
