/**
 * 
 */
package com.facturacion.api.service.liquidacion;

import java.util.List;
import com.facturacion.api.model.entity.MotivoTraslado;

/**
 * @author CESAR
 *
 */
public interface MotivoTrasladoService {
	
	List<MotivoTraslado> listarTodosLosMotivosDeTrasladoBienes();


}
