package com.facturacion.api.service.liquidacion.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.Impuesto;
import com.facturacion.api.repository.ImpuestoRepository;
import com.facturacion.api.service.liquidacion.ImpuestoService;

@Service
public class ImpuestoServiceImpl implements ImpuestoService{
	
	@Autowired
	private ImpuestoRepository impuestoRepository;
	
	
	public List<Impuesto> listarTodosLosImpuestos() {
		// TODO Auto-generated method stub
		return this.impuestoRepository.findAll();
	}

	
	public Optional<Impuesto> obtenerImpuestoPorId(Integer idImpuesto) {
		// TODO Auto-generated method stub
		return this.impuestoRepository.findById(idImpuesto);
	}

}
