/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.bo.Item;
import com.facturacion.api.model.bo.TipoItem;
import com.facturacion.api.model.entity.Producto;
import com.facturacion.api.model.entity.UnidadMedida;
import com.facturacion.api.repository.ProductoRepository;
import com.facturacion.api.repository.UnidadMedidaRepository;
import com.facturacion.api.service.remision.ProductoService;

/**
 * @author CESAR
 *
 */
@Service
public class ProductoServiceImpl implements ProductoService {
	
	
	@Autowired
	private ProductoRepository productoRepository ;
	
	@Autowired
	private UnidadMedidaRepository unidadMedidaRepository;
	
	
	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.ProductoService#listarTodosLosProductos()
	 */
	@Override
	public List<Producto> listarTodosLosProductos() {
		return productoRepository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.ProductoService#listarTodosLosProductosPorCategoria(java.lang.Integer)
	 */
	@Override
	public List<Producto> listarTodosLosProductosPorCategoria(Integer idCategoriaProd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> listarTodosLosProductosServicios(Integer idEmpresa) {
		
		List<Item> listaItems = new ArrayList<>();
		List<Producto> listaProductos = this.productoRepository.findAll();
		List<UnidadMedida> unidadMedidaLista = this.unidadMedidaRepository.findAll();
		
		for (Producto producto : listaProductos) {
			Item item = new Item();
			item.setId(producto.getIdproducto());
			if (producto.getProductocategoriaIdcategoria()!= null) {
				item.setTipo(new TipoItem(producto.getProductocategoriaIdcategoria().getIdcategoria()));
			} else {
				item.setTipo(new TipoItem(1));
			}
			item.setCodigo(producto.getCodigo());
			item.setDescripcion(producto.getNombre());
			item.setTarifa(producto.getPrecio());
			item.setUnidadMedida(unidadMedidaLista.get(0));
			item.setCantidad(0.00);
			item.setEsOrdenServicio(false);
			listaItems.add(item);
		}
		return listaItems;
	}

	@Override
	public Producto registrarProducto(Producto productoCreate) {
		return this.productoRepository.save(productoCreate);
	}

	@Override
	public Producto actualizarProducto(Producto productoUpdate) {
		Optional<Producto> producto =  this.productoRepository.findById(productoUpdate.getIdproducto());
		productoUpdate.setFechaingreso(producto.get().getFechaingreso());
		return this.productoRepository.save(productoUpdate);
	}

	@Override
	public Optional<Producto> obtenerProductoPorCodigo(String codigo) {
		return productoRepository.findByCodigo(codigo);
	}

}
