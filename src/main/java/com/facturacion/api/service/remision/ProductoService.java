/**
 * 
 */
package com.facturacion.api.service.remision;

import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.bo.Item;
import com.facturacion.api.model.entity.Producto;

/**
 * @author CESAR
 *
 */
public interface ProductoService {
	
	List<Producto> listarTodosLosProductos();
	List<Producto> listarTodosLosProductosPorCategoria(Integer idCategoriaProd);
	List<Item> listarTodosLosProductosServicios(Integer idEmpresa);
	
	// Graba Producto 
	Producto registrarProducto(Producto productoCreate);
	
	// Actualiza Producto
	Producto actualizarProducto(Producto productoUpdate);
	
	// Búsqueda por Código de Producto
	Optional<Producto> obtenerProductoPorCodigo(String codigo);



}
