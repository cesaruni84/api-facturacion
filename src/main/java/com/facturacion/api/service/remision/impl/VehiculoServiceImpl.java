/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Vehiculo;
import com.facturacion.api.repository.VehiculoRepository;
import com.facturacion.api.service.remision.VehiculoService;

/**
 * @author CESAR
 *
 */

@Service
public class VehiculoServiceImpl implements VehiculoService {
	
	
	@Autowired
	private VehiculoRepository vehiculoRepository ;

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.VehiculoService#listarTodasLosVehiculos()
	 */
	@Override
	public List<Vehiculo> listarTodasLosVehiculos() {
		return vehiculoRepository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.VehiculoService#registrarFactoria(com.facturacion.api.model.entity.Vehiculo)
	 */
	@Override
	public Vehiculo registrarVehiculo(Vehiculo vehiculoCreate) {
		return vehiculoRepository.save(vehiculoCreate);
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.VehiculoService#actualizarFactoria(com.facturacion.api.model.entity.Vehiculo)
	 */
	@Override
	public Vehiculo actualizarVehiculo(Vehiculo vehiculoUpdate) {
		// TODO Auto-generated method stub
		Optional<Vehiculo> vehiculoQuery = vehiculoRepository.findById(vehiculoUpdate.getIdvehiculo());
		vehiculoUpdate.setFecharegistro(vehiculoQuery.get().getFecharegistro());
		return vehiculoRepository.save(vehiculoUpdate);
	}

}
