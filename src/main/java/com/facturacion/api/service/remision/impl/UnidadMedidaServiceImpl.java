/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.UnidadMedida;
import com.facturacion.api.repository.UnidadMedidaRepository;
import com.facturacion.api.service.remision.UnidadMedidaService;

/**
 * @author CESAR
 *
 */
@Service
public class UnidadMedidaServiceImpl implements UnidadMedidaService {
	
	
	
	@Autowired
	private UnidadMedidaRepository unidadMedidaRepository ;

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.UnidadMedidaService#listarTodasLasUnidadesMedidas()
	 */
	@Override
	public List<UnidadMedida> listarTodasLasUnidadesMedidas() {
		// TODO Auto-generated method stub
		return unidadMedidaRepository.findAll();
	}

}
