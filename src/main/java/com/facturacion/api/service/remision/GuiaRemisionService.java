/**
 * 
 */
package com.facturacion.api.service.remision;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.bo.GuiaRemisionDTO;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.filters.FiltrosGuiaRemision;

/**
 * @author CESAR
 *
 */
public interface GuiaRemisionService {
	
	//Busqueda para la Grilla
	List<GuiaRemisionDTO> listarTodasLasGuiasPorEmpresa(Integer idEmpresa);
	
	//Busqueda por Defecto
	List<GuiaRemision> listarTodasLasGuiasPorEmpresaNativo(Integer idEmpresa);
	
	//Para Grilla de Seleccion en Pantalla de Liquidacion
	List<GuiaRemision> listarGuiasParaLiquidacion(Liquidacion liqHeaderQuery);
	
	List<GuiaRemision> listarGuiasPorFacturar(Integer idEmpresa, Integer idOrigen, Integer idDestino, Date fechaIni, Date fechaFin);
	
	List<GuiaRemisionDTO> listarTodasLasGuiasPorFiltro(FiltrosGuiaRemision filtros);
	
	List<GuiaRemisionDTO> obtenerDatosParaGrilla(List<GuiaRemision> lista, Integer tipoBusqueda);
	
	List<GuiaRemision> actualizarTarifasEnListado(List<GuiaRemision> lista);
	
	//Para Pantalla de Filtros de Busqueda de Guias
	List<GuiaRemision> listarGuiasPorFiltrosConCriteria(String nroSerie,String nroSecuencia, Integer idEmpresa, Integer idEstado, Integer idChofer, Integer idOrigen, Integer idDestino, Date fechaIni, Date fechaFin, Integer tipoBusqueda);

	//Busqueda por nro de guia cliente
	Optional<GuiaRemision> obtenerGuiaRemisionPorNroGuiaCliente(String nroSerieCli, String nroSecuenciaCli, Integer idEmpresa);
	
	// Lista por coincidencia de nro serie del Cliente
	List<GuiaRemision> listarGuiasPorNroSerieCli(Integer idEmpresa, String nroSerieCli, Date fechaIni, Date fechaFin);
	
	// Lista por coincidencia de nro serie o nro secuencia de la Guia Cliente
	List<GuiaRemision> listarGuiasPorCoincidenciaNroSerieSecuenciaGuiaCliente(Integer idEmpresa, String nroSerieCli, String nroSecuenciaCli, Integer idEstado, Integer idChofer, Integer idOrigen, Integer idDestino, Date fechaIni, Date fechaFin, Integer tipoBusqueda);
	
	// Lista por coincidencia de nro serie de la Guia
	List<GuiaRemision> listarGuiasPorNroSerie(Integer idEmpresa, String nroSerie, Date fechaIni, Date fechaFin);
	
	// Lista por coincidencia de nro serie o nro secuencia por Guia Empresa
	List<GuiaRemision> listarGuiasPorCoincidenciaNroSerieSecuencia(Integer idEmpresa, String nroSerie, String nroSecuencia, Date fechaIni, Date fechaFin);

	Optional<GuiaRemision> obtenerGuiaRemisionPorId(Integer idGuiaRemision, Integer idEmpresa);
	
	Optional<GuiaRemision> obtenerGuiaRemisionPorNroGuiaEmpresa(String nroGuia, String nroSecuencia, Integer idEmpresa);

	GuiaRemision registrarGuiaRemision(GuiaRemision obj);
	
	GuiaRemision actualizarGuiaRemision(GuiaRemision obj);
	
	GuiaRemision actualizarTarifaGuiaRemision(GuiaRemision obj);

	GuiaRemision extornarGuiaRemision(GuiaRemision obj);
	
	GuiaRemision actualizarGuiaRemisionLiquidacion(Integer idGuiaRemision, Integer idLiquidacion);
	
	void eliminarGuiaBD(Integer idGuiaRemision);
	
	void liberarGuiasDeLiquidacion(Integer idLiquidacion);


}
