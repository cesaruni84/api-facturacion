/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.Transportista;
import com.facturacion.api.repository.TransportistaRepository;
import com.facturacion.api.service.remision.TransportistaService;

/**
 * @author CESAR
 *
 */
@Service
public class TransportistaServiceImpl implements TransportistaService {
	
	
	@Autowired
	private TransportistaRepository transportistaRepository ;

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.TransportistaService#listarChoferesPorEmpresa(java.lang.Integer)
	 */
	@Override
	public List<Transportista> listarChoferesPorEmpresa(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return this.transportistaRepository.listarChoferesPorEmpresa(idEmpresa);
	}

	@Override
	public Transportista registrarTransportista(Transportista choferCreate) {
		// TODO Auto-generated method stub
		return transportistaRepository.save(choferCreate);
	}

	@Override
	public Transportista actualizaTransportista(Transportista choferUpdate) {
		// TODO Auto-generated method stub
		Optional<Transportista> choferQuery = transportistaRepository.findById(choferUpdate.getIdtransportista());
		choferUpdate.setFecharegistro(choferQuery.get().getFecharegistro());
		return transportistaRepository.save(choferUpdate);
	}

}
