/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.facturacion.api.dao.GuiaRemisionDAO;
import com.facturacion.api.model.bo.GuiaRemisionDTO;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.GuiaRemisionProducto;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.entity.TarifaRuta;
import com.facturacion.api.model.enums.BusquedaGuias;
import com.facturacion.api.model.enums.EstadoGuia;
import com.facturacion.api.model.filters.FiltrosGuiaRemision;
import com.facturacion.api.repository.GuiaRemisionRepository;
import com.facturacion.api.repository.TarifaRutaRepository;
import com.facturacion.api.service.liquidacion.impl.SearchCriteria;
import com.facturacion.api.service.remision.GuiaRemisionService;

/**
 * @author CESAR
 *
 */
@Service
public class GuiaRemisionServiceImpl implements GuiaRemisionService {

	@Autowired
	private GuiaRemisionRepository guiaRemisionRepository;

	@Autowired
	private GuiaRemisionDAO guiaRemisionDAO;
	
	@Autowired
	private TarifaRutaRepository tarifaRutaRepository;

	
	@Override
	public List<GuiaRemisionDTO> listarTodasLasGuiasPorEmpresa(Integer idEmpresa) {
		// TODO Auto-generated method stub

		List<GuiaRemisionDTO> grillaGuias = new ArrayList<>();
		List<GuiaRemision> lista = this.guiaRemisionRepository.listarPorEmpresa(idEmpresa);
		grillaGuias = obtenerDatosParaGrilla(lista,1);
		return grillaGuias;
	}
	
	@Override
	public List<GuiaRemisionDTO> obtenerDatosParaGrilla(List<GuiaRemision> lista, Integer tipoBusqueda) {
		List<GuiaRemisionDTO> grillaGuias_ = new ArrayList<>();
		String DEFAULT_VALUE="-" ; 
		int item =1;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		for (GuiaRemision guiaRemision : lista) {
			GuiaRemisionDTO remisionGrillaBO = new GuiaRemisionDTO();
			//remisionGrillaBO.setIdGuiaRemision(guiaRemision.getIdguiaremision());
			remisionGrillaBO.setIdGuiaRemision(item);
			remisionGrillaBO.setNroGuiaRemision(guiaRemision.getNroserieguia());
			remisionGrillaBO.setNroSecuencia(guiaRemision.getNrosecuenciaguia());
			remisionGrillaBO.setFechaEmision(formatter.format(guiaRemision.getFecharemision()));
			remisionGrillaBO.setUsuarioRegistra(guiaRemision.getUsuarioregistro());
						
			if (guiaRemision.getLiquidacionIdliquidacion()!=null) {
				remisionGrillaBO.setNroLiquidacionRemision(guiaRemision.getLiquidacionIdliquidacion().getNrodocumentoliq());
				remisionGrillaBO.setNroOrdenServicio(DEFAULT_VALUE);
			}else {
				remisionGrillaBO.setNroLiquidacionRemision(DEFAULT_VALUE);
				remisionGrillaBO.setNroOrdenServicio(DEFAULT_VALUE);
			}
			
			remisionGrillaBO.setCodigoFactura(DEFAULT_VALUE);
			if (guiaRemision.getLiquidacionIdliquidacion()!=null) {
				if (guiaRemision.getLiquidacionIdliquidacion().getDocumentoIddocumento()!=null) {
					String serieDoc =guiaRemision.getLiquidacionIdliquidacion().getDocumentoIddocumento().getSerie();
					String secueDoc =guiaRemision.getLiquidacionIdliquidacion().getDocumentoIddocumento().getSecuencia();
					Integer estadoFactura =guiaRemision.getLiquidacionIdliquidacion().getDocumentoIddocumento().getEstado();
					remisionGrillaBO.setCodigoFactura(serieDoc + "-" + secueDoc);
					remisionGrillaBO.setEstadoFactura(estadoFactura);
					remisionGrillaBO.setNemonicoEstadoFactura(retornaNemonicoEstadoFactura(estadoFactura));
				}		
			} else {
				if (guiaRemision.getDocumentoIddocumento()!= null) {
					String serieDoc =guiaRemision.getDocumentoIddocumento().getSerie();
					String secueDoc =guiaRemision.getDocumentoIddocumento().getSecuencia();
					Integer estadoFactura =guiaRemision.getDocumentoIddocumento().getEstado();
					remisionGrillaBO.setCodigoFactura(serieDoc + "-" + secueDoc);
					remisionGrillaBO.setEstadoFactura(estadoFactura);
					remisionGrillaBO.setNemonicoEstadoFactura(retornaNemonicoEstadoFactura(estadoFactura));

				}
			}
			
			//remisionGrillaBO.setRazonSocialRemitente(guiaRemision.getClientefactoriaIdremitente().getClienteIdcliente().getRazonsocial());
			//remisionGrillaBO.setRazonSocialDestinatario(guiaRemision.getClientefactoriaIddestinatario().getClienteIdcliente().getRazonsocial());
			if (guiaRemision.getClientefactoriaIdremitente() != null) {
				remisionGrillaBO.setRazonSocialRemitente(guiaRemision.getClientefactoriaIdremitente().getReferenciaLargaFactoria2());
			}
			
			if (guiaRemision.getClientefactoriaIddestinatario() != null) {
				remisionGrillaBO.setRazonSocialDestinatario(guiaRemision.getClientefactoriaIddestinatario().getReferenciaLargaFactoria2());
			}
			
			
			EstadoGuia ESTADO_GUIA = EstadoGuia.values()[guiaRemision.getEstadoguia()];
			remisionGrillaBO.setEstadoGuiaRemision(ESTADO_GUIA.toString());
			Set<GuiaRemisionProducto> guiasDetalle = guiaRemision.getGuiaRemisionProductoSet();
			int i = 0;
			String nombreProducto = "";
			String unidadMedida = "";
			if (guiasDetalle != null ) {
				for (GuiaRemisionProducto guiaRemisionProducto : guiasDetalle) {
					if (i == 0) {
						nombreProducto = guiaRemisionProducto.getProductoIdproducto().getNombre();
						unidadMedida = guiaRemisionProducto.getUnidadmedidaIdunidadmedida().getValor();
					}
				}
			}

			remisionGrillaBO.setNombreProductoAsociado(nombreProducto);
			remisionGrillaBO.setCantidadProductoAsociado(guiaRemision.getTotalcantidad().toString() + " " + unidadMedida);
			
			if (guiaRemision.getTransportistaIdtransportista() != null) {
				remisionGrillaBO.setNombreChofer(guiaRemision.getTransportistaIdtransportista().getNombres()+ " "  +
					 	guiaRemision.getTransportistaIdtransportista().getApellidos());	
			} else {
				remisionGrillaBO.setNombreChofer("");
			}

			remisionGrillaBO.setNroGuiaRemisionCliente(
					guiaRemision.getNroserieguiacliente() + "-" + guiaRemision.getNrosecuenguiacliente());
			remisionGrillaBO.setFechaRecepcionGuia(formatter.format(guiaRemision.getFecharecepcion()));
			
			if (tipoBusqueda == BusquedaGuias.GUIAS_FACTURADAS.getValue()) {
				 if (guiaRemision.getLiquidacionIdliquidacion()!=null) {
					 if (guiaRemision.getLiquidacionIdliquidacion().getDocumentoIddocumento()!=null) {
						 grillaGuias_.add(remisionGrillaBO);
					 }
				 } else {
					 if (guiaRemision.getDocumentoIddocumento()!= null) {
						 grillaGuias_.add(remisionGrillaBO);
					 }
				 }
			} else {
				grillaGuias_.add(remisionGrillaBO);
			}
			item++;	
		}

		return grillaGuias_;
		
	}

	@Override
	public List<GuiaRemision> listarTodasLasGuiasPorEmpresaNativo(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return this.guiaRemisionRepository.listarPorEmpresa(idEmpresa);
	}


	@Override
	public Optional<GuiaRemision> obtenerGuiaRemisionPorId(Integer idGuiaRemision, Integer idEmpresa) {
		// TODO Auto-generated method stub
		Optional<GuiaRemision> optGuiaRemision = guiaRemisionRepository.obtenerGuiaRemision(idGuiaRemision, idEmpresa);
		if (optGuiaRemision.isPresent()) {
			//Obtiene tarifa asociada a la ruta
			Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.
											obtenerTarifaTurnoAsociada(optGuiaRemision.get().getClientefactoriaIdremitente().getIdclientefactoria(), 
																		optGuiaRemision.get().getClientefactoriaIddestinatario().getIdclientefactoria(), 
																	   new Integer(1));
			if(tarifaRuta.isPresent()) {
				BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
				optGuiaRemision.get().setImportetarifaasociado(tarifa);
			}else {
				optGuiaRemision.get().setImportetarifaasociado(new BigDecimal(0.00));
			}
			
		}
		return optGuiaRemision;
	}

	@Override
	public List<GuiaRemisionDTO> listarTodasLasGuiasPorFiltro(FiltrosGuiaRemision filtros) {

		List<SearchCriteria> parametrosFiltros = new ArrayList<SearchCriteria>();

		if (filtros.getNroSerieRemisionFiltro() != null) {
			parametrosFiltros.add(new SearchCriteria("nroserieguia", ":", filtros.getNroSerieRemisionFiltro()));
		}

		if (filtros.getNroGuiaRemisionFiltro() != null) {
			parametrosFiltros.add(new SearchCriteria("nrosecuenciaguia", ":", filtros.getNroGuiaRemisionFiltro()));
		}

		if (filtros.getFechaInicioEmisionFiltro() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
			String fechaIni = filtros.getFechaInicioEmisionFiltro();
			Date dateFechaIni;
			try {
				dateFechaIni = formatter.parse(fechaIni);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace()
				dateFechaIni = new Date();
			}

			parametrosFiltros.add(new SearchCriteria("fecharemision", ">=", dateFechaIni));
		}

		if (filtros.getFechaFinEmisionFiltro() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
			String fechaFin = filtros.getFechaFinEmisionFiltro();
			Date dateFechaFin;
			try {
				dateFechaFin = formatter.parse(fechaFin);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace()
				dateFechaFin = new Date();
			}

			parametrosFiltros.add(new SearchCriteria("fecharemision", "<=", dateFechaFin));
		}

		if (filtros.getIdEstadoFiltro() != null) {
			parametrosFiltros.add(new SearchCriteria("estadoguia", ":", filtros.getIdEstadoFiltro()));
		}

		if (filtros.getIdChoferFiltro() != null) {
			parametrosFiltros.add(new SearchCriteria("transportistaIdtransportista", ":", filtros.getIdChoferFiltro()));
		}

		if (filtros.getIdDestinoFactoriaCliFiltro() != null) {
			parametrosFiltros.add(new SearchCriteria("clientefactoriaIddestinatario", ":",filtros.getIdDestinoFactoriaCliFiltro()));
		}
		
		
		if (filtros.getIdEmpresaGuia() != null) {
			parametrosFiltros.add(new SearchCriteria("empresaIdempresa", ":",filtros.getIdEmpresaGuia()));
		}
		
		// List<GuiaRemision> remisions  =	guiaRemisionDAO.buscarPorFiltros(parametrosFiltros);
		// grillaSalida = obtenerDatosParaGrilla(remisions);
		
		return null;
	}

	@Override
	@Transactional
	public GuiaRemision registrarGuiaRemision(GuiaRemision guiaRemisionForm) {
		// TODO Auto-generated method stub

		Set<GuiaRemisionProducto> listaGuiaRemisionProducto_ = new HashSet<GuiaRemisionProducto>();
		Set<GuiaRemisionProducto> listaGuiaRemisionProducto = guiaRemisionForm.getGuiaRemisionProductoSet();

		// TimeStamp
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date fechaTimestamp = new Date(ts.getTime());
		
		//Fecha Registro y Actualizacion
		guiaRemisionForm.setFecharegistrosistema(fechaTimestamp);
		guiaRemisionForm.setFechaactualizasistema(fechaTimestamp);
		
		//Obtiene tarifa asociada a la ruta
		Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.
										obtenerTarifaTurnoAsociada(guiaRemisionForm.getClientefactoriaIdremitente().getIdclientefactoria(), 
																   guiaRemisionForm.getClientefactoriaIddestinatario().getIdclientefactoria(), 
																   new Integer(1));

		if(tarifaRuta.isPresent()) {
			BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
			guiaRemisionForm.setImportetarifaasociado(tarifa);
		}else {
			guiaRemisionForm.setImportetarifaasociado(new BigDecimal(0));
		}
		
		for (GuiaRemisionProducto guiaRemisionProducto : listaGuiaRemisionProducto) {
			guiaRemisionProducto.setGuiaremisionIdguiaremision(guiaRemisionForm);
			listaGuiaRemisionProducto_.add(guiaRemisionProducto);
		}
	
		guiaRemisionForm.setGuiaRemisionProductoSet(listaGuiaRemisionProducto_);

		return guiaRemisionRepository.save(guiaRemisionForm);
	}

	@Override
	public Optional<GuiaRemision> obtenerGuiaRemisionPorNroGuiaEmpresa(String nroGuia, String nroSecuencia, Integer idEmpresa) {
		// TODO Auto-generated method stub
		Optional<GuiaRemision> optGuiaRemision = guiaRemisionRepository.obtenerGuiaRemisionPorNroGuia(nroGuia, nroSecuencia,idEmpresa);
		

		if (optGuiaRemision.isPresent() && optGuiaRemision.get().getClientefactoriaIdremitente()!= null 
				&& optGuiaRemision.get().getClientefactoriaIddestinatario() != null) {
			//Obtiene tarifa asociada a la ruta
			Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.
											obtenerTarifaTurnoAsociada(optGuiaRemision.get().getClientefactoriaIdremitente().getIdclientefactoria(), 
																		optGuiaRemision.get().getClientefactoriaIddestinatario().getIdclientefactoria(), 
																	   new Integer(1));
			if(tarifaRuta.isPresent()) {
				BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
				optGuiaRemision.get().setImportetarifaasociado(tarifa);
			}else {
				optGuiaRemision.get().setImportetarifaasociado(new BigDecimal(0.00));
			}
			
		}

		return optGuiaRemision;
	}

	@Override
	@Transactional
	public GuiaRemision actualizarGuiaRemision(GuiaRemision guiaRemisionUpdate) {

		Optional<GuiaRemision> guiaRemisionQuery = guiaRemisionRepository.findById(guiaRemisionUpdate.getIdguiaremision());

		if (guiaRemisionQuery.isPresent()) {

			Set<GuiaRemisionProducto> listaGuiaRemisionProducto_ = new HashSet<GuiaRemisionProducto>();
			Set<GuiaRemisionProducto> listaGuiaRemisionProductoUpdated = guiaRemisionUpdate
					.getGuiaRemisionProductoSet();

			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			guiaRemisionUpdate.setFechaactualizasistema(fechaTimestamp);
			guiaRemisionUpdate.setFecharegistrosistema(guiaRemisionQuery.get().getFecharegistrosistema());

			for (GuiaRemisionProducto guiaRemisionProducto : listaGuiaRemisionProductoUpdated) {
				guiaRemisionProducto.setGuiaremisionIdguiaremision(guiaRemisionUpdate);
				listaGuiaRemisionProducto_.add(guiaRemisionProducto);
			}
			guiaRemisionUpdate.setGuiaRemisionProductoSet(listaGuiaRemisionProducto_);

			return guiaRemisionRepository.save(guiaRemisionUpdate);
		}
		return new GuiaRemision();

	}
	
	@Override
	@Transactional
	public GuiaRemision actualizarGuiaRemisionLiquidacion(Integer idGuiaRemision, Integer idLiquidacion) {
		// TODO Auto-generated method stub
		this.guiaRemisionRepository.actualizarGuiaLiquidacion(idGuiaRemision, idLiquidacion);
		return new GuiaRemision();
	}

	@Override
	public List<GuiaRemision> listarGuiasParaLiquidacion(Liquidacion liqHeaderQuery) {
		// TODO Auto-generated method stub
		List<GuiaRemision> grillaGuiasLiquidacion_ = new ArrayList<>();

		
		Integer idOrigen = liqHeaderQuery.getClientefactoriaOrigen().getIdclientefactoria();
		Integer idDestino = liqHeaderQuery.getClientefactoriaDestino().getIdclientefactoria();
		Integer ESTADO_GUIA = EstadoGuia.REGISTRADO.getValue();
		Date fechaIniTraslado_ = liqHeaderQuery.getFechainitraslado();
		Date fechaFinTraslado_ = liqHeaderQuery.getFechafintraslado();
		Integer idEmpresa = liqHeaderQuery.getEmpresaIdempresa().getIdempresa();
		
		List<GuiaRemision> guiaRemisionQuery = guiaRemisionRepository.listarGuiasNoLiquidadasxEmpresa(idOrigen, idDestino, ESTADO_GUIA,
				fechaIniTraslado_, fechaFinTraslado_, idEmpresa);
		
		
		for (GuiaRemision guiaRemision_ : guiaRemisionQuery) {
			
			//Obtiene tarifa asociada a la ruta
			if (guiaRemision_.getClientefactoriaIdremitente()!= null  && guiaRemision_.getClientefactoriaIddestinatario() != null) {
				Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.
						obtenerTarifaTurnoAsociada(guiaRemision_.getClientefactoriaIdremitente().getIdclientefactoria(), 
												guiaRemision_.getClientefactoriaIddestinatario().getIdclientefactoria(), 
												   new Integer(1));
				if(tarifaRuta.isPresent()) {
					BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
					guiaRemision_.setImportetarifaasociado(tarifa);
				}else {
					guiaRemision_.setImportetarifaasociado(new BigDecimal(0.00));
				}
			} else {
				guiaRemision_.setImportetarifaasociado(new BigDecimal(0.00));
			}
			grillaGuiasLiquidacion_.add(guiaRemision_);
		}
		
		return grillaGuiasLiquidacion_;
	}

	@Override
	public List<GuiaRemision> listarGuiasPorFacturar(Integer idEmpresa, Integer idOrigen, Integer idDestino, Date fechaIni, Date fechaFin) {
		// TODO Auto-generated method stub
		Integer idOrigenMin, idOrigenMax = 0;
		Integer idDestinoMin, idDestinoMax = 0;
		// Integer idEstadoMin, idEstadoMax = 0;
		List<GuiaRemision> guiasPorLiquidar = new ArrayList<>();


		if (idOrigen == 0) {
			idOrigenMin = idOrigen;
			idOrigenMax = 999;
		} else {
			idOrigenMin = idOrigen;
			idOrigenMax = idOrigen;
		}

		if (idDestino == 0) {
			idDestinoMin = idDestino;
			idDestinoMax = 999;
		} else {
			idDestinoMin = idDestino;
			idDestinoMax = idDestino;
		}

		List<GuiaRemision> guias = this.guiaRemisionRepository.listarGuiasNoFacturadasxEmpresa(idEmpresa, idOrigenMin, idOrigenMax, 
				idDestinoMin, idDestinoMax, fechaIni, fechaFin);
		
		for (GuiaRemision guiaRemision_ : guias) {
			
			//Obtiene tarifa asociada a la ruta
			if (guiaRemision_.getClientefactoriaIdremitente() != null && guiaRemision_.getClientefactoriaIddestinatario()!=null) {
				Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.obtenerTarifaTurnoAsociada(
						guiaRemision_.getClientefactoriaIdremitente().getIdclientefactoria(),
						guiaRemision_.getClientefactoriaIddestinatario().getIdclientefactoria(), new Integer(1));

				if (tarifaRuta.isPresent()) {
					BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
					guiaRemision_.setImportetarifaasociado(tarifa);
				} else {
					guiaRemision_.setImportetarifaasociado(new BigDecimal(0.00));
				}
			}else {
				guiaRemision_.setImportetarifaasociado(new BigDecimal(0.00));
			}

			guiasPorLiquidar.add(guiaRemision_);
		}
		

		return guiasPorLiquidar;
		
		
		
	}

	@Override
	public List<GuiaRemision> listarGuiasPorFiltrosConCriteria(String nroSerie,
																String nroSecuencia,
																Integer idEmpresa, 
																Integer idEstado, 
																Integer idChofer, 
																Integer idOrigen,
																Integer idDestino, 
																Date fechaIni, 
																Date fechaFin,
																Integer tipoBusqueda) {
        return guiaRemisionDAO.findAll(new Specification<GuiaRemision>() {
			private static final long serialVersionUID = 1L;

			@Override
            public Predicate toPredicate(Root<GuiaRemision> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                
                // Join<GuiaRemision, Liquidacion> owners = root.join("liquidacionIdliquidacion");
                
                
                // Predicate predicateJOIN =  criteriaBuilder.equal(root.get("liquidacionIdliquidacion"), owners.get("guiaRemisionSet"));
                // predicates.add(criteriaBuilder.and();
                // owners.on(predicateJOIN);

                // WHERE
                if(nroSerie!=null && nroSerie!= "" ) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("nroserieguia"), "%"+nroSerie+"%")));
                }
                
                if(nroSecuencia!=null && nroSecuencia!= "" ) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("nrosecuenciaguia"), "%"+nroSecuencia+"%")));
                }
                
                if(idEmpresa > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("empresaIdempresa"), idEmpresa)));
                }       
                if(idEstado >= 0 && idEstado < 99) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estadoguia"), idEstado)));
                }
                if(idChofer > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("transportistaIdtransportista"), idChofer)));
                }
                if(idOrigen > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clientefactoriaIdremitente"), idOrigen)));
                }
                if(idDestino> 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clientefactoriaIddestinatario"), idDestino)));
                }   
                
                if (tipoBusqueda == BusquedaGuias.GUIAS_NO_FACTURADAS.getValue()) { // guiasSinLiqFac
                    predicates.add(criteriaBuilder.and(criteriaBuilder.isNull(root.get("liquidacionIdliquidacion"))));
                    predicates.add(criteriaBuilder.and(criteriaBuilder.isNull(root.get("documentoIddocumento"))));
                } else {
                	if (tipoBusqueda == BusquedaGuias.GUIAS_FACTURADAS.getValue()) { // soloGuiasFacturadas
                	    
                        predicates.add(criteriaBuilder.and(criteriaBuilder.or(criteriaBuilder.isNotNull(root.get("documentoIddocumento")) , 
                        														criteriaBuilder.isNotNull(root.get("liquidacionIdliquidacion")) 
                        													  )
                        								)             		
                        );


                	}
                }

                
                if(fechaIni!= null && fechaFin!= null){
                	//if (!(nroSerie!=null && nroSerie!= "" ) && !(nroSecuencia!=null && nroSecuencia!= "" )) {
                        predicates.add(criteriaBuilder.and(
                        		criteriaBuilder.greaterThanOrEqualTo(root.get("fecharemision"), fechaIni),
                        		criteriaBuilder.lessThanOrEqualTo(root.get("fecharemision"), fechaFin)
                        		)
                        );
                	//}

                }
                //ORDER BY
                query.orderBy(criteriaBuilder.desc(root.get("fecharemision")),
               			  criteriaBuilder.desc(root.get("nroserieguia")),
                		  criteriaBuilder.desc(root.get("nrosecuenciaguia"))); 
                
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });	
    }

	@Override
	public Optional<GuiaRemision> obtenerGuiaRemisionPorNroGuiaCliente(String nroSerieCli, String nroSecuenciaCli,
			Integer idEmpresa) {
		
		Optional<GuiaRemision> optGuiaRemision = this.guiaRemisionRepository.obtenerGuiasPorGuiaCliente(nroSerieCli, nroSecuenciaCli, idEmpresa);
		
		if (optGuiaRemision.isPresent() && optGuiaRemision.get().getClientefactoriaIdremitente()!= null 
				&& optGuiaRemision.get().getClientefactoriaIddestinatario() != null) {
			//Obtiene tarifa asociada a la ruta
			Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.
											obtenerTarifaTurnoAsociada(optGuiaRemision.get().getClientefactoriaIdremitente().getIdclientefactoria(), 
																		optGuiaRemision.get().getClientefactoriaIddestinatario().getIdclientefactoria(), 
																	   new Integer(1));
			if(tarifaRuta.isPresent()) {
				BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
				optGuiaRemision.get().setImportetarifaasociado(tarifa);
			}else {
				optGuiaRemision.get().setImportetarifaasociado(new BigDecimal(0.00));
			}
			
		}

		return optGuiaRemision;
	}

	@Override
	public List<GuiaRemision> listarGuiasPorNroSerieCli(Integer idEmpresa, String nroSerieCli, Date fechaIni, Date fechaFin) {
		return this.guiaRemisionRepository.listarGuiasPorSerieCliente(nroSerieCli, idEmpresa, fechaIni, fechaFin );
	}

	@Override
	public List<GuiaRemision> listarGuiasPorNroSerie(Integer idEmpresa, String nroSerie, Date fechaIni, Date fechaFin) {
		return this.guiaRemisionRepository.listarGuiasPorSerieGuia(nroSerie, idEmpresa, fechaIni, fechaFin);
	}

	@Override
	public List<GuiaRemision> listarGuiasPorCoincidenciaNroSerieSecuencia(Integer idEmpresa, String nroSerie,
			String nroSecuencia, Date fechaIni, Date fechaFin) {
		
        return guiaRemisionDAO.findAll(new Specification<GuiaRemision>() {
			private static final long serialVersionUID = 1L;

			@Override
            public Predicate toPredicate(Root<GuiaRemision> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                
                // WHERE
                if(idEmpresa > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("empresaIdempresa"), idEmpresa)));
                }       
                if(nroSerie!=null ) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("nroserieguia"), "%"+nroSerie+"%")));
                }
                
                if(nroSecuencia!=null) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("nrosecuenciaguia"), "%"+nroSecuencia+"%")));
                }
                if(fechaIni!= null && fechaFin!= null){
                    predicates.add(criteriaBuilder.and(
                    		criteriaBuilder.greaterThanOrEqualTo(root.get("fecharemision"), fechaIni),
                    		criteriaBuilder.lessThanOrEqualTo(root.get("fecharemision"), fechaFin)
                    		)
                    );

                }
                
                //ORDER BY
                query.orderBy(criteriaBuilder.desc(root.get("fecharemision")),
               			  criteriaBuilder.desc(root.get("nroserieguia")),
                		  criteriaBuilder.desc(root.get("nrosecuenciaguia"))); 
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });	
	}

	@Override
	public List<GuiaRemision> listarGuiasPorCoincidenciaNroSerieSecuenciaGuiaCliente(Integer idEmpresa,
			String nroSerieCli, String nroSecuenciaCli, Integer idEstado, Integer idChofer, Integer idOrigen,
			Integer idDestino, Date fechaIni, Date fechaFin, Integer tipoBusqueda) {
		 return guiaRemisionDAO.findAll(new Specification<GuiaRemision>() {
				private static final long serialVersionUID = 1L;

				@Override
	            public Predicate toPredicate(Root<GuiaRemision> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
	                List<Predicate> predicates = new ArrayList<>();
	                
	                // WHERE
	                if(idEmpresa > 0) {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("empresaIdempresa"), idEmpresa)));
	                }       
	                if(nroSerieCli!=null && nroSerieCli!="") {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("nroserieguiacliente"), "%"+nroSerieCli+"%")));
	                }
	                
	                if(nroSecuenciaCli!=null && nroSecuenciaCli!="") {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("nrosecuenguiacliente"), "%"+nroSecuenciaCli+"%")));
	                }
	                if(idEstado >= 0 && idEstado < 99) {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estadoguia"), idEstado)));
	                }
	                if(idChofer > 0) {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("transportistaIdtransportista"), idChofer)));
	                }
	                if(idOrigen > 0) {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clientefactoriaIdremitente"), idOrigen)));
	                }
	                if(idDestino> 0) {
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clientefactoriaIddestinatario"), idDestino)));
	                }               
	                if (tipoBusqueda == BusquedaGuias.GUIAS_NO_FACTURADAS.getValue()) {  // sinFacturaLiquidaciones
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.isNull(root.get("liquidacionIdliquidacion"))));
	                    predicates.add(criteriaBuilder.and(criteriaBuilder.isNull(root.get("documentoIddocumento"))));
	                } else {
	                	if (tipoBusqueda == BusquedaGuias.GUIAS_FACTURADAS.getValue()) {  // soloGuiasFacturadas
	                        predicates.add(criteriaBuilder.and(criteriaBuilder.or(criteriaBuilder.isNotNull(root.get("documentoIddocumento")) , 
	                        														criteriaBuilder.isNotNull(root.get("liquidacionIdliquidacion")) 
	                        													  )));
	                	}
	                }
	                
	                if(fechaIni!= null && fechaFin!= null){
	                	//if ( !(nroSerieCli!=null && nroSerieCli!="") && !(nroSecuenciaCli!=null && nroSecuenciaCli!="") ) {
		                    predicates.add(criteriaBuilder.and(
		                    		criteriaBuilder.greaterThanOrEqualTo(root.get("fecharemision"), fechaIni),
		                    		criteriaBuilder.lessThanOrEqualTo(root.get("fecharemision"), fechaFin)
		                    		)
		                    );
	                	//}

	                }
	                
	                //ORDER BY
	                query.orderBy(criteriaBuilder.desc(root.get("fecharemision")),
	               			  criteriaBuilder.desc(root.get("nroserieguia")),
	                		  criteriaBuilder.desc(root.get("nrosecuenciaguia"))); 
	                
	                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	            }
	        });	
	}

	@Override
	public List<GuiaRemision> actualizarTarifasEnListado(List<GuiaRemision> lista) {
		List<GuiaRemision> listadoFinal = new ArrayList<>();
		for (GuiaRemision guiaRemision_ : lista) {
			if (guiaRemision_.getClientefactoriaIdremitente()!= null && guiaRemision_.getClientefactoriaIddestinatario()!=null) {
				Optional<TarifaRuta> tarifaRuta = tarifaRutaRepository.obtenerTarifaTurnoAsociada(
						guiaRemision_.getClientefactoriaIdremitente().getIdclientefactoria(),
						guiaRemision_.getClientefactoriaIddestinatario().getIdclientefactoria(), new Integer(1));
				if (tarifaRuta.isPresent()) {
					BigDecimal tarifa = tarifaRuta.get().getImportetarifa();
					guiaRemision_.setImportetarifaasociado(tarifa);
				} else {
					guiaRemision_.setImportetarifaasociado(new BigDecimal(0.00));
				}
			} else {
				guiaRemision_.setImportetarifaasociado(new BigDecimal(0.00));
			}

			listadoFinal.add(guiaRemision_);
		}
		return listadoFinal;
	}
	
	private String retornaNemonicoEstadoFactura(Integer estadoFactura) {
		String nemonicoEstadoFactura = "-";
		switch (estadoFactura) {
		case 1:
			nemonicoEstadoFactura = "Registrado";
			break;
		case 2:
			nemonicoEstadoFactura = "Cancelado";
			break;
		case 3:
			nemonicoEstadoFactura = "Anulado";
			break;
		default:
			break;
		}
		return nemonicoEstadoFactura;
	}

	@Override
	@Transactional
	public GuiaRemision extornarGuiaRemision(GuiaRemision guiaRemisionExtornar) {

		if (guiaRemisionExtornar !=null) {
			// Fechas
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			guiaRemisionExtornar.setFechaactualizasistema(fechaTimestamp);
			guiaRemisionExtornar.setFecharegistrosistema(fechaTimestamp);
			
			// Detalle de Guias
			Set<GuiaRemisionProducto> listaGuiaRemisionProducto_ = new HashSet<GuiaRemisionProducto>();
			Set<GuiaRemisionProducto> listaGuiaRemisionProductoUpdated = guiaRemisionExtornar
					.getGuiaRemisionProductoSet();
			
			for (GuiaRemisionProducto guiaRemisionProducto : listaGuiaRemisionProductoUpdated) {
				guiaRemisionProducto.setGuiaremisionIdguiaremision(guiaRemisionExtornar);
				listaGuiaRemisionProducto_.add(guiaRemisionProducto);
			}
			guiaRemisionExtornar.setGuiaRemisionProductoSet(listaGuiaRemisionProducto_);
			
			// Libera guia de cliente asociada
			guiaRemisionExtornar.setNroserieguiacliente("");
			guiaRemisionExtornar.setNrosecuenguiacliente("");

			return guiaRemisionRepository.save(guiaRemisionExtornar);
		}
		return new GuiaRemision();
	}

	@Override
	public GuiaRemision actualizarTarifaGuiaRemision(GuiaRemision obj) {
		Optional<GuiaRemision> guiaRemisionBD = guiaRemisionRepository.findById(obj.getIdguiaremision());
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		if (guiaRemisionBD.isPresent()) {
			GuiaRemision guiaRemisionUpdate = guiaRemisionBD.get();
			guiaRemisionUpdate.setImportetarifaasociado(obj.getImportetarifaasociado());
			Date fechaTimestamp = new Date(ts.getTime());
			guiaRemisionUpdate.setFechaactualizasistema(fechaTimestamp);
			guiaRemisionUpdate.setUsuarioactualiza(obj.getUsuarioactualiza());
			return guiaRemisionRepository.save(guiaRemisionUpdate);
		}
		return new GuiaRemision();
	}

	@Override
	public void eliminarGuiaBD(Integer idGuiaRemision) {
		this.guiaRemisionRepository.deleteById(idGuiaRemision);
	}

	@Override
	@Transactional
	public void liberarGuiasDeLiquidacion(Integer idLiquidacion) {
		this.guiaRemisionRepository.liberarGuiasDeLiquidacion(idLiquidacion);		
	}
	


}
