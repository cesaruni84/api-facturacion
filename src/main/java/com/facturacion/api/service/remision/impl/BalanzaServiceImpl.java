/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.Balanza;
import com.facturacion.api.repository.BalanzaRepository;
import com.facturacion.api.service.remision.BalanzaService;

/**
 * @author CESAR
 *
 */
@Service
public class BalanzaServiceImpl implements BalanzaService {
	
	
	@Autowired
	private BalanzaRepository balanzaRepository  ;

	@Override
	public List<Balanza> listarTodasBalanzas() {
		// TODO Auto-generated method stub
		return balanzaRepository.findAll();
	}



}
