/**
 * 
 */
package com.facturacion.api.service.remision;

import java.util.List;
import com.facturacion.api.model.entity.Transportista;

/**
 * @author CESAR
 *
 */
public interface TransportistaService {
	
	List<Transportista> listarChoferesPorEmpresa(Integer idEmpresa);
	Transportista registrarTransportista(Transportista choferCreate);
	Transportista actualizaTransportista(Transportista choferUpdate);


}
