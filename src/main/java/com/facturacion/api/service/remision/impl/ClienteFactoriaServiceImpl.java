/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.ClienteFactoria;
import com.facturacion.api.repository.ClienteFactoriaRepository;
import com.facturacion.api.service.remision.ClienteFactoriaService;

/**
 * @author CESAR
 *
 */
@Service
public class ClienteFactoriaServiceImpl implements ClienteFactoriaService {
	
	@Autowired
	private ClienteFactoriaRepository clienteFactoriaRepository;
	

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.ClienteFactoriaService#listarTodasLasFactorias()
	 */
	@Override
	public List<ClienteFactoria> listarTodasLasFactorias() {
		// TODO Auto-generated method stub
		return clienteFactoriaRepository.listarTodoFactoria();
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.remision.ClienteFactoriaService#listarTodasLasFactoriasPorCliente(java.lang.Integer)
	 */
	@Override
	public List<ClienteFactoria> listarTodasLasFactoriasPorCliente(Integer idCliente) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ClienteFactoria> listarFactoriasPorTipo(String tipoFactoria) {
		return this.clienteFactoriaRepository.listarFactoriasPorTipo(tipoFactoria);
	}

	@Override
	public ClienteFactoria registrarFactoria(ClienteFactoria factoriaCreate) {
		return clienteFactoriaRepository.save(factoriaCreate);
	}

	@Override
	public ClienteFactoria actualizarFactoria(ClienteFactoria factoriaUpdate) {
		return clienteFactoriaRepository.save(factoriaUpdate);
	}

	@Override
	public void eliminarFactoria(Integer idFactoria) {
		clienteFactoriaRepository.deleteById(idFactoria);
	}


}
