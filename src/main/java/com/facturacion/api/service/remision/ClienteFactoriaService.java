/**
 * 
 */
package com.facturacion.api.service.remision;

import java.util.List;
import com.facturacion.api.model.entity.ClienteFactoria;

/**
 * @author CESAR
 *
 */
public interface ClienteFactoriaService {
	
	
	List<ClienteFactoria> listarTodasLasFactorias();
	List<ClienteFactoria> listarTodasLasFactoriasPorCliente(Integer idCliente);
	List<ClienteFactoria> listarFactoriasPorTipo(String tipoFactoria);
	ClienteFactoria registrarFactoria(ClienteFactoria factoriaCreate);
	ClienteFactoria actualizarFactoria(ClienteFactoria factoriaUpdate);
	void eliminarFactoria(Integer idFactoria);

}
