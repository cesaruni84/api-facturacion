package com.facturacion.api.service.remision;

import java.util.List;
import com.facturacion.api.model.entity.Vehiculo;

public interface VehiculoService {
	
	List<Vehiculo> listarTodasLosVehiculos();
	Vehiculo registrarVehiculo(Vehiculo vehiculoCreate);
	Vehiculo actualizarVehiculo(Vehiculo vehiculoUpdate);

}
