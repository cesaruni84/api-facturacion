/**
 * 
 */
package com.facturacion.api.service.remision.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.TarifaRuta;
import com.facturacion.api.repository.TarifaRutaRepository;
import com.facturacion.api.service.remision.TarifaRutaService;

/**
 * @author CESAR
 *
 */
@Service
public class TarifaRutaServiceImpl implements TarifaRutaService{
	
	@Autowired
	private TarifaRutaRepository tarifaRutaRepo;

	@Override
	public Optional<TarifaRuta> obtenerTarifaRuta(Integer idCliOrigen, Integer idCliDestino, Integer idTurno) {
		return this.tarifaRutaRepo.obtenerTarifaTurnoAsociada(idCliOrigen, idCliDestino, idTurno);
	}

	@Override
	public List<TarifaRuta> listarTarifas() {
		return tarifaRutaRepo.findAll();
	}

	@Override
	public TarifaRuta registrarTarifa(TarifaRuta tarifaCreate) {
		return tarifaRutaRepo.save(tarifaCreate);
	}

	@Override
	public TarifaRuta actualizarTarifa(TarifaRuta tarifaUpdate) {
		Optional<TarifaRuta> tarifaQuery = tarifaRutaRepo.findById(tarifaUpdate.getIdtarifaruta());
		tarifaUpdate.setFecharegistrosistema(tarifaQuery.get().getFecharegistrosistema());
		return tarifaRutaRepo.save(tarifaUpdate);
	}

	@Override
	public void eliminarTarifaBD(Integer idTarifaRuta) {
		tarifaRutaRepo.deleteById(idTarifaRuta);
	}

}
