/**
 * 
 */
package com.facturacion.api.service.remision;

import java.util.List;
import com.facturacion.api.model.entity.Balanza;

/**
 * @author CESAR
 *
 */
public interface BalanzaService {
	
	List<Balanza> listarTodasBalanzas();


}
