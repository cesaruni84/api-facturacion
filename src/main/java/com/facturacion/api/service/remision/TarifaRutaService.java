package com.facturacion.api.service.remision;

import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.entity.TarifaRuta;

public interface TarifaRutaService {
	
	Optional<TarifaRuta> obtenerTarifaRuta(Integer idCliOrigen, Integer idCliDestino, Integer idTurno);
	List<TarifaRuta> listarTarifas();
	TarifaRuta registrarTarifa(TarifaRuta tarifaCreate);
	TarifaRuta actualizarTarifa(TarifaRuta tarifaUpdate);
	void eliminarTarifaBD(Integer idTarifaRuta);

}
