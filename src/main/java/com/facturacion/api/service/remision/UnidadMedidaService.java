/**
 * 
 */
package com.facturacion.api.service.remision;

import java.util.List;
import com.facturacion.api.model.entity.UnidadMedida;

/**
 * @author CESAR
 *
 */
public interface UnidadMedidaService {
	
	
	List<UnidadMedida> listarTodasLasUnidadesMedidas();

}
