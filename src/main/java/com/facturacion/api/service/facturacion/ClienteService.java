/**
 * 
 */
package com.facturacion.api.service.facturacion;

import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.entity.Cliente;

/**
 * @author CESAR
 *
 */
public interface ClienteService {

	List<Cliente> listarTodosLosClientes(Integer idEmpresa);
	
	// Graba cliente 
	Cliente registrarCliente(Cliente clienteCreate);
	
	// Actualiza cliente
	Cliente actualizarCliente(Cliente clienteUpdate);
	
	// Búsqueda por Nro de Doc Cliente
	Optional<Cliente> obtenerClientePorNroDoc(Integer tipoDoc, String nroDoc);


}
