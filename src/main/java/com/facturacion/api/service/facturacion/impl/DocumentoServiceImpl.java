/**
 * 
 */
package com.facturacion.api.service.facturacion.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.facturacion.api.dao.DocumentoDAO;
import com.facturacion.api.model.entity.Documento;
import com.facturacion.api.model.entity.DocumentoItem;
import com.facturacion.api.model.entity.GuiaRemision;
import com.facturacion.api.model.entity.Liquidacion;
import com.facturacion.api.model.enums.TipoDocumento;
import com.facturacion.api.model.enums.TipoFactura;
import com.facturacion.api.repository.DocumentoRepository;
import com.facturacion.api.repository.GuiaRemisionRepository;
import com.facturacion.api.repository.LiquidacionRepository;
import com.facturacion.api.service.facturacion.DocumentoService;

/**
 * @author CESAR
 *
 */

@Service
public class DocumentoServiceImpl implements DocumentoService {
	
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired
	private GuiaRemisionRepository guiaRemisionRepository;
	
	@Autowired
	private DocumentoDAO documentoDAO;

	
	@Autowired
	private LiquidacionRepository liquidacionRepository;
	
	/* (non-Javadoc)
	 * @see com.facturacion.api.service.facturacion.DocumentoService#listTodosComprobantesPorEmpresa(java.lang.Integer)
	 */
	@Override
	public List<Documento> listTodosComprobantesPorEmpresa(Integer idEmpresa) {
		// TODO Auto-generated method stub
		return this.documentoRepository.listarDocumentosPorEmpresa(idEmpresa);
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.facturacion.DocumentoService#listTodosComprobantesPorEmpresaCliente(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Documento> listTodosComprobantesPorEmpresaCliente(Integer idEmpresa, Integer idCliente) {
		return this.documentoRepository.listarDocumentosPorEmpresaCliente(idEmpresa, idCliente);
	}


	/* (non-Javadoc)
	 * @see com.facturacion.api.service.facturacion.DocumentoService#obtenerComprobantePorNroDoc(java.lang.String, java.lang.String)
	 */
	@Override
	public Optional<Documento> obtenerComprobantePorNroDoc(Integer tipoDocumento, String nroSerie, String nroSecuencia, Integer idEmpresa) {
		if (tipoDocumento == TipoFactura.TODOS.getValue()) {
			return this.documentoRepository.obtenerComprobantePorNroDOC(nroSerie, nroSecuencia,idEmpresa);
		}else {
			return this.documentoRepository.obtenerComprobantePorNroDocTipo(tipoDocumento, nroSerie, nroSecuencia,idEmpresa);
		}
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.facturacion.DocumentoService#registrarDocumento(com.facturacion.api.model.entity.Documento)
	 */
	@Override
	@Transactional
	public Documento registrarDocumento(Documento documentoCreate) {
		
		// TimeStamp
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date fechaTimestamp = new Date(ts.getTime());

		// Fecha Registro y Actualizacion
		documentoCreate.setFecharegistro(fechaTimestamp);
		documentoCreate.setFechaactualiza(fechaTimestamp);
		
		
		// Documento Item
		List<DocumentoItem> listaDocumentosItemFinal = new ArrayList<DocumentoItem>();
		List<DocumentoItem> listaDocumentosItem_ = documentoCreate.getDocumentoitemSet();
		for (DocumentoItem item : listaDocumentosItem_) {
			item.setIddocitem(null);
			item.setDocumentoIddocumento(documentoCreate);
			listaDocumentosItemFinal.add(item);
		}
		documentoCreate.setDocumentoitemSet(listaDocumentosItemFinal);
		
		// Liquidaciones
		Set<Liquidacion> listaLiquidacionesFinal = new HashSet<Liquidacion>();
		Set<Liquidacion> listaLiquidaciones_ = documentoCreate.getLiquidacionSet();
		for (Liquidacion liquidacion : listaLiquidaciones_) {
			Optional<Liquidacion> liquidacionBD = liquidacionRepository.findById(liquidacion.getIdliquidacion());
			Liquidacion liquidacionBD_ = liquidacionBD.get();
			liquidacionBD_.setDocumentoIddocumento(documentoCreate);
			listaLiquidacionesFinal.add(liquidacionBD_);
		}
		documentoCreate.setLiquidacionSet(listaLiquidacionesFinal);
		
		
		// Guias de Remisión
		Set<GuiaRemision> listaGuiaRemisionFinal = new HashSet<GuiaRemision>();
		Set<GuiaRemision> listaGuiaRemision_ = documentoCreate.getGuiaRemisionSet();
		for (GuiaRemision guia : listaGuiaRemision_) {
			Optional<GuiaRemision> ordenServicioBD = guiaRemisionRepository.findById(guia.getIdguiaremision());
			GuiaRemision guiaBD_ = ordenServicioBD.get();
			guiaBD_.setDocumentoIddocumento(documentoCreate);
			listaGuiaRemisionFinal.add(guiaBD_);
		}
		documentoCreate.setGuiaRemisionSet(listaGuiaRemisionFinal);
	
		// Grabar en BD
		return this.documentoRepository.save(documentoCreate);
	}

	/* (non-Javadoc)
	 * @see com.facturacion.api.service.facturacion.DocumentoService#actualizarDocumento(com.facturacion.api.model.entity.Documento)
	 */
	@Override
	@Transactional
	public Documento actualizarDocumento(Documento documentoUpdate) {
		
		Optional<Documento> documentoQuery = documentoRepository.findById(documentoUpdate.getIddocumento());
		
		if (documentoQuery.isPresent()) {
			// TimeStamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			documentoUpdate.setFechaactualiza(fechaTimestamp);
			documentoUpdate.setFecharegistro(documentoQuery.get().getFecharegistro());
			
			// Documento Item
			List<DocumentoItem> listaDocumentosItemFinal = new ArrayList<DocumentoItem>();
			List<DocumentoItem> listaDocumentosItem_ = documentoUpdate.getDocumentoitemSet();
			for (DocumentoItem item : listaDocumentosItem_) {
				item.setDocumentoIddocumento(documentoUpdate);
				listaDocumentosItemFinal.add(item);
			}
			documentoUpdate.setDocumentoitemSet(listaDocumentosItemFinal);
			
			// Liquidaciones
			Set<Liquidacion> listaLiquidacionesFinal = new HashSet<Liquidacion>();
			Set<Liquidacion> listaLiquidaciones_ = documentoUpdate.getLiquidacionSet();
			for (Liquidacion liquidacion : listaLiquidaciones_) {
				if (liquidacion.getIdliquidacion() != null) {
					Optional<Liquidacion> liquidacionBD = liquidacionRepository.findById(liquidacion.getIdliquidacion());
					Liquidacion liquidacionBD_ = liquidacionBD.get();
					liquidacionBD_.setDocumentoIddocumento(documentoUpdate);
					listaLiquidacionesFinal.add(liquidacionBD_);
				}
			}
			documentoUpdate.setLiquidacionSet(listaLiquidacionesFinal);
			
			// Guias de Remisión
			Set<GuiaRemision> listaGuiaRemisionFinal = new HashSet<GuiaRemision>();
			Set<GuiaRemision> listaGuiaRemision_ = documentoUpdate.getGuiaRemisionSet();
			for (GuiaRemision guia : listaGuiaRemision_) {
				if (guia.getIdguiaremision()!= null) {
					Optional<GuiaRemision> ordenServicioBD = guiaRemisionRepository.findById(guia.getIdguiaremision());
					GuiaRemision guiaBD_ = ordenServicioBD.get();
					guiaBD_.setDocumentoIddocumento(documentoUpdate);
					listaGuiaRemisionFinal.add(guiaBD_);
				}

			}
			documentoUpdate.setGuiaRemisionSet(listaGuiaRemisionFinal);
			
			// Actualiza en BD
			return this.documentoRepository.save(documentoUpdate);
		}

		return null;
	}

	@Override
	public List<Documento> listarComprobantesPorFiltro(Integer idEmpresa, Integer idTipoDocumento, Integer idCliente,
			Integer estado, Date fechaIni, Date fechaFin) {
		// TODO Auto-generated method stub

		Integer idClienteMin, idClienteMax = 0;
		Integer idEstadoMin, idEstadoMax = 0;

		if (idCliente == 0) {
			idClienteMin = idCliente;
			idClienteMax = 999;
		} else {
			idClienteMin = idCliente;
			idClienteMax = idCliente;
		}

		if (estado == 0) {
			idEstadoMin = estado;
			idEstadoMax = 99;
		} else {
			idEstadoMin = estado;
			idEstadoMax = estado;
		}

		return this.documentoRepository.listarDocumentosPorFiltro(idEmpresa, idClienteMin, idClienteMax, idEstadoMin,
				idEstadoMax, fechaIni, fechaFin, idTipoDocumento);
	}

	@Override
	public List<Documento> listarDocumentosPorFiltro(Integer idEmpresa, Integer idTipoDocumento,String serie, String secuencia, Integer idCliente,
			Integer estado, Date fechaIni, Date fechaFin) {
		
		
        return documentoDAO.findAll(new Specification<Documento>() {
			private static final long serialVersionUID = 1L;

			@Override
            public Predicate toPredicate(Root<Documento> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                
                // WHERE
                if(idEmpresa > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("empresaIdempresa"), idEmpresa)));
                } 
                
                if(idTipoDocumento > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("tipodocumento"), idTipoDocumento)));
                } 
                
                if(serie!=null && serie != "") {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("serie"), "%"+serie+"%")));
                }
                   
                if(secuencia!=null && secuencia != "") {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("secuencia"), "%"+secuencia+"%")));
                }
                
                if(idCliente > 0 ) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("clienteIdcliente"), idCliente)));
                }
                
                if(estado > 0) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estado"), estado)));
                }
                if(fechaIni!= null && fechaFin!= null){
                    predicates.add(criteriaBuilder.and(
                    		criteriaBuilder.greaterThanOrEqualTo(root.get("fechaemision"), fechaIni),
                    		criteriaBuilder.lessThanOrEqualTo(root.get("fechaemision"), fechaFin)
                    		)
                    );

                }
                
                //ORDER BY
                query.orderBy(criteriaBuilder.desc(root.get("serie")),
               			  criteriaBuilder.desc(root.get("secuencia")),
                		  criteriaBuilder.desc(root.get("fechaemision"))); 
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });	
		
		

	}

	@Override
	@Transactional
	public Documento extornarDocumento(Documento documentoUpdate) {
		
		Optional<Documento> documentoQuery = documentoRepository.findById(documentoUpdate.getIddocumento());

		if (documentoQuery.isPresent()) {
			// TimeStamp
			Documento documento = documentoQuery.get();
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			documento.setFechaactualiza(fechaTimestamp);
			documento.setEstado(documentoUpdate.getEstado());
			// documentoUpdate.setFecharegistro(documentoQuery.get().getFecharegistro());
			
			// Documento Item
			// List<DocumentoItem> listaDocumentosItem_ = documentoQuery.get().getDocumentoitemSet();
			// documentoQuery.get().setDocumentoitemSet(listaDocumentosItem_);

			// libera Liquidaciones
			// Set<Liquidacion> listaLiquidaciones_ = documento.getLiquidacionSet();
			// Set<GuiaRemision> listaGuiaRemision_ = documento.getGuiaRemisionSet();
			Set<Liquidacion> listaLiquidaciones_ = documento.getLiquidacionSet();


			// if (listaLiquidaciones_ !=null) {
				//if (listaLiquidaciones_.size() > 0 ) {
			if (documento.getNotasadicionales().trim().equals(String.valueOf(TipoDocumento.CON_LIQUIDACION.getValue()))) {
				
				//Libera guias x liquidacion
				for (Liquidacion liquidacion : listaLiquidaciones_) {
					this.documentoRepository.liberarGuiaLiquidacion(liquidacion.getIdliquidacion());
				}
				
				//Libera liquidaciones - documento
				this.documentoRepository.liberarLiquidacionDocumento(documento.getIddocumento());
				

				

			} else if (documento.getNotasadicionales().trim().equals(String.valueOf(TipoDocumento.CON_GUIAREMISION.getValue()))) {
				this.documentoRepository.liberarGuiaDocumento(documento.getIddocumento());
			}
			
				// }
			//}
	
			// libera Guias de Remisión
			//if (listaGuiaRemision_!=null) {
				// System.out.println("paso 31");
				//if (listaGuiaRemision_.size() > 0 ) {
				//}
			//}
			// Set<Liquidacion> listaLiquidacionesFinal = new HashSet<Liquidacion>();
			
			// documento.setLiquidacionSet(new HashSet<Liquidacion>() );
			documento.setLiquidacionSet(null);
			documento.setGuiaRemisionSet(null);
			// documentoQuery.get().setGuiaRemisionSet(new HashSet<GuiaRemision>());


			// Actualiza en BD
			return this.documentoRepository.save(documento);
		}

		return null;
	}

	@Override
	public Documento cancelarDocumento(Documento documentoCancel) {

		Optional<Documento> documentoQuery = documentoRepository.findById(documentoCancel.getIddocumento());

		if (documentoQuery.isPresent()) {
			// TimeStamp
			Documento documento = documentoQuery.get();
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date fechaTimestamp = new Date(ts.getTime());
			documento.setFechaactualiza(fechaTimestamp);
			documento.setEstado(documentoCancel.getEstado());
			documento.setObservacionsunat(documentoCancel.getObservacionsunat());

			// Actualiza en BD
			return this.documentoRepository.save(documento);
		}

		return null;
	}

	@Override
	public List<Documento> listarDocumentosVigentesIncongruentes(Integer idEmpresa, Integer subTipoDocumento,Date fechaIni, Date fechaFin) {
		return  this.documentoRepository.listarDocumentosVigentesIncongruentes(idEmpresa, subTipoDocumento.toString(), fechaIni, fechaFin);
	}

}
