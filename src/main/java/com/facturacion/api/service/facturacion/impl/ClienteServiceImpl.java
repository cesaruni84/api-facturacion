package com.facturacion.api.service.facturacion.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.facturacion.api.model.entity.Cliente;
import com.facturacion.api.repository.ClienteRepository;
import com.facturacion.api.service.facturacion.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public List<Cliente> listarTodosLosClientes(Integer idEmpresa) {
		return this.clienteRepository.findAll();
	}

	@Override
	public Cliente registrarCliente(Cliente clienteCreate) {
		return this.clienteRepository.save(clienteCreate);
	}

	@Override
	public Cliente actualizarCliente(Cliente clienteUpdate) {
		Optional<Cliente> clienteQuery = this.clienteRepository.findById(clienteUpdate.getIdcliente());
		clienteUpdate.setFecharegistro(clienteQuery.get().getFecharegistro());
		return this.clienteRepository.save(clienteUpdate);
	}

	@Override
	public Optional<Cliente> obtenerClientePorNroDoc(Integer tipoDoc, String nroDoc) {
		return this.clienteRepository.obtenerClientePorNroDOC(tipoDoc, nroDoc);
	}

}
