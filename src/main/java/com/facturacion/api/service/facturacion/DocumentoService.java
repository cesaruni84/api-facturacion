package com.facturacion.api.service.facturacion;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import com.facturacion.api.model.entity.Documento;

public interface DocumentoService {
	
	//Facturas por Empresa
	List<Documento> listTodosComprobantesPorEmpresa(Integer idEmpresa);
	
	//Facturas por Empresa y Cliente
	List<Documento> listTodosComprobantesPorEmpresaCliente(Integer idEmpresa, Integer idCliente);
	

	List<Documento> listarComprobantesPorFiltro( Integer idEmpresa,
													Integer idTipoDocumento,
													Integer idCliente,
													Integer estado,
													Date fechaIni,
													Date fechaFin);

	List<Documento> listarDocumentosPorFiltro( Integer idEmpresa,
													Integer idTipoDocumento,
													String serie,
													String secuencia,
													Integer idCliente,
													Integer estado,
													Date fechaIni,
													Date fechaFin);
	
	
	//Optional<Liquidacion> obtenerLiquidacionPorId(Integer idLiquidacion, Integer idEmpresa);
	// Factura por Nro de Serie y Secuencia
	Optional<Documento> obtenerComprobantePorNroDoc(Integer tipoDocumento, String nroSerie, String nroSecuencia, Integer idEmpresa);
	
	// Graba documento electrónico
	Documento registrarDocumento(Documento documentoCreate);
	
	// Actualiza documento electrónico
	Documento actualizarDocumento(Documento documentoUpdate);
	
	// Extorna documento electrónico
	Documento extornarDocumento(Documento documentoUpdate);
	
	// Cancela documento electrónico
	Documento cancelarDocumento(Documento documentoUpdate);
	
	List<Documento> listarDocumentosVigentesIncongruentes( Integer idEmpresa,
			Integer subTipoDocumento,
			Date fechaIni,
			Date fechaFin);

}
