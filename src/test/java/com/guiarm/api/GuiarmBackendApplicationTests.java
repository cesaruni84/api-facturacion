package com.guiarm.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.facturacion.api.GuiarmBackendApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GuiarmBackendApplication.class)
public class GuiarmBackendApplicationTests {

	@Test
	public void contextLoads() {
	}

}
